# EventType #
This enum represents one of 3 types: `PRESS`, `HOLD` or `RELEASE`. 

* `PRESS` : In the previous iteration no collision occurred. In the current iteration a collision occurred.
* `HOLD` : In the previous iteration a collision occurred. In the current iteration a collision occurred.
* `RELEASE` : In the previous iteration a collision occurred. In the current iteration no collision occurred.

It is defined in a separate .proto file because it is used by both the Event- and the Configuration-Message.