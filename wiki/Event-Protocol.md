# Overview #
The configuration protocol is used initially set up the UI-Server, change the underlying logical layout, initialize calibration assist and print out the status of the underlying data structure.

Messages sent between client and server need to be prefixed with a fixed, 4 byte sized header and need a trailing end-of-transmission(^D) character. The header defines the length of the following protocol buffer message, and contains 4 bytes of little endian encoded integer. Note: Most socket implementations provide an automated end-of-transmission character. Multiple EOT characters might result in unwanted behavior.

![drawio_protobuf_message_structure.png](pictures/drawio_protobuf_message_structure.png)

# Protocol Buffer Message #

* int32 element_id : the id of the ui element (button) responsible for this event

* [EventType](EventType.md) eventType : one of 3 types: PRESS, HOLD or RELEASE.

* string info : optional string for additional information

* int32 listener_id : the id of the element's listener which should only listen to events of type `eventType`

# Distributing IDs #
IDs are distributed by the server where each id is unique for its type(Element, Listener). IDs start at 1 to differentiate between the default(0) and set IDs inside a protobuf message.