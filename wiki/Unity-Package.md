# General

# High Level Component #

The high-level component allows easy, plug- and play style configuration of the remote fuics server. It is a simple, empty game object that can be placed in the scene. 

The component is supposed to serve the following purposes

* Hide all internal work behind an easy-to-use, Inspector based component
* Connect to the remote server, configure and calibrate it within a single step, upon launch or at request
* Integrate into Unity's predefined system. 

## Usage ##
There are two ways to use this package. 

1. Usage of the high level component. See [here](Unity-Package-Getting-Started.md) for more Information.

2. Usage of the low level API. See [here](Unity-Package-API.md) for more information.