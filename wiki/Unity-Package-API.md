# Overview
The FUICS's Unity package API implements the FUICS protocol on a lower, close-to-network layer. It allows direct communication with the server, with only very little overhead in between. Each action performed is translated into a protocol message counterpart. An example:

A call of `AddListener(int id, ListenerAdapter listener)` is directly translated into a message of type [`ADD_LISTENER`](Configuration-Request-Types.md#Add Listener).

![UnityPackageStructureOverview.png](pictures/UnityPackageStructureOverview.png)

As seen above, the package is structured into 3 major components. The network components use the TCP protocol to communicate with the server, and are split into the `EventMessageDAO`(persistent TCP connection, event channel) and `ConfigurationMessageDAO` (configuration channel). 

Instead of working directly on the generated protocol buffer code, all non-internal methods and components are working on adapters. Adapters are written for all necessary classes and most enums. If an adapter does not exist, it is most likely not necessary, and another way exists to use the API correctly.

The API functionality is exposed within the `FUICSCore` class. All non-internal methods are intended to be used as an API, whereas methods declared as `internal` should **not** be called, as they may potentially cause unwanted behaviour.

# Basics
## Initialization
The `FUICSCore` class is used as main component for client/server communication. As such, this core has to be used and initialized for any operations to succedd. 

To allow easy integration into Unity, without dragging references through the inspector, core instances are unique per hostname, and can be created by calling `FUICSCore.CreateCore(hostname)`, where hostname defines the server to which the client will connect. By calling `FUICSCore.CreateCore(hostname)` with the same hostname again, the previously created instance will be returned.

For Logging purposes, [Log4Net](https://logging.apache.org/log4net/) needs to be initialized from within the source code. The static method `ConfigureAllLogging` achieves that. Log Levels can be altered with `SetLogLevel`.

The [event](Event-Protocol.md) channel is not open by default. To listen for events, `StartListening()` has to be called. 

## Adding Elements or Listeners
With an instantiated core, elements and listeners may be added with the `AddUiElement` and `AddListener` methods. Their usage is straightforward, but note that the objects are mutated and IDs are written into the objects passed as arguments. See [IDs](#IDs)

## Removing Elements or Listeners
Removal of stated elements and listeners is achieved by usage of the IDs given to the elements. Therefor, IDs should be stored manually. The core itself performs a sort of storage by ID, but this is only used for internal validation. Your game should always manage the virtual UI elements and listeners itself if they are supposed to be removed eventually, and one by one. 

For a fast clear of all listeners and elements, [Sessions](#Sessions) may be used. Opening and closing a session results in a full server-side clear. Beware that this will also affect all other clients currently communicating with the server. 

## IDs
Elements and Listeners are identified by their unique IDs. These IDs are given by the server. Methods that directly add or remove a single element or listener will always write these IDs in the element or listener provided to them, instead of returning them. They will modify the given object. 

## Sessions
The API allows a form of raw sessioning. Sessions are a comfortable way to generated [`REPLACE_STATUS`](Configuration-Request-Types.md#Replace Status) message. Whenever a session is opened, a new status is created with each call of [`ADD_ELEMENT`](Configuration-Request-Types.md#Add Element), which can then be sent to the server by flushing or closing the session. 

This feature is still work-in-progress, as there is currently no way to retrieve the IDs of elements and listeners of the newly generated status. It should only be used in combination with static UIs.

## Internal methods
Methods declared internal are not for API usage!

# Potential problems and suggested solutions
## Dynamic UIs 
User interfaces are usually not inherently static. Usually, your game has sever layers of recursion in a single UI, or different interfaces itself that are displayed conditionally. In theory, the protocol implements a straightforward mechanism for this kind of dynamic - the [`REPLACE_STATUS`](Configuration-Request-Types.md#Replace Status) request. Unfortunately, this there is - due to a misconception in the initial protocol - no possibility to keep track of the elements added. An example:

The following status was trasmitted as a replacement

* Element(id = ?, dimensions1)

* Element(id = ?, dimensions1)

* Element(id = ?, dimensions2)

There are also various listeners attached to these elements.
The server would set the IDs to these, but the initial order of the elements is not guaranteed to be stable. Therefore, elements might be "misplaced".

Because of this limitation, smooth usage of status replacement is only possibly if IDs do not matter, which is the case when elements do not have to be removed one by one. For most cases of dynamic UIs, this should be the case. 

As an example, we would have an interface that has the following structure:

Main

* Start
* Options
* Highscore
* Quit

Options

* Option1
* Option2
* Back

Highscore

* Back

As a result, a management class would store 3 `UIStatusAdapter` objects that are initialized correctly and are mapped to the given Unity interface. Whenever a change is necessary, a replace status message would be sent. This allows "hotswapping" of entire user interfaces. 

## Moving UI elements
Most UIs have some sort of animation attached to their functionality. If an UI is animated, there are two things to consider:

* While my UI is moving, do I really want it to be interactable?
* How accurate does my moving UI has to be?

Usually it won't be necessary to interact with an ui element while it is moving or animated. And most of the time, accuracy of animated UIs can be ignored.
As FUICS implements a client/server model, and the packages communication is kept strictly synchronous, consider not making your interface interactble during the animation. And if it really has to, do not perform an update during each `Update()` or `FixedUpdate()`. 
If an update is necessary, the element can be "updated" by calling `RemoveElement` and `AddElement` right after. There is no `UpdateElement` functionality in the protocol.

If there are a lot of elements that are moving and need updating, the network communication will create a hard bottleneck effect, due to it's synchronous nature. If this becomes the case, the FUICSCore has to be wrapped into a capsule class, and the capsule executed in a seperate thread. This should not provide any issues. 

## Normalization