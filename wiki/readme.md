# Getting started #

## Unity Package ##
The Unity package provides dead simple, easy access to top-level UI functionality of fuics. It is intended to work out-of the box, without further installation. 

Initial setup for basic button / ui functionality can be done in a few minutes and fully integrates into Unity's existing Event System. 

A more detailed package API implements low-level communication with the server and allows more sophisticated logic, which is, in theory, able to fully replace Unity's event system, although doing so is *strongly* advised against.

[Read More...](Unity-Package.md)

## Examples ##

Two very simple example games implemented in Unity exist to illustrate usage of the FUICS Unitypackage. Their source code can be found in the repository, the compiled examples are available in the projects folder of the vr-tools server and can be playtested in the CAVE environment. 

# Further Information#
* [Server](Server.md) 
* [Protocol: Configuration](Configuration-Protocol.md) for Information about the configuration protocol
* [Protocol: Event](Event-Protocol.md) for information about the event protocol
* [Development](Development.md) for important development information