# Default Response
As a default response, the server sends back a copy of the received message. Only relevant fields are changed in the response depending on the server's state.

# ASSIST

## Request 
Allows the Client to request Calibration assistance from the Server. The dimensions field may be set to overwrite the default set by the server. 

## Response
The Dimensions field will contain the new, modified dimensions for the feet calibration.
The [Server's](Server.md) answer will have it's request type set to Assist and it's ConfigurationError set to:
* `ERR_NOERR` when no error occurred and the assist was properly enabled serverside
* `ERR_UNKNOWN` when an error occurred and the assist could not be enabled. For now, refer to the log file for a detailed error description and a stack trace.


# UPDATE_DIMENSIONS

## Request
Requests update of the feet coordinates, including the feet width and height.
Only callable when the UI is currently in assist status. This allows modification of the feet colliders, represented by their position and width/height.

Next to the request type, the Dimensions field has to be initialized with delta values. Example
Current Feet Dimensions: 0.0/1.0/1.0 5.0/5.0
Target Feet Dimensions: 1.0/1.0/1.0 5.0/3.0
Delta given in the Request: 1.0/0.0/0.0 0.0/0.0/-2.0

## Response
Upon success, the server will return `ERR_NOERROR` as error code. The Dimensions field will contain the new, modified dimensions for the feet calibration.

If an error occurred, one of the following error codes may be set:

* `ASSIST_INACTIVE` if calibration assist is inactive.
* `ERR_UNKNOWN` if an unknown error occurred during processing. This is likely caused by an unhandled exception, refer to the servers log for more information

# FINALIZE

## Request
Allows the client to finalize Calibration assistance from the Server. There are no further parameters required. The current tracking data is saved for the calibration to be used later when calculating the transformed feet rectangles after they move from their calibrated position and change orientation

## Response
The [Server's](Server.md) answer will have it's request type set to finalize and it's ConfigurationError set to:

* `ERR_NOERR` when no error occurred and the assist was properly enabled serverside
* `ERR_UNKNOWN` when an error occurred and the assist could not be enabled. For now, refer to the log file for a detailed error description and a stack trace.

# Default Rectangles
The default rectangles start without any visible area and with the following dimensions

* `x = CaveDimensions::midXCoordinate`
* `y = CaveDimensions::midYCoordinate`
* `z = CaveDimensions::minZCoordinate`
* `width = 0`
* `height = 0`

![assist without dim-field.png](pictures/assist without dim-field.png)

# Delta Update
when a configurationmessage of type ASSIST or UPDATE_DIMENSIONS is received, the currently set rectangles are updated with the dimensions of the dimensions message field. The rectangles are mirrored at the y-axis and therefore change like this:

## Right Rectangle
* `x += deltax`
* `y += deltay`
* `z += deltaz`
* `width += deltawidth`
* `height += deltaheight`

## Left Rectangle
* `x -= deltax`
* `y += deltay`
* `z += deltaz`
* `width -= deltawidth`
* `height += deltaheight`

![assist with dim-field.png](pictures/assist with dim-field.png)

# Example: new user

* Server receives an ASSIST-message without the dimensions field set
* Server starts with default Rectangles with starting point at the cave center on the floor
* Server receives numerous UPDATE_DIMENSIONS-messages with delta-offsets until the rectangles fit around the user's feet
* Server receives FINALIZE-message to finish the calibration process

# Example: user who knows their correct calibration
* Server receives an ASSIST-message with the dimensions field set to the user's personal calibration
* Server directly applies the changes to the starting Rectangles
* User now has to place his/her feet inside the already correctly set rectangles
* Server receives FINALIZE-message to finish the calibration process

# Rendering:
See [Debug Rendering](Server.md#debug-rendering)