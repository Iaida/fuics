# Getting Started - Unity Package 

This tutorial illustrates a simple way to get started with the Unity packages high level component. This assumes that the fuics server is already running on either one of the CAVE computers or a different linux-device that is reachable within the same network. This guide will use Unity's default Layout. 

# Importing the Package

Once the package is downloaded, unzip it anywhere you see fit and open your Unity project. Within the ***Project*** tab, right click anywhere and and ***Import Package*** -> ***Custom Package***. Locate the folder you extracted the release to, and import the ***FUICS.unitypackage***. On the following dialogue, all assets from the package are visible. 

The following dependencies may be excluded:

* UnityTestTools

* Tests 

* Plugin/NSubstitute.dll

as these are only required for running the tests. Import all other dependencies. They will be added to the ***FUICS*** folder within your assets folder.

**Important**: There will be a compiler error with Unity's Default settings that is caused by Log4Net. To fix this problem, go to ***Edit->Project Settings->Player->Other settings*** and set ***Api Compatibility Level*** to ***.NET 2.0*** instead of ***.NET 2.0 Subset***. This will increase your games size and will remove good compatibility with smartphones... but then, when using FUICS, the target system is probably CAVE.

# Adding the component and configuration

In the project browser, go to ***FUICS/components*** and drag the ***HighLevelComponent*** into your scene. (Note: There are other ways to use FUICS, but this is the most basic). Open Unity's Inspector on the component within the scene. You will see the following:

![fuicsCompGuide.PNG](pictures/fuicsCompGuide.PNG)

* The ***Hostname*** has to be changed to the CAVE computer running the FUICS server instance

* The ***Port*** field defines the port used for Calibration. Leave this as it is, unless the server was changed

* The ***Log File Name*** will be the file to which [https://logging.apache.org/log4net/](Log4Net) writes all log output. This log file will contain important information if something goes haywire, and may help you troubleshoot problems

* The ***Lib Log Level*** defines which events will be Logged out. The FUICS package is quite verbose, so consider changing it to ***None*** to silence it.

* The *** Buttons*** array has to be filled with the buttons you want to have mapped to to the fuics server. Buttons placed here are now essentially controlled by the fuics server. Events will be forwarded from the server via the package into Unity's event system. There are two fields to set correctly.
**Important** The button's scale has to be set to (1/1/1) and no parent components that alter the scale may be present. If this is the case, the package will generate wrong virtual buttons.

    * Unity Button is a reference to a button within your current scene

    * Fuics Event Type defines *when* the button is clicked by FUICS. For example, press means that the button is
pressed when a foot collides with a button and there was no collision during the previous iteration.

* ***Initialize Immediately*** and ***Start Listening Immediately*** need to be checked. This is the default configuration that will instantly configure the UI with the server upon launch. It will also start listening for server-sided events right after configuration. 

# Done
Once the configuration is done, FUICS will be initialized within the `FuicsHighLevelAPI.Start()` method. Events are checked each time `Update()` is called by Unity. Entering the CAVE with the correct trackers (2 for left foot, 4 for right foot) will now invoke button presses when the respective foot collides with a button.