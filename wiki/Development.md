# Purpose 
Purpose of this page is to provide a general overview over all work that has been done, over important design decisions, structure and internal processes. It is intended to extend the already existing inline documentation and complement it.

## Client-Server: An extended description
### Language Barrier and Protocol Buffers
FUICS was intended to integrate into the existing CAVE enviroment without adding libraries to your project. Communication is based around TCP and Googles message format [Protocol Buffers](https://developers.google.com/protocol-buffers) and is platform independent. This allows integrating FUICS into your project without having to worry about languages barriers. The only requirement is that the existing protocol can be compiled into the language of your choice. As of Mar 2017, the following languages are supported

* C++
* Java
* C#(.NET 2.5 only with reservations, release pending)
* Javascript (Release pending)
* Go
* Pyhton

For some of these languages, precompiled libararies for the target version (3.2) exist, for some, manual compilation is required. C++ (linux) and .NET compiled libararies can be found within the FUICS repo. 

### The Client-Server architecture
![CSStructure.png](pictures/CSStructure.png)

The above graphic shows a simplified version of the general architecture that FUICS is built with. There are two main components:

* The server, which is hosted on any computer within the same network as the CAVE computers. The server will interprete the tracking data it receives via UDP and translates it into collisions and events. 
* The client, which is running on any CAVE computer. It actively communicates with the server to set up a configuration, while constantly receiving events from the server. 


In order to function properly, the server needs to actually receive tracking data from DTrack. To make this possible, either have the server running on one of the CAVE PCs or have the DTrack system sent the tracking data to the servers host (the 5th target address within the DTrack configuration may be used for this purpose). 


The server itself is able to have multiple clients connected to it, and is, in theory, able to work with concurrency, but as the server itself isn't distinguishing between the clients, the clients itself need to know that 'they are not alone', which means that [Replace Status](Configuration-Protocol.md#Replace-Status) should be avoided. 


There are two ports on which the server listens:

* TCP Port 5000, which is used for [Configuration](Configuration-Protocol.md). A single connection is intended to be used once: Open a connection, send the configuration, wait for an answer and then close the connection, although the server is able to handle persistent connections, too.
* TCP Port 5001, which is used for [Events](Event-Protocol.md). This connection is established by the client and is persistent. Once connected, the server will start sending **all** events to the client, even those that are meant for another connected client. Some sort of filtering has to be performed client-side. An example of this filtering can be found within the [Unity Package](Unity-Package.md).

### Implementing the Protocol
As stated before, the advantage of this client-server architecture is the possibility to write your own implementations of the defined protocol. To avoid problems, the following steps should be done before implementing the protocol:

* Read the protocol documentations! The [Events](Event-Protocol.md) and [Configuration](Configuration-Protocol.md) pages are relevant
* Compile the protocol buffer messages. The following versions were used during development, and should be used:
    * Protocol Buffers v3.2 C++ for unix32 and unix64 in server/lib
    * Protocol Buffers v3.2 for Java via Maven: https://search.maven.org/#artifactdetails%7Ccom.google.protobuf%7Cprotobuf-java%7C3.2.0%7Cbundle
    * Protocol Buffers for C# 32 and 64 bit. Release pending. Commit 17174b54ddd040a326dec6db75d1bfb5e5b3caa9 of https://github.com/google/protobuf/tree/master/csharp was used and is working properly
    * For any other language, the v3.2 should work as intended.  

## Unity Package
The Unity package is a reference implementation of the protocol. It provides the protocols main functionality in an easy to use way, and may be used to in games or as reference for own protocol implementations. For a general overview of the package, refer to [Unity Package](Unity-Package.md) and the generated documentation or inline comments. 

## Java Client
A Client that implements the protocol written in Java exists for debugging and development purposes. During a rework of our protocol, the client became unusable as the generated protobuf code is no longer compiling. For further development, the protocol should be recompiled to java and the client made functional. 

The client itself is a maven project with all dependencies defined as maven dependencies. To change the UI, IntelliJs GUI tool is recommended, as the source code for the swing UI is autogenerated.

## Server
See [Server](Server.md)