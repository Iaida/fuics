# Server Debug Window Screenshots

## Game running
![RememberIt debug window.png](pictures/RememberIt debug window.png)

* Feet in green with L for left foot and R for right foot
* Buttons with their unique IDs

## Calibration assist
![after pressing a for assist.png](pictures/after pressing a for assist.png)

* Feet in green: L for left foot and R for right foot
* Rectangles in red: previous calibration

## Calibration change
![after pressing arrow keys to change dimensions.png](pictures/after pressing arrow keys to change dimensions.png)

* Feet in green: L for left foot and R for right foot
* Rectangles in red: previous calibration
* Rectangles in blue: current changes made to previous calibration

## Calibration finalize
![after pressing f for finalize.png](pictures/after pressing f for finalize.png)

* Feet in green: L for left foot and R for right foot, now overlap with the previously blue calibration rectangles