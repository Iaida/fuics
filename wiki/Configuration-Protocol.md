# Overview #
The configuration protocol is used initially set up the UI-Server, change the underlying logical layout, initialize calibration assist and print out the status of the underlying data structure.

Messages sent between client and server need to be prefixed with a fixed, 4 byte sized header and need a trailing end-of-transmission(^D) character. The header defines the length of the following protocol buffer message, and contains 4 bytes of little endian encoded integer. Note: Most socket implementations provide an automated end-of-transmission character. Multiple EOT characters might result in unwanted behavior.

![drawio_protobuf_message_structure.png](pictures/drawio_protobuf_message_structure.png)

# Protocol Buffer Message #
The configuration message is intended as client-based communication. The client will send a request to the server, the server will answer. Although the TCP connection held open by the server will be persistent, the server will not send any packages other than direct answers.

The message itself is structured by different message types:

* [ASSIST](Calibration.md#ASSIST)
Requests calibration assistance. The server will project two rectangle onto the CAVE floor, which the user can then use to calibrate the tracking.

* [UPDATE_DIMENSIONS](Calibration.md#UPDATE_DIMENSIONS)
Only accepted after ASSIST and before FINALIZE. Sending a message with the `dimensions`-field set will change the current calibration. For more details, please refer to the “Calibration” section.

* [FINALIZE](Calibration.md#FINALIZE)
Requests finalization of the calibration. The current calibration values are written into the current UI configuration.


 
* [Debug](Configuration Request Types.md#Assist)
Requests full debug information of the UIStatus. This includes the following:
    * The current virtual canvas dimensions. 
    * All virtual UI Elements, including their type and dimensions
    * For each virtual UI Element, it’s attached listeners

 


* [Add Element](Configuration-Request-Types.md#Add-Element)

* [Remove Element](Configuration-Request-Types.md#Remove-Element)

* [Replace Status](Configuration-Request-Types.md#Replace-Status)

* [Add Listener](Configuration-Request-Types.md#Add-Listener)

* [Remove Listener](Configuration-Request-Types.md#Remove-Listener)

The other fields vary based on request type and message type(request or response).

The error field is used by the server to define if there were logical, not syntactic errors in the received message.

# Distributing IDs #
IDs are distributed by the server where each id is unique for its type(Element, Listener). IDs start at 1 to differentiate between the default(0) and set IDs inside a protobuf message.