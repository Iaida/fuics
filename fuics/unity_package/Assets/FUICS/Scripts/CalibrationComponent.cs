﻿using System;
using Fhw.Fuics.Wrapper;
using NSubstitute;
using UnityEngine;
using UnityEngine.UI;

namespace Fhw.Fuics
{
    public class CalibrationComponent : MonoBehaviour
    {
        #region InspectorFields
        // == INSPECTOR BASED FIELDS ======================================= //
        // == The following fields are intended for Unity-Inspector based == //
        // == initialization. ============================================== //

        /// <summary>
        /// Key string that is used to start the calibration
        /// </summary>
        public String InputStringStartCalibration = "a";

        /// <summary>
        /// Key string that is used to end the calibration
        /// </summary>
        public String InputStringEndCalibration = "x";

        /// <summary>
        /// Defines if <see cref="HighLevelApi"/> should be automatically discovered,
        /// or if it is set manually. If autodiscover is enabled, this script
        /// has to be attached to the same component as <see cref="FuicsHighLevelApi"/>
        /// </summary>
        public bool DoAutodiscover = true;

        /// <summary>
        /// Canvas on which the foot images are drawn. Needs to be set.
        /// </summary>
        public Canvas ParentCanvas;

        /// <summary>
        /// Image / Sprite that is used to visualize a foot. Has to be square sized
        /// (1:1 width:height).
        /// If working with a NON-DEFAULT image, set the Anchors to the center
        /// </summary>
        public Image FootImage;

        /// <summary>
        /// Refernece to the component with the a high level API attached.
        /// If the calibration component is the attached to the same gameobject as the API, 
        /// set <see cref="DoAutodiscover"/> to true and use it instead. 
        /// </summary>
        public FuicsHighLevelApi HighLevelApi;

        public DimensionsAdapter InitialDimensions;
        #endregion

        

        private Image _leftFoot;
        private Image _rightFoot;
        

        private bool _calibrationStarted = false;
        
        void Start()
        {
            if (DoAutodiscover)
            {
                HighLevelApi = gameObject.GetComponent<FuicsHighLevelApi>();
                _leftFoot = GameObject.Instantiate(FootImage);
                _leftFoot.transform.SetParent(ParentCanvas.transform, true);
                _leftFoot.rectTransform.sizeDelta = new Vector2(0, 0);
                _leftFoot.rectTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
                _leftFoot.gameObject.SetActive(false);
                _leftFoot.gameObject.name = "Left Foot";

                _rightFoot = GameObject.Instantiate(FootImage);
                _rightFoot.transform.SetParent(ParentCanvas.transform, true);
                _rightFoot.rectTransform.sizeDelta = new Vector2(0, 0);
                _rightFoot.rectTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
                _rightFoot.gameObject.SetActive(false);
                _rightFoot.gameObject.name = "Right Foot";
            }
        }

        private void UpdateFeetRactangles(DimensionsAdapter dimensions)
        {

            
            _leftFoot.rectTransform.sizeDelta = new Vector2(dimensions.Width, dimensions.Height);
            _rightFoot.rectTransform.sizeDelta = new Vector2(dimensions.Width, dimensions.Height);

           
            

            _leftFoot.rectTransform.anchoredPosition
                = new Vector2(dimensions.Pos.x , 
                                dimensions.Pos.y);
            float distanceTo = Screen.width / 2.0f - _leftFoot.rectTransform.anchoredPosition.x;
            _rightFoot.rectTransform.anchoredPosition
                = new Vector2(Screen.width / 2.0f + distanceTo - dimensions.Width,
                                dimensions.Pos.y);

           
        }


        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(InputStringStartCalibration) && !_calibrationStarted)
            {
                _calibrationStarted = true;
                _leftFoot.gameObject.SetActive(true);
                _rightFoot.gameObject.SetActive(true);
                UpdateFeetRactangles(InitialDimensions);
                HighLevelApi.StartCalibration(InitialDimensions);
                
            }
            if (_calibrationStarted && Input.GetKeyDown(InputStringEndCalibration))
            {
                _calibrationStarted = false;
                _leftFoot.gameObject.SetActive(false);
                _rightFoot.gameObject.SetActive(false);
                HighLevelApi.EndCalibration();
            }
        }
    }
}
