﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fhw.Fuics.Wrapper
{
    internal class EventMessageAdapter
    {
        public Int32 ListenerId { get; set; }
        public Int32 ElementId { get; set; }
        public string Info { get; set; }
        public EventType EventType { get; set; }

        /// <summary>
        /// Default Empty constructor for Unity
        /// </summary>
        internal EventMessageAdapter() {}

        internal EventMessageAdapter(EventMessage msg)
        {
            ListenerId = msg.ListenerId;
            ElementId = msg.ElementId;
            Info = msg.Info;
            EventType = msg.EventType;
        }
    }
}
