﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fhw.Fuics.Wrapper
{
    public class UiStatusAdapter
    {
        internal float CanvasHeight { get; set; }
        internal float CanvasWidth { get; set; }

        public List<UiElementAdapter> Elements { get; set; }

        internal UiStatusAdapter()
        {
            Elements = new List<UiElementAdapter>();
        }


        public UiStatusAdapter(float canvasHeight, float canvasWidth, List<UiElementAdapter> elements)
        {
            CanvasHeight = canvasHeight;
            CanvasWidth = canvasWidth;
            Elements = elements;
        }


        internal UiStatusAdapter(UIStatus status)
        {
            CanvasHeight = (float) status.CanvasHeight;
            CanvasWidth = (float) status.CanvasWidth;
            Elements = new List<UiElementAdapter>();
            foreach (var uiElement in status.Elements)
            {
                Elements.Add(new UiElementAdapter(uiElement));
            }
        }

        internal UIStatus ToProtobuf()
        {
            UIStatus result = new UIStatus
            {
                CanvasHeight = CanvasHeight,
                CanvasWidth = CanvasWidth
            };
            foreach (var element in Elements)
            {
                result.Elements.Add(element.ToProtobuf());
            }
            return result;
        }

        public void AddElement(UiElementAdapter element)
        {
            Elements.Add(element);
        }

        public List<ListenerAdapter> GetAllListeners()
        {
            List<ListenerAdapter> result = new List<ListenerAdapter>();
            Elements.ForEach(x =>
            {
                result.AddRange(x.Listeners);
            });
            return result;
        }
    }
}
