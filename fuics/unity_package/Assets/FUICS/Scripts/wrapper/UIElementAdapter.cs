﻿using System.Collections;
using System.Collections.Generic;
using Fhw.Fuics.Exception;
using JetBrains.Annotations;

namespace Fhw.Fuics.Wrapper
{
    /// <summary>
    /// A fuics UI element that represents a logical 
    /// </summary>
    public class UiElementAdapter
    {
        /// <summary>
        /// The dimensions of the element. 
        /// </summary>
        public DimensionsAdapter DimensionsAdapter { get; set; }
        /// <summary>
        /// Id of this element. This ID is set by the server and overwritten during persistence operations. 
        /// Setting this will have no effect on the server side. 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Type of this element. The type defines how <see cref="DimensionsAdapter"/> is interpreted. 
        /// </summary>
        public ElementTypeAdapter Type { get; set; }

        /// <summary>
        /// The listeners of this element. An element may have arbitary listeners. Listeners may also repeat themselves.
        /// </summary>
        [NotNull]
        public List<ListenerAdapter> Listeners = new List<ListenerAdapter>();

        /// <summary>
        /// Empty constructor for testing and serialization. 
        /// </summary>
        public UiElementAdapter()
        {
            
        }

        public UiElementAdapter(DimensionsAdapter dimensionsAdapter, int id, ElementTypeAdapter type, List<ListenerAdapter> listeners)
        {
            DimensionsAdapter = dimensionsAdapter;
            Id = id;
            Type = type;
            Listeners = listeners;
        }

        /// <summary>
        /// Constructs this object from the given protobuf object. 
        /// </summary>
        /// <param name="element"></param>
        internal UiElementAdapter(UIElement element)
        {
            DimensionsAdapter = new DimensionsAdapter(element.Dimensions);
            Id = element.Id;
            Type = ElementTypeAdapterMethods.FromProtobuf(element.Type);
            Listeners = new List<ListenerAdapter>();
            foreach (Listener listener in element.Listeners)
            {
                Listeners.Add(new ListenerAdapter(listener));
            }
        }

        /// <summary>
        /// Adds a listener to this ui Element. This operation does not invoke any network operation, adding a listener to this adapter
        /// will not invoke any ui server changes unless explicitly committed to the server. 
        /// <para>
        /// This will also overwirte the <see cref="ListenerAdapter.ParentElement"/> field.
        /// </para>
        /// </summary>
        /// <param name="listener"></param>
        public void AddListener(ListenerAdapter listener)
        {
            listener.ParentElement = this;
            Listeners.Add(listener);
        }

        public void RemoveListener(int id)
        {
            if(Listeners.RemoveAll(x => x.Id == id) <= 0)
                throw new InternalConstraintViolation("Listener with id " + id + " not found in listeners");
        }

        internal global::UIElement ToProtobuf()
        {
            global::UIElement result = new global::UIElement();
            result.Id = Id;
            result.Dimensions = DimensionsAdapter.ToProtobuf();
            foreach (ListenerAdapter listener in Listeners)
            {
                result.Listeners.Add(listener.ToProtobuf());   
            }
            result.Type = Type.ToProtobuf();
            return result;
        }
    }
}
