﻿using UnityEngine;

namespace Fhw.Fuics.Wrapper
{
    /// <summary>
    /// Encapsulates basic dimensionsAdapter.
    /// <p>
    /// DimensionsAdapter consist of a <see cref="Vector3"/> that encapsulates x, y and z dimensionsAdapter, and two doubles containing
    /// an additional width and height.
    /// The values interpretion depends on the element the dimensionsAdapter are attached to. 
    /// </p>
    /// </summary>
    [System.Serializable]
    public class DimensionsAdapter
    {
        [SerializeField] public Vector3 Pos;

        [SerializeField] public float Height;

        [SerializeField] public float Width;

        public DimensionsAdapter()
        {
            
        }
        public DimensionsAdapter(Vector3 pos, float height, float width)
        {
            Pos = pos;
            Height = height;
            Width = width;
        }

        public DimensionsAdapter(global::Dimensions dimensions)
        {
            Height = dimensions.Height;
            Width = dimensions.Width;
            Pos = new Vector3((float)dimensions.X, (float)dimensions.Y, (float)dimensions.Z);
        }

        /// <summary>
        /// Creates the protocol buffers object that is associated with this class.
        /// All fields will be set, there is no validation, invalid data is forwarded to the protobuf serializer.
        /// <p>This should only be used within the network layer. Do not work with original, auto generated protobuf classes outside of direct network communcation.</p>
        /// </summary>
        internal Dimensions ToProtobuf()
        {
            global::Dimensions result = new global::Dimensions();
            result.X = Pos.x;
            result.Y = Pos.y;
            result.Z = Pos.z;
            result.Height = Height;
            result.Width = Width;
            return result;
        }

        
        public override string ToString() 
        {
            return "Dimensions [" +Pos.x +"/" + Pos.y +"/" + Pos.z +"/" + Width +"/" + Height +"]";
        }



    }
}
