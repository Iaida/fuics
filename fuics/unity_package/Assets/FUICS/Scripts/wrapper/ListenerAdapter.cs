﻿
namespace Fhw.Fuics.Wrapper
{
    public delegate void OnFuicsEventCallback(string info); 

    public class ListenerAdapter
    {
        /// <summary>
        /// Id of this listener. This value is usually set by the server. Setting this value before a persistence
        /// operation has no effect, as the ID is overwritten.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Additional info that may be given to this listener. This will be available on the server, too. May be used to encapsualted 
        /// arbitary information necessary for this listener. 
        /// </summary>
        public string Info { get; set; }

        /// <summary>
        /// Listener listens to this event. This is no real observer/observeable. This should be used to filter incoming events. 
        /// </summary>
        public EventTypeAdapter ListensTo { get; set; }

        /// <summary>
        /// Reference to the <see cref="UiElementAdapter"/>
        /// to which this listener is attached. Needs to be maintained by this
        /// library, as it is not maintained by the server.
        /// </summary>
        public UiElementAdapter ParentElement { get; set; }

        /// <summary>
        /// Delegate called when this listener is invoked. Used for library internal processing.
        /// </summary>
        internal InternalInvokeButtonClick OnTriggerInternal;

        /// <summary>
        /// Delegate called when this listener is invoked. Passes this listeners info string as argument. Use this
        /// for non-internal purposes, directly reflects the protocol. 
        /// </summary>
        public OnFuicsEventCallback OnEventTriggered;

        /// <summary>
        /// Empty constructor; Used for test and serialization. 
        /// </summary>

        public ListenerAdapter()
        {
            
        }


        public ListenerAdapter(int id, string info, EventTypeAdapter listensTo)
        {
            Id = id;
            Info = info;
            this.ListensTo = listensTo;
        }

        /// <summary>
        /// Constructs this object from an already available protobuf object. 
        /// </summary>
        /// <param name="listener"></param>
        internal ListenerAdapter(Listener listener)
        {
            Id = listener.Id;
            Info = listener.Info;
            ListensTo = EventTypeAdapterMethods.ToAdapter(listener.ListensTo);
        }

        /// <summary>
        /// Genereates a <see cref="Listener"/> from this adapter. 
        /// </summary>
        /// <returns></returns>
        internal Listener ToProtobuf()
        {
            Listener  result = new Listener()
            {
                Id = Id,
                Info = Info,
                ListensTo = ListensTo.ToProtobuf()
            };

            return result;
        }

        /// <summary>
        /// Invokes the listeners that are attached to this listener. 
        /// </summary>
        public void Invoke()
        {
            if (OnTriggerInternal != null)
            {
                OnTriggerInternal();
            }

            if (OnEventTriggered != null)
            {
                OnEventTriggered(Info);
            }
           
        }
    }
}
