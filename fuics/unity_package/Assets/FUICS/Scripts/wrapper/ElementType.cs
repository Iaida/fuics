﻿using System;
using System.Runtime.CompilerServices;

namespace Fhw.Fuics.Wrapper
{
    public enum ElementTypeAdapter  {
        Rectangle
    }

    public static class ElementTypeAdapterMethods
    {
        public static string GetDisplayString(this ElementTypeAdapter elementTypeAdapter)
        {
            switch (elementTypeAdapter)
            {
                case ElementTypeAdapter.Rectangle:
                    return "Rectangle";
                default:
                    throw new ArgumentOutOfRangeException("elementTypeAdapter", elementTypeAdapter, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elementTypeAdapter"></param>
        /// <returns></returns>
        internal static ElementType ToProtobuf(this ElementTypeAdapter elementTypeAdapter)
        {
            switch (elementTypeAdapter)
            {
                case ElementTypeAdapter.Rectangle:
                    return ElementType.Rectangle;
                default:
                    throw new ArgumentOutOfRangeException("elementTypeAdapter", elementTypeAdapter, null);
            }   
        }

        internal static ElementTypeAdapter FromProtobuf(ElementType elementType)
        {
            switch (elementType)
            {
                case ElementType.Rectangle:
                    return ElementTypeAdapter.Rectangle;
                default:
                    throw new ArgumentOutOfRangeException("elementType", elementType, null);
            }
        }
    }

   
}
