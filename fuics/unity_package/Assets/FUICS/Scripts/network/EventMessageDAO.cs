﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Castle.Core.Internal;
using Fhw.Fuics.Wrapper;
using log4net;
using log4net.Core;
using UnityEngine;

[assembly: InternalsVisibleTo("Assembly-CSharp-Editor")]
[assembly: InternalsVisibleTo(InternalsVisible.ToDynamicProxyGenAssembly2)]

namespace Fhw.Fuics.Network
{
    /// <summary>
    /// Provides a capsule around the event message protocol and allows
    /// a persistent connection to the remote server to receive a continues stream of event messages. These event messages
    /// should be read from the socket on a regular base. 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    internal class EventMessageDAO
    {
        
        private IPEndPoint fuicsEndPoint;

        private NetworkStream networkStream;

        private Socket _socket;

        private static readonly Int32 EVENT_MSG_HEADER_SIZE = 4;

        private static readonly ILog Log = LogManager.GetLogger(typeof(EventMessageDAO));

        internal EventMessageDAO()
        {
            
        }

        internal EventMessageDAO(String hostname, int port)
        {
            IPHostEntry host = Dns.GetHostEntry(hostname);
            IPAddress ipAdress = host.AddressList[0];
            fuicsEndPoint = new IPEndPoint(ipAdress, port);
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
        }

        /// <summary>
        /// Opens a connection to the configured remote endpoint 
        /// </summary>
        internal void Open()
        {
            Log.Info("Opening connection to endpoint " + fuicsEndPoint);
            _socket.SendTimeout = 500;
            _socket.ReceiveTimeout = 500;
            _socket.Connect(fuicsEndPoint); 
            networkStream = new NetworkStream(_socket);
        }

        /// <summary>
        /// Closes the connection to the configured remote endpoint
        /// </summary>
        internal void Close()
        {
            Log.Info("Closing connection.");
            networkStream.Close();
        }

        /// <summary>
        /// Reads all currently available event messages from the buffer. Evnets that have been completely read are removed
        /// from the sockets buffer. 
        /// </summary>
        /// <returns></returns>
        public virtual List<EventMessageAdapter> ReadEvents()
        {
            List<EventMessageAdapter> result = new List<EventMessageAdapter>();
            if (networkStream != null)
            {
                while (networkStream.DataAvailable)
                {
                    Int32 msgSize = parseHeader(networkStream);
                    result.Add(new EventMessageAdapter(parseEventMessage(networkStream, msgSize)));
                }
            }
            if (result.Count > 0)
            {
                Log.Debug("Events read: " + result.Count);
            }
            return result;
        }

        /// <summary>
        /// Attempts to parse 4 Byte from the given stream and interpretes it as 32bit integer. 
        /// If not enough bytes are available, an exceptions is thrown. 
        /// </summary>
        /// <returns></returns>
        private Int32 parseHeader(NetworkStream networkStream)
        {
            byte[] header = new byte[EVENT_MSG_HEADER_SIZE];
            networkStream.Read(header, 0, EVENT_MSG_HEADER_SIZE);
            return BitConverter.ToInt32(header, 0);
        }

        private EventMessage parseEventMessage(NetworkStream networkStream, Int32 msgSize)
        {
            byte[] msgAsBytes = new byte[msgSize];
            networkStream.Read(msgAsBytes, 0, msgSize);
            return EventMessage.Parser.ParseFrom(msgAsBytes);
        }

        
    }
}
