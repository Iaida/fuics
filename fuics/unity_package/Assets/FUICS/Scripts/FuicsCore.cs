﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Fhw.Fuics.Exception;
using Fhw.Fuics.logging;
using Fhw.Fuics.Network;
using Fhw.Fuics.Wrapper;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;

[assembly: InternalsVisibleTo("Assembly-CSharp-Editor")]

/// <summary>
/// Delegate describing a Unity button click invocation. Only for library internal use.
/// </summary>
internal delegate void InternalInvokeButtonClick();


namespace Fhw.Fuics
{
    /// <summary>
    /// Provides core functionalties for fuics server access. 
    /// <para>
    /// The core serves as low-level API for client/server communication and does not implement any of Unity's methods. This
    /// clearly seperates it from Unity, giving full control to the developer over functionality and timing. In theory, this could
    /// be used to create a concurrent event system to Unitys, without ever synchronising them. 
    /// </para>
    /// <para>
    /// <see cref="FuicsCore"/> is designed as a variable set of singletons. Each core may only be unique per hostname. This allows
    /// accessing a core for a given hostname from anywhere within the code. Usage of singletons is an easy, straightforward way to
    /// break through Unity's component structure and allow more flexibility - no need to create a new component for it and drag the 
    /// references with the Inspector.
    /// <break/>
    /// Cores should, therefor, only ever be created and accessed by usage of <see cref="CreateCore"/>. The constructor is private by 
    /// default, this should not be changed. Multiple cores for the same hostname could potentially create problems. 
    /// </para>
    /// <para>
    /// The core tracks all <see cref="UiElementAdapter"/>s and <see cref="ListenerAdapter"/>s added through it for the user. Note that IDs
    /// are defined and set by the server. This behaviour is simulated within this package. Whenever adding an object - listener or element 
    /// - it's state is mutated and the ID written into the object passed as argument. An example:
    /// <break/>
    /// <code>
    /// FuicsCore core = FuicsCore.CreateCore("localhost");
    /// UiElementAdapter element = new UiElementAdapter;
    /// core.AddElement(element):
    /// Debug.Log(element) // This will now log out the ID assigned by the server. 
    /// </code>
    /// This described behaviour is not working with sessions. 
    /// </para>
    /// <para>
    /// Events are generated and forwarded for each listener that is tracked by the core, but the event processing
    /// needs manual triggering. It is encouraged to integrate this into Unitys exisiting behaviours, like FixedUpdate or
    /// Update.
    /// </para>
    /// The core provides functionality to work with sessions. Sessions are a simple implementation of status replacement that
    /// are only useful for large, static user interfaces. Each element is not sent seperately, but in a batch <code>ReplaceStatus</code>
    /// request. Note that this will replace the elements provided by <see cref="AddUiElement"/>, and IDs are not written into
    /// the given object. It will also completely erase the remote status. 
    /// </summary>
    public class FuicsCore
    {
        /// <summary>
        /// The port used for configuration protocol. Only change when the server has changed        
        /// </summary>
        private const Int32 ConfigPort = 5000;

        /// <summary>
        /// The port used for event protocol. Only change when the server has changed. 
        /// </summary>
        private static readonly Int32 EventPort = 5001;

        /// <summary>
        /// IP of the CAVE PC that is identified as "down". 
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public static readonly String IP_CAVE_PC_DOWN = "172.16.9.122";
       

        /// <summary>
        /// Stores all created cores. Cores are unique for each hostname. Cores are created
        /// by using <see cref="CreateCore"/>
        /// </summary>
        private static readonly Dictionary<string, FuicsCore> Cores = new Dictionary<string, FuicsCore>();

        /// <summary>
        /// Map of all UiElements currently managed by the core. Elements registered <em>by this</em> core added to the dictionary, and elements
        /// removed by this core removed from it. 
        /// </summary>
        private readonly Dictionary<int, UiElementAdapter> _elements = new Dictionary<int, UiElementAdapter>();

        /// <summary>
        /// Stores all listeners that are currently managed by this core. 
        /// </summary>
        /// <returns></returns>
        private readonly Dictionary<int, ListenerAdapter> _listeners = new Dictionary<int, ListenerAdapter>();


        private bool _sessionOpen = false;

        /// <summary>
        /// The virtual canvas present on the server. Upper width boarder. Correctly initialized
        /// during creation. 
        /// </summary>
        private float _fuicslCanvasWidthHigh = 1.0f;

        /// <summary>
        /// The virtual canvas present on the server. lower width boarder. Correctly initialized
        /// during creation. 
        /// </summary>
        private float _fuicsCanvasWidthLow = 0.0f;

        /// <summary>
        /// The virtual canvas present on the server. Upper height boarder. Correctly initialized
        /// during creation. 
        /// </summary>
        private float _fuicsCanvasHeightHigh = 1.0f;

        /// <summary>
        /// /// The virtual canvas present on the server. lower height boarder. Correctly initialized
        /// during creation. 
        /// </summary>
        private float _fuicsCanvasHeightLow = 0.0f;

        /// <summary>
        /// Used to handle network actions for configuration messages
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        // ReSharper disable once InconsistentNaming
        private ConfigurationMessageDAO configurationMessageDao;


        /// <summary>
        /// Used to handle network actions for event messages. Peristent TCP connection
        /// </summary>
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private EventMessageDAO eventMessageDao;

        /// <summary>
        /// Status into which session based operations are written. 
        /// </summary>
        private UiStatusAdapter _currentStatus;

        /// <summary>
        /// Used to generate client-side IDs, which are necessary to track listeners through
        /// replace status operations. 
        /// </summary>
        private static int _currentId = 200;

        private static readonly ILog Log = LogManager.GetLogger(typeof(FuicsCore));

        /// <summary>
        /// Only allows private construction to avoid duplicated cores connected to the same host.
        /// </summary>
        private FuicsCore(string hostname)
        {
            configurationMessageDao = new ConfigurationMessageDAO(hostname, ConfigPort);
            eventMessageDao = new EventMessageDAO(hostname, EventPort);
        }

        public static int NextId()
        {
            return ++_currentId;
        }

        /// <summary>
        /// Initializes the size of the virtual canvas - to which normalization will be performed - with the values
        /// defined by the server. This performs a synchronous network accesss.
        /// </summary>
        public void InitCanvasDimensions()
        {
            UiStatusAdapter initialStatus = configurationMessageDao.GetStatus();
            _fuicsCanvasHeightHigh = initialStatus.CanvasHeight;
            _fuicslCanvasWidthHigh = initialStatus.CanvasWidth;
            Log.Debug("New Canvas dimensions: " + _fuicsCanvasHeightHigh + " / " + _fuicslCanvasWidthHigh);
        }

        /// <summary>
        /// Creates or retreives a fuics core. If no core for the given hostname exists, a new core
        /// is created. If a core already exists, the instance with the given hostname is returned. This
        /// allows creation of multiple cores while maintaining some sort of Singleton pattern, allowing access to the core globally
        /// and bypasses unitys component structure. 
        /// </summary>
        /// <param name="hostname">The hostname to which both sockets will connect. Has to be a valid hostname</param>
        /// <returns></returns>
        public static FuicsCore
            CreateCore(string hostname)
        {
            FuicsCore result;
            if (!Cores.TryGetValue(hostname, out result))
            {
                result = new FuicsCore(hostname);
                Cores.Add(hostname, result);
            }
            return result;
        }

        /// <summary>
        /// Frees all currently created cores. May be useful for testing purposes. 
        /// <p>
        /// Only clear all cores when there are no references to existing cores anymore. 
        /// </p>
        /// </summary>
        public static void ClearCores()
        {
            Cores.Clear();
        }


        /// <summary>
        /// Simple map function that allows linear mapping from one range into the other. 
        /// </summary>
        /// <param name="oldMin"></param>
        /// <param name="oldMax"></param>
        /// <param name="newMin"></param>
        /// <param name="newMax"></param>
        /// <param name="value"></param>
        /// <returns>Mapped value. </returns>
        private float map(float oldMin, float oldMax, float newMin, float newMax, float value)
        {
            return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
        }

        /// <summary>
        /// Nomralized the given value within the range of [low..high] into FUICS space.
        /// Applies for the x-Dimension/width
        /// <para>
        /// The values for the normalization are initialized during creation. If no server was present
        /// at that time, default values are used which equal a range of [0..1]. 
        /// </para>
        /// </summary>
        /// <param name="x"></param>
        /// <param name="low"></param>
        /// <param name="high"></param>
        /// <returns></returns>
        public float NormalizeDimX(float x, float low, float high)
        {
            return map(low, high, _fuicsCanvasWidthLow, _fuicslCanvasWidthHigh, x);
        }

        /// <summary>
        /// Nomralized the given value within the range of [low..high] into FUICS space.
        /// Applies for the y-Dimension/height
        /// <para>
        /// The values for the normalization are initialized during creation. If no server was present
        /// at that time, default values are used which equal a range of [0..1]. 
        /// </para>
        /// </summary>
        /// <param name="y"></param>
        /// <param name="low"></param>
        /// <param name="high"></param>
        /// <returns></returns>
        public float NormalizeDimY(float y, float low, float high)
        {
            return map(low, high, _fuicsCanvasHeightLow, _fuicsCanvasHeightHigh, y);
        }

        /// <summary>
        /// Removes the listener with the given id.  The Listener is only removed
        /// if it was previously added by this cores instance. 
        /// </summary>
        /// <param name="id"></param>
        public void RemoveListener(Int32 id)
        {

            if (_listeners.ContainsKey(id))
            {
                ConfigurationError error = configurationMessageDao.RemoveListener(id);
                if (error == ConfigurationError.ErrNoerror)
                {
                    ListenerAdapter l = _listeners[id];
                    _listeners.Remove(id);
                    // Listener must have been attached to an element. Remove also.
                    l.ParentElement.RemoveListener(id);
                    
                }
                else
                {
                    throw new InternalConstraintViolation("Server respondend with " + error);
                }
            }
            else
            {
                Log.Error("No listener for id " + id + " found.");        
            }
            
        }



        /// <summary>
        /// Removes the element with the given ID. Also removes all listeners attached to it. 
        /// <para>
        /// This represents a single call sent to the server.
        /// </para>
        /// </summary>
        /// <param name="id"></param>
        public void RemoveUiElement(Int32 id)
        {
            if (_elements.ContainsKey(id))
            {
                UiElementAdapter element = _elements[id];
                // Check if every listener attached to the element is represented in the
                // _listeners list.
                element.Listeners.ForEach(x =>
                {
                    if(!_listeners.ContainsKey(x.Id))
                        throw new InternalConstraintViolation("No listener for id " + id + " found. Was attached to element with id " + element.Id);
                });
                // Remove the element. Only progress if no error occured.
                ConfigurationError error = configurationMessageDao.RemoveUiElement(id);
                if (error == ConfigurationError.ErrNoerror)
                {
                    element.Listeners.ForEach(x => _listeners.Remove(x.Id));
                    _elements.Remove(id);
                }
                else
                {
                    throw new InternalConstraintViolation("Server respondend with " + error);
                }
            }
            else
            {
                throw new InternalConstraintViolation("No element for id " + id + " found.");
            }
        }




        /// <summary>
        /// Adds an element and forwards it to the fuics server. Backreferences from the eleements
        /// listeners back to the element do not have to be set. They will be overwritten
        /// in this method. 
        /// </summary>
        /// <param name="element"></param>
        public void AddUiElement(UiElementAdapter element)
        {
            if (_sessionOpen)
            {
                // No need to overwrite the backreferences here, as
                // the original elements will be discarded an replaced by
                // element sent from the server.
                _currentStatus.AddElement(element);
            }
            else
            {
                // For reasons of simplification, remove the listeners from the element before adding it. 
                // The listeners will be added one by one
                // TODO: might be an option to use replace status
                List<ListenerAdapter> listeners = element.Listeners;
                element.Listeners = new List<ListenerAdapter>();
                configurationMessageDao.AddUiElement(element);
                // Add the element into the dictionary. 
                _elements.Add(element.Id, element);
                // Adding the listeners later. This way, the info-strings can safely be ignored, and no
                // additional matching has to be performed, simplifying the whole process.
                listeners.ForEach(l =>
                {
                    l.ParentElement = element;
                    AddListener(element.Id, l);
                });
            }
        }

        /// <summary>
        /// Adds a listener.
        /// <para>
        /// The listeners <see cref="ListenerAdapter.ParentElement"/> doesn't have to be set, as
        /// the reference will be overwritten by this method. 
        /// </para>
        /// </summary>
        /// <param name="elementId"></param>
        /// <param name="listener"></param>
        /// <returns></returns>
        public Int32 AddListener(Int32 elementId, ListenerAdapter listener)
        {
            UiElementAdapter element;
            Int32 id = -1;
            if (_elements.TryGetValue(elementId, out element))
            {
                // Add it to the element (this will persist into the list)
                element.AddListener(listener);
                // This will write the ID into the listener
                configurationMessageDao.AddListener(listener, elementId);
                if (id == 0) throw new InternalConstraintViolation("Id may not be 0.");
                _listeners.Add(listener.Id, listener);
                id = listener.Id;
            }
            else
            {
                throw new InternalConstraintViolation("Element with the id " + elementId + " does not exist.");
            }
            return id;
        }



        /// <summary>
        /// Opens a replacing session. A replacing session will cache all <see cref="AddUiElement(UiElementAdapter)"/> operations
        /// until <see cref="FlushReplacingSession"/> or <see cref="CloseReplacingSession"/> is called. This way, a whole UI status can be
        /// progressibly generated. 
        /// </summary>
        /// <returns></returns>
        public bool OpenReplacingSession()
        {
            if (!_sessionOpen)
            {
                ClearReplacingSession();
                _sessionOpen = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Flushes the replacing session without discarding it's actions. The session remains open, the current status
        /// is transmitted to the server, but remains.
        /// </summary>
        public void FlushReplacingSession()
        {
            if (_sessionOpen)
            {
                ReplaceStatus();
            }
        }

        /// <summary>
        /// Closes the replacing sessions, sends the status to the server and discards the current status afterwards. 
        /// </summary>
        public void CloseReplacingSession()
        {
            if (_sessionOpen)
            {
                ReplaceStatus();
                ClearReplacingSession();
                _sessionOpen = false;
            }
        }

        /// <summary>
        /// Clears the whole internal status and all data structures associated with it.
        /// Does not clear <see cref="_currentStatus"/>
        /// </summary>
        private void ClearInternalStructures()
        {
            _elements.Clear();
            _listeners.Clear();
        }

        /// <summary>
        /// FIXME nt comment !IMPORTANT
        /// </summary>
        /// <param name="referenceListeners"></param>
        /// <param name="listener"></param>
        private void SetDelegatesOfListener(List<ListenerAdapter> referenceListeners, ListenerAdapter listener)
        {
            ListenerAdapter reference = referenceListeners.Find(x => x.Info == listener.Info);
            listener.OnEventTriggered = reference.OnEventTriggered;
            listener.OnTriggerInternal = reference.OnTriggerInternal;
        }

        private void ReplaceStatus()
        {
            ClearInternalStructures();
            List<ListenerAdapter> oldlisteners = _currentStatus.GetAllListeners();
            HashSet<String> infos = new HashSet<string>();
            // Validate that alle infos are unique. They are necessary to map the listeners
            // returned by the server back to their originals
            // for more info, refer to wiki. 
            oldlisteners.ForEach(l =>
            {
                if (infos.Contains(l.Info))
                    throw new InternalConstraintViolation(l.Info + " already existing in listeners" +
                                                          "given to replace status");
                infos.Add(l.Info);
            });
            _currentStatus = configurationMessageDao.ReplaceStatus(_currentStatus);
            foreach (var currentStatusElement in _currentStatus.Elements)
            {
                _elements.Add(currentStatusElement.Id, currentStatusElement);
                foreach (var listenerAdapter in currentStatusElement.Listeners)
                {
                    _listeners.Add(listenerAdapter.Id, listenerAdapter);
                    SetDelegatesOfListener(oldlisteners, listenerAdapter);
                }
            }
        }

        /// <summary>
        /// Resets the current replacing sessions. All cached operations are discarded.
        /// </summary>
        public void ClearReplacingSession()
        {
            _currentStatus = new UiStatusAdapter();
        }


        public void StartListening()
        {
            eventMessageDao.Open();
        }

        public void EndListening()
        {
            eventMessageDao.Close();
        }

        public void StartCalibration(DimensionsAdapter dimensions)
        {
            configurationMessageDao.StartAssist(dimensions);
        }

        public void EndCalibration()
        {
            configurationMessageDao.FinalizeAssist();
        }


        /// <summary>
        /// <list type="number">
        ///     <item>
        ///          <description>Invokes reading of the event channel. Reads all bytes from the socket, interpretes them as EventMessage</description>
        ///     </item>
        ///     <item>
        ///          <description>For each event, determines the listener that caused the event to fire. </description>   
        ///     </item>
        ///     <item>
        ///          <description>On each listener, <see cref="ListenerAdapter.Invoke()"/>  is called. </description>
        ///     </item>
        /// </list>
        /// </summary>
        public void InvokeEventProcessing()
        {
            List<EventMessageAdapter> messages = eventMessageDao.ReadEvents();
            foreach (EventMessageAdapter msg in messages)
            {
                Log.Debug("Event of type " + msg.EventType + " received.");
                ListenerAdapter listener;
                _listeners.TryGetValue(msg.ListenerId, out listener);
                if (listener != null)
                {

                    listener.Invoke();
                }
                else
                {
                    // This core did not register the listener. Simply ignore it. 
                }
            }
        }

        /// <summary>
        ///  Configure logging to write to Logs\EventLog.txt and the Unity console output.
        /// Used instead of an xml file to simplify dependencies. 
        /// </summary>
        public static void ConfigureAllLogging(String filename)
        {
            
            var patternLayout = new PatternLayout
            {
                ConversionPattern = "%date %-5level %logger - %message%newline"
            };
            patternLayout.ActivateOptions();

            // setup the appender that writes to Log\EventLog.txt
            var fileAppender = new RollingFileAppender
            {
                AppendToFile = false,
                File = @"Logs\" + filename,
                Layout = patternLayout,
                MaxSizeRollBackups = 5,
                MaximumFileSize = "5mb",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            fileAppender.ActivateOptions();

            var patternLayoutUnity = new PatternLayout
            {
                ConversionPattern = "%date %-5level %logger - %message%newline"
            };
           

            var unityLogger = new UnityAppender
            {
                Layout = patternLayoutUnity
            };
            patternLayoutUnity.ActivateOptions();
            unityLogger.ActivateOptions();

            BasicConfigurator.Configure(unityLogger, fileAppender);
        }

        public static void SetLogLEvel(Level level)
        {
            ((log4net.Repository.Hierarchy.Hierarchy) LogManager.GetRepository()).Root.Level = level;
        }


    }
}


