﻿
using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Fhw.Fuics;
using Fhw.Fuics.Exception;
using Fhw.Fuics.Network;
using Fhw.Fuics.Wrapper;
using NSubstitute;
using UnityEngine;
using Assert = UnityEngine.Assertions.Assert;
using Debug = System.Diagnostics.Debug;
// ReSharper disable InconsistentNaming

namespace Assets.Tests.Editor
{
    // TODO: Inner constraints 
    // -> Info strings
    // -> No negative ids received
    public class FuicsCoreTest
    {


       

        [SetUp]
        public void Init()
        {
            FuicsCore.ClearCores();
        }

        private EventMessageDAO MockEventMessageDao(List<EventMessageAdapter> messagesToBeRead)
        {
            var result = NSubstitute.Substitute.For<EventMessageDAO>();
            result.When(x => x.ReadEvents()).DoNotCallBase();
            result.ReadEvents().Returns(messagesToBeRead);
            return result;
        }

        private ConfigurationMessageDAO MockConfigReplaceStatus(UiStatusAdapter statusToReturn)
        {
            ConfigurationMessageDAO result = Substitute.For<ConfigurationMessageDAO>();
            result.WhenForAnyArgs(x => x.ReplaceStatus(Arg.Any<UiStatusAdapter>())).DoNotCallBase();
            result.ReplaceStatus(Arg.Any<UiStatusAdapter>()).ReturnsForAnyArgs(statusToReturn);
            return result;
        }

        /// <summary>
        /// Mocks a <see cref="ConfigurationMessageDAO"/> that does nothing but return "OK"
        /// </summary>
        /// <returns></returns>
        private ConfigurationMessageDAO MockConfigurationMessageDao()
        {
            ConfigurationMessageDAO result = NSubstitute.Substitute.For<ConfigurationMessageDAO>();
            result.When(x => x.AddListener(Arg.Any<ListenerAdapter>(), Arg.Any<int>())).DoNotCallBase();
            result.When(x => x.AddUiElement(Arg.Any<UiElementAdapter>())).DoNotCallBase();
            result.When(x => x.RemoveListener(Arg.Any<int>())).DoNotCallBase();
            result.When(x => x.RemoveUiElement(Arg.Any<int>())).DoNotCallBase();

            result.AddListener(Arg.Any<ListenerAdapter>(), Arg.Any<int>()).Returns(ConfigurationError.ErrNoerror);
            result.AddUiElement(Arg.Any<UiElementAdapter>()).Returns(ConfigurationError.ErrNoerror);
            result.RemoveListener(Arg.Any<int>()).Returns(ConfigurationError.ErrNoerror);
            result.RemoveUiElement(Arg.Any<int>()).Returns(ConfigurationError.ErrNoerror);
            return result;
        }

        /// <summary>
        /// Validates and asserts that for each listener present in the status, the backreference
        /// to their respective elements are set correctly. 
        /// </summary>
        /// <param name="staus"></param>
        private void AssertStatusConstraints(UiStatusAdapter status)
        {
            List<ListenerAdapter> listeners = status.GetAllListeners();
            listeners.ForEach(x =>
            {
                Assert.IsNotNull(x.ParentElement);
            });
        }

        [Test]
        public void NormalizationTest()
        {
            FuicsCore core = FuicsCore.CreateCore("localhost");
            // By default 0..1 is set
            Assert.AreApproximatelyEqual(0.0f, core.NormalizeDimX(0, 0.0f, 800.0f));
            Assert.AreApproximatelyEqual(0.1f, core.NormalizeDimX(80.0f, 0.0f, 800.0f));
            Assert.AreApproximatelyEqual(0.5f, core.NormalizeDimX(400.0f, 0.0f, 800.0f));
            Assert.AreApproximatelyEqual(1.0f, core.NormalizeDimX(800.0f, 0.0f, 800.0f));

            Assert.AreApproximatelyEqual(0.0f, core.NormalizeDimY(0, 0.0f, 800.0f));
            Assert.AreApproximatelyEqual(0.1f, core.NormalizeDimY(80.0f, 0.0f, 800.0f));
            Assert.AreApproximatelyEqual(0.5f, core.NormalizeDimY(400.0f, 0.0f, 800.0f));
            Assert.AreApproximatelyEqual(1.0f, core.NormalizeDimY(800.0f, 0.0f, 800.0f));

            Assert.AreApproximatelyEqual(0.0f, core.NormalizeDimX(-400.0f, -400.0f, 400.0f));
            Assert.AreApproximatelyEqual(0.5f, core.NormalizeDimX(0.0f, -400.0f, 400.0f));
            Assert.AreApproximatelyEqual(1.0f, core.NormalizeDimX(400.0f, -400.0f, 400.0f));

            Assert.AreApproximatelyEqual(1.0f, core.NormalizeDimX(-400.0f, 400.0f, -400.0f));
            Assert.AreApproximatelyEqual(0.5f, core.NormalizeDimX(0.0f, 400.0f, -400.0f));
            Assert.AreApproximatelyEqual(0.0f, core.NormalizeDimX(400.0f, 400.0f, -400.0f));

        }

        [Test]
        public void TestSimpleSessioning()
        {
            FuicsCore core = FuicsCore.CreateCore("localhost");
            core.OpenReplacingSession();

            // Add a few elements
            UiElementAdapter element = new UiElementAdapter();
            element.Id = 22;
            UiElementAdapter element1 = new UiElementAdapter();
            element1.Id = 23;
            UiElementAdapter element2 = new UiElementAdapter();
            element2.Id = 24;
            core.AddUiElement(element);
            core.AddUiElement(element1);
            core.AddUiElement(element2);

            UiStatusAdapter readStatus = (UiStatusAdapter) TestUtil.Read(core, "_currentStatus", typeof(FuicsCore));

            Assert.AreEqual(3, readStatus.Elements.Count);
        }

        /// <summary>
        /// Tests if the event processing works in combination with adding a listener
        /// to an already added UI Element.
        /// <para>
        /// This test validates calling of <see cref="ListenerAdapter.Invoke"/> when using the
        /// <see cref="FuicsCore.InvokeEventProcessing"/> method in combination with a mocked network layer.
        /// </para>
        /// </summary>
        [Test]
        public void TestEventProcessing()
        {
            bool eventRaised = false;
            FuicsCore core = FuicsCore.CreateCore("localhost");
            ConfigurationMessageDAO mockedDao = MockConfigurationMessageDao();
            List<EventMessageAdapter> events = new List<EventMessageAdapter>();
            events.Add(new EventMessageAdapter
            {
                ElementId = 1,
                EventType = EventType.Press,
                Info = "HalloWelt",
                ListenerId = 11
            });
            EventMessageDAO eventMessageDao = MockEventMessageDao(events);
            TestUtil.Inject(core, "configurationMessageDao", mockedDao);
            TestUtil.Inject(core, "eventMessageDao", eventMessageDao);
            // Add two elements without listeners.
            UiElementAdapter element1 = new UiElementAdapter
            {
                Id = 1,
                Type = ElementTypeAdapter.Rectangle
            };
            core.AddUiElement(element1);

            ListenerAdapter listener1 = new ListenerAdapter
            {
                Id = 11,
                OnEventTriggered = delegate { eventRaised = true; },
                Info = "HalloWelt",
                ListensTo = EventTypeAdapter.Press
            };
            core.AddListener(1, listener1);
            core.InvokeEventProcessing();
            Assert.IsTrue(eventRaised);
        }

        public UiElementAdapter GetElementWithListenerCount(List<UiElementAdapter> e, Int32 listenerCount)
        {
            return e.Find(x => x.Listeners.Count == listenerCount);
        }


        /// <summary>
        /// Validates a form of "sessioning" that relys upon usage of replace status.
        /// This method validates that the element - listener associations are not lost, and that 
        /// listeners remain functional after calling of replace status. 
        /// </summary>
        [Test]
        public void TestComplexSessioningWithCorrectListenerMapping()
        {
            FuicsCore core = FuicsCore.CreateCore("localhost");
            // The status that is transmitted to the server.
            UiStatusAdapter statusTransmitted = new UiStatusAdapter();
            UiStatusAdapter stautsReceived = new UiStatusAdapter();
            ConfigurationMessageDAO configurationMessageDao = MockConfigReplaceStatus(stautsReceived);
            TestUtil.Inject(core, "configurationMessageDao", configurationMessageDao);
            // Create element for transmitted status
            UiElementAdapter elementAdapter1 = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 1, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            UiElementAdapter elementAdapter2 = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 2, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            UiElementAdapter elementAdapter3 = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 3, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            UiElementAdapter elementAdapter4 = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 4, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            // Element1 has no listeners
            // Element2 has two listeners
            // Element 3 has one
            // Element 4 has 3 listeners
            elementAdapter2.AddListener(new ListenerAdapter(20, "10", EventTypeAdapter.Hold));
            elementAdapter2.AddListener(new ListenerAdapter(20, "11", EventTypeAdapter.Hold));
            ListenerAdapter element3Listener = new ListenerAdapter(20, "12", EventTypeAdapter.Hold);
            bool eventCalled = false;
            element3Listener.OnEventTriggered = delegate
            {
                eventCalled = true;
            };
            elementAdapter3.AddListener(element3Listener);
            elementAdapter4.AddListener(new ListenerAdapter(20, "13", EventTypeAdapter.Hold));
            elementAdapter4.AddListener(new ListenerAdapter(20, "14", EventTypeAdapter.Hold));
            elementAdapter4.AddListener(new ListenerAdapter(20, "15", EventTypeAdapter.Hold));
            statusTransmitted.AddElement(elementAdapter1);
            statusTransmitted.AddElement(elementAdapter2);
            statusTransmitted.AddElement(elementAdapter3);
            statusTransmitted.AddElement(elementAdapter4);

            // Create the same elements again. Use correct ids. 
            // This mimics the servers behaviour. 
            UiElementAdapter elementAdapter1c = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 1, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            UiElementAdapter elementAdapter2c = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 2, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            UiElementAdapter elementAdapter3c = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 3, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            UiElementAdapter elementAdapter4c = new UiElementAdapter(new DimensionsAdapter(Vector3.down, 1, 1), 4, ElementTypeAdapter.Rectangle, new List<ListenerAdapter>());
            // Element1 has no listeners
            // Element2 has two listeners
            // Element 3 has one
            // Element 4 has 3 listeners
            elementAdapter2c.AddListener(new ListenerAdapter(20, "10", EventTypeAdapter.Hold));
            elementAdapter2c.AddListener(new ListenerAdapter(21, "11", EventTypeAdapter.Hold));
            elementAdapter3c.AddListener(new ListenerAdapter(22, "12", EventTypeAdapter.Hold));
            elementAdapter4c.AddListener(new ListenerAdapter(23, "13", EventTypeAdapter.Hold));
            elementAdapter4c.AddListener(new ListenerAdapter(24, "14", EventTypeAdapter.Hold));
            elementAdapter4c.AddListener(new ListenerAdapter(25, "15", EventTypeAdapter.Hold));
            stautsReceived.AddElement(elementAdapter1c);
            stautsReceived.AddElement(elementAdapter2c);
            stautsReceived.AddElement(elementAdapter3c);
            stautsReceived.AddElement(elementAdapter4c);


            // Set the session to open to allow a flush
            TestUtil.Inject(core, "_sessionOpen", true);
            TestUtil.Inject(core, "_currentStatus", statusTransmitted);

            core.FlushReplacingSession();

            UiStatusAdapter status = (UiStatusAdapter) TestUtil.Read(core, "_currentStatus", typeof(FuicsCore));
            AssertStatusConstraints(status);
            Assert.IsNotNull(status);
            Assert.AreEqual(4, status.Elements.Count);
            List<UiElementAdapter> elementAdapters = status.Elements;
            UiElementAdapter zeroListenUiElementAdapter = GetElementWithListenerCount(elementAdapters, 0);
            UiElementAdapter oneListenUiElementAdapter = GetElementWithListenerCount(elementAdapters, 1);
            UiElementAdapter twoListenUiElementAdapter = GetElementWithListenerCount(elementAdapters, 2);
            UiElementAdapter threeListenUiElementAdapter = GetElementWithListenerCount(elementAdapters, 3);
            Assert.IsNotNull(zeroListenUiElementAdapter);
            Assert.IsNotNull(oneListenUiElementAdapter);
            Assert.IsNotNull(twoListenUiElementAdapter);
            Assert.IsNotNull(threeListenUiElementAdapter);

            Assert.AreEqual(0, zeroListenUiElementAdapter.Listeners.Count);
            Assert.AreEqual(1, oneListenUiElementAdapter.Listeners.Count);
            Assert.AreEqual(2, twoListenUiElementAdapter.Listeners.Count);
            Assert.AreEqual(3, threeListenUiElementAdapter.Listeners.Count);

            oneListenUiElementAdapter.Listeners[0].Invoke();
            Assert.IsTrue(eventCalled);
        }

        /// <summary>
        /// Validates that adding an ui element and listener seperatly also adds
        /// it into the necessary maps and into the UI Status.
        /// Mocks the network layer to return the correct id
        /// </summary>
        [Test]
        public void TestAddElementAndListener()
        {
            FuicsCore core = FuicsCore.CreateCore("localhost");
            ConfigurationMessageDAO configurationMessageDao =
                NSubstitute.Substitute.For<ConfigurationMessageDAO>();
            // Mock DAO to set the element id to 50
            configurationMessageDao.When(x => x.AddUiElement(Arg.Any<UiElementAdapter>())).Do(
                x => x.Arg<UiElementAdapter>().Id = 50
                );

            configurationMessageDao.When(x => x.AddListener(Arg.Any<ListenerAdapter>(), Arg.Any<int>())).Do(
                x => x.Arg<ListenerAdapter>().Id = 51
                );
            TestUtil.Inject(core, "configurationMessageDao", configurationMessageDao);
            UiElementAdapter elementAdapter = new UiElementAdapter
            {
                Id = -1,
                Type = ElementTypeAdapter.Rectangle
            };
            ListenerAdapter listenerAdapter = new ListenerAdapter();
            listenerAdapter.Id = -1;
            listenerAdapter.Info = "";
            listenerAdapter.ListensTo = EventTypeAdapter.Press;
            core.AddUiElement(elementAdapter);
            core.AddListener(50, listenerAdapter);

            Dictionary<int, ListenerAdapter> listenersFromCore =
                TestUtil.Read(core, "_listeners", typeof(FuicsCore)) as Dictionary<int, ListenerAdapter>;
            Dictionary<int, UiElementAdapter> elementsFromCore =
                TestUtil.Read(core, "_elements", typeof(FuicsCore)) as Dictionary<int, UiElementAdapter>;

            Assert.IsNotNull(listenersFromCore);
            Assert.IsNotNull(elementsFromCore);
            Assert.AreEqual(1, elementsFromCore.Count);
            Assert.AreEqual(1, listenersFromCore.Count);
            Assert.IsNotNull(elementsFromCore[50]);
            Assert.IsNotNull(listenersFromCore[51]);
        }

        /// <summary>
        /// Tests if an Element with listeners can be added.
        /// <para>
        /// During this request, ids should be added to the listeners, but there is no direct
        /// definition which listener receives which id. This is solved by usage of the
        /// info field. This test validates that this info-field usage
        /// works with <see cref="FuicsCore.AddUiElement"/>
        /// </para>
        /// </summary>
        [Test]
        public void TestAddElementWithListeners()
        {
            // The element that will be responded
            UiElementAdapter responseElement = new UiElementAdapter();
            responseElement.Id = 1;
            responseElement.AddListener(new ListenerAdapter(2, "2", EventTypeAdapter.Hold));
            responseElement.AddListener(new ListenerAdapter(3, "3", EventTypeAdapter.Hold));
            responseElement.AddListener(new ListenerAdapter(4, "4", EventTypeAdapter.Hold));
            ConfigurationMessageDAO configurationMessageDao =
                NSubstitute.Substitute.For<ConfigurationMessageDAO>();
            configurationMessageDao.When(x => x.AddUiElement(Arg.Any<UiElementAdapter>())).Do(
                x => {
                        x.Arg<UiElementAdapter>().Id = 1;
                        x.Arg<UiElementAdapter>().Listeners = responseElement.Listeners;
                    });
           
            // The element to be added.
            UiElementAdapter addedElement = new UiElementAdapter();
            addedElement.Id = -1;
            ListenerAdapter l1 = new ListenerAdapter(-1, "2", EventTypeAdapter.Hold);
            ListenerAdapter l2 = new ListenerAdapter(-1, "2", EventTypeAdapter.Hold);
            ListenerAdapter l3 = new ListenerAdapter(-1, "2", EventTypeAdapter.Hold);
            addedElement.AddListener(l1);
            addedElement.AddListener(l2);
            addedElement.AddListener(l3);

            configurationMessageDao.When(x => x.AddListener(l1, 1)).Do(x => x.Arg<ListenerAdapter>().Id = 2);
            configurationMessageDao.When(x => x.AddListener(l2, 1)).Do(x => x.Arg<ListenerAdapter>().Id = 3);
            configurationMessageDao.When(x => x.AddListener(l3, 1)).Do(x => x.Arg<ListenerAdapter>().Id = 4);

            FuicsCore core = FuicsCore.CreateCore("localhost");
            TestUtil.Inject(core, "configurationMessageDao", configurationMessageDao);

            core.AddUiElement(addedElement);
            Dictionary<int, ListenerAdapter> listenersFromCore =
                TestUtil.Read(core, "_listeners", typeof(FuicsCore)) as Dictionary<int, ListenerAdapter>;
            Dictionary<int, UiElementAdapter> elementsFromCore =
                TestUtil.Read(core, "_elements", typeof(FuicsCore)) as Dictionary<int, UiElementAdapter>;
            Assert.IsNotNull(listenersFromCore);
            Assert.IsNotNull(elementsFromCore);
            Assert.AreEqual(1, elementsFromCore.Count);
            Assert.AreEqual(3, listenersFromCore.Count);

            Assert.IsNotNull(listenersFromCore[2]);
            Assert.IsNotNull(listenersFromCore[3]);
            Assert.IsNotNull(listenersFromCore[4]);
            for (int i = 2; i <= 4; i++)
            {
                Assert.AreEqual(listenersFromCore[2].Id, Convert.ToInt32(listenersFromCore[2].Info));
            }
        }

        /// <summary>
        /// Validates the inner constraints of this package: Info strings have to be unique. 
        /// <para>This constraint is necessary to re-map listeners received from the server back to the
        /// original listeners that were attached to the element. 
        /// </para>
        /// </summary>
        [Test]
        public void TestAddElementWithInvalidInfoStrings()
        {
            UiElementAdapter elementAdapter = new UiElementAdapter();
            // Not good: Same info strings
            elementAdapter.AddListener(new ListenerAdapter(-1, "Hallo", EventTypeAdapter.Hold));
            elementAdapter.AddListener(new ListenerAdapter(-1, "Hallo", EventTypeAdapter.Hold));
            
            UiStatusAdapter status = new UiStatusAdapter();
            status.AddElement(elementAdapter);

            FuicsCore core = FuicsCore.CreateCore("localhost");

            // Fake a session that can be flushed. Flushing Invokes the validation that should
            // throw an exception
            TestUtil.Inject(core, "_sessionOpen", true);
            TestUtil.Inject(core, "_currentStatus", status);
          
            NUnit.Framework.Assert.Throws<InternalConstraintViolation>(delegate
            {
                core.FlushReplacingSession();
            });
        }

        /// <summary>
        /// Validates that simple listeners may be removed.
        /// </summary>
        [Test]
        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        public void TestRemoveListener()
        {

            int eId = 10;
            int lId = 11;
            bool elementRemoveCalled = false;
            bool listenerRemoveCalled = false;
            ConfigurationMessageDAO configurationMessageDao = MockConfigurationMessageDao();
            configurationMessageDao.When(x => x.AddUiElement(Arg.Any<UiElementAdapter>())).Do(x => x.Arg<UiElementAdapter>().Id = eId);
            configurationMessageDao.When(x => x.AddListener(Arg.Any<ListenerAdapter>(), 10)).Do(x => x.Arg<ListenerAdapter>().Id = lId);
            configurationMessageDao.When(x => x.RemoveListener(Arg.Any<int>())).Do(x => listenerRemoveCalled = true);
            configurationMessageDao.When(x => x.RemoveUiElement(Arg.Any<int>())).Do(x => elementRemoveCalled = true);



            FuicsCore core = FuicsCore.CreateCore("localhost");
            TestUtil.Inject(core, "configurationMessageDao", configurationMessageDao);

            UiElementAdapter element = new UiElementAdapter();

            core.AddUiElement(element);
            core.AddListener(eId, new ListenerAdapter());

            Dictionary<int, ListenerAdapter> listeners = TestUtil.Read(core, "_listeners", typeof(FuicsCore)) as Dictionary<int, ListenerAdapter>;
                
            core.RemoveListener(lId);

            Assert.IsFalse(listeners.ContainsKey(lId));
            // Get the only element
            Dictionary<int, UiElementAdapter> elements = TestUtil.Read(core, "_elements", typeof(FuicsCore)) as Dictionary<int, UiElementAdapter>;

            // ReSharper disable once PossibleNullReferenceException
            UiElementAdapter e = elements[eId];
            Assert.AreEqual(0, e.Listeners.Count);

            Assert.IsTrue(listenerRemoveCalled);
            Assert.IsFalse(elementRemoveCalled);

        }

        /// <summary>
        /// Validates that an element with listenerrs attached can be removed. 
        /// That the element is actually added is validated in <see cref="TestAddElementAndListener"/>
        /// </summary>
        [Test]
        public void TestRemoveElementWithListeners()
        {
            int eId = 10;
            int lId = 11;
            bool elementRemoveCalled = false;
            bool listenerRemoveCalled = false;
            ConfigurationMessageDAO configurationMessageDao = MockConfigurationMessageDao();
            configurationMessageDao.When(x => x.AddUiElement(Arg.Any<UiElementAdapter>())).Do(x => x.Arg<UiElementAdapter>().Id = eId);
            configurationMessageDao.When(x => x.AddListener(Arg.Any<ListenerAdapter>(), 10)).Do(x => x.Arg<ListenerAdapter>().Id = lId);
            configurationMessageDao.When(x => x.RemoveListener(Arg.Any<int>())).Do(x => listenerRemoveCalled = true);
            configurationMessageDao.When(x => x.RemoveUiElement(Arg.Any<int>())).Do(x => elementRemoveCalled = true);

            FuicsCore core = FuicsCore.CreateCore("localhost");
            TestUtil.Inject(core, "configurationMessageDao", configurationMessageDao);
            UiElementAdapter elementAdapter = new UiElementAdapter();
            elementAdapter.AddListener(new ListenerAdapter());
    
            core.AddUiElement(elementAdapter);
            core.RemoveUiElement(eId);
            Dictionary<int, ListenerAdapter> listeners = TestUtil.Read(core, "_listeners", typeof(FuicsCore)) as Dictionary<int, ListenerAdapter>;
            Dictionary<int, UiElementAdapter> elements = TestUtil.Read(core, "_elements", typeof(FuicsCore)) as Dictionary<int, UiElementAdapter>;

            Assert.AreEqual(0, elements.Count);
            Assert.AreEqual(0, listeners.Count);
            Assert.IsTrue(elementRemoveCalled);
            Assert.IsFalse(listenerRemoveCalled);



        }
    }

   
}
