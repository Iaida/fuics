var _event_type_8cs =
[
    [ "pb", "_event_type_8cs.html#a3a8b1403f9034aa88aad1ce2c733db60", null ],
    [ "pbc", "_event_type_8cs.html#acf353ca81053b715ef0a189df5a75499", null ],
    [ "pbr", "_event_type_8cs.html#a4c9bca18934bf8b310b9d65812404c69", null ],
    [ "scg", "_event_type_8cs.html#a6de48d7be2d1c733071a3f897657ee05", null ],
    [ "EventType", "_event_type_8cs.html#a2628ea8d12e8b2563c32f05dc7fff6fa", [
      [ "Press", "_event_type_8cs.html#a2628ea8d12e8b2563c32f05dc7fff6faa0610123bdd4ffc191a3ea05a847e1307", null ],
      [ "Hold", "_event_type_8cs.html#a2628ea8d12e8b2563c32f05dc7fff6faabcd8db575b47c838e5d551e3973db4ac", null ],
      [ "Release", "_event_type_8cs.html#a2628ea8d12e8b2563c32f05dc7fff6faab8e7b465df7c5979dc731d06e84ce2cf", null ]
    ] ]
];