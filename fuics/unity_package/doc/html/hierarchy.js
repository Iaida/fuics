var hierarchy =
[
    [ "AppenderSkeleton", null, [
      [ "Fhw.Fuics.logging.UnityAppender", "class_fhw_1_1_fuics_1_1logging_1_1_unity_appender.html", null ]
    ] ],
    [ "Fhw.Fuics.ButtonCapsule", "class_fhw_1_1_fuics_1_1_button_capsule.html", null ],
    [ "Fhw.Fuics.Wrapper.DimensionsAdapter", "class_fhw_1_1_fuics_1_1_wrapper_1_1_dimensions_adapter.html", null ],
    [ "Exception", null, [
      [ "Fhw.Fuics.Exception.InternalConstraintViolation", "class_fhw_1_1_fuics_1_1_exception_1_1_internal_constraint_violation.html", null ]
    ] ],
    [ "Fhw.Fuics.FuicsCore", "class_fhw_1_1_fuics_1_1_fuics_core.html", null ],
    [ "IMessage", null, [
      [ "ConfigurationMessage", "class_configuration_message.html", null ],
      [ "Dimensions", "class_dimensions.html", null ],
      [ "EventMessage", "class_event_message.html", null ],
      [ "Listener", "class_listener.html", null ],
      [ "UIElement", "class_u_i_element.html", null ],
      [ "UIStatus", "class_u_i_status.html", null ]
    ] ],
    [ "Fhw.Fuics.Wrapper.ListenerAdapter", "class_fhw_1_1_fuics_1_1_wrapper_1_1_listener_adapter.html", null ],
    [ "MonoBehaviour", null, [
      [ "Fhw.Fuics.CalibrationComponent", "class_fhw_1_1_fuics_1_1_calibration_component.html", null ],
      [ "Fhw.Fuics.FuicsHighLevelApi", "class_fhw_1_1_fuics_1_1_fuics_high_level_api.html", null ]
    ] ],
    [ "Fhw.Fuics.Wrapper.UiElementAdapter", "class_fhw_1_1_fuics_1_1_wrapper_1_1_ui_element_adapter.html", null ],
    [ "Fhw.Fuics.Wrapper.UiStatusAdapter", "class_fhw_1_1_fuics_1_1_wrapper_1_1_ui_status_adapter.html", null ]
];