var _configuration_message_8cs =
[
    [ "Dimensions", "class_dimensions.html", "class_dimensions" ],
    [ "Listener", "class_listener.html", "class_listener" ],
    [ "UIElement", "class_u_i_element.html", "class_u_i_element" ],
    [ "UIStatus", "class_u_i_status.html", "class_u_i_status" ],
    [ "ConfigurationMessage", "class_configuration_message.html", "class_configuration_message" ],
    [ "pb", "_configuration_message_8cs.html#a3a8b1403f9034aa88aad1ce2c733db60", null ],
    [ "pbc", "_configuration_message_8cs.html#acf353ca81053b715ef0a189df5a75499", null ],
    [ "pbr", "_configuration_message_8cs.html#a4c9bca18934bf8b310b9d65812404c69", null ],
    [ "scg", "_configuration_message_8cs.html#a6de48d7be2d1c733071a3f897657ee05", null ],
    [ "ConfigurationError", "_configuration_message_8cs.html#a8a63bcc069be3663e42edb6610d27150", [
      [ "ErrNoerror", "_configuration_message_8cs.html#a8a63bcc069be3663e42edb6610d27150a8e869ea5ca439731a6629a348254c28c", null ],
      [ "ErrUnknown", "_configuration_message_8cs.html#a8a63bcc069be3663e42edb6610d27150a275a7136d263fc89a7b8d6af9b5cb7c6", null ],
      [ "ErrAssistActive", "_configuration_message_8cs.html#a8a63bcc069be3663e42edb6610d27150a35ce582c02d21d9cfecb234ae511606b", null ],
      [ "ErrAssistInactive", "_configuration_message_8cs.html#a8a63bcc069be3663e42edb6610d27150a90db7164777ab4b85a981cd76067554f", null ],
      [ "ErrUnknownId", "_configuration_message_8cs.html#a8a63bcc069be3663e42edb6610d27150a17182f628af86784deba699dde1f5f07", null ]
    ] ],
    [ "ConfigurationRequestType", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89", [
      [ "Assist", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89a937d383940beb94573cc1e2d862ec75a", null ],
      [ "Finalize", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89afd565de81da1c94807c0b80840ba18b0", null ],
      [ "Debug", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89aa603905470e2a5b8c13e96b579ef0dba", null ],
      [ "UpdateDimensions", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89a75310a678f4c8a73315de679f36dfd1d", null ],
      [ "AddElement", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89ac4cdf151d451cf1b8ad1cc2374226720", null ],
      [ "RemoveElement", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89a88bc403c4844cd9977762fcbb42fbd81", null ],
      [ "ReplaceStatus", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89a101a0c7594810500bec44bb0074d7437", null ],
      [ "AddListener", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89a9a40fe2d30a5ee17181153d93070c9b5", null ],
      [ "RemoveListener", "_configuration_message_8cs.html#ac204cdd537fd98b98ad544462900ac89a443e3ecff5a4946a31d960d54f17dae1", null ]
    ] ],
    [ "ElementType", "_configuration_message_8cs.html#a16b11be27a8e9362dd122c4d879e01ae", [
      [ "Rectangle", "_configuration_message_8cs.html#a16b11be27a8e9362dd122c4d879e01aeace9291906a4c3b042650b70d7f3b152e", null ]
    ] ]
];