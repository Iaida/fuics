﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class EndScreen : MonoBehaviour
    {
        public Text ScoreText;

        public void Show(TimeSpan completionTime)
        {
            this.gameObject.SetActive(true);
            ScoreText.text = "-- " + completionTime.Seconds + ":" + completionTime.Milliseconds + " --";
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        // Use this for initialization
        void Start () {
		
        }
	
        // Update is called once per frame
        void Update () {
		
        }
    }
}
