﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Fhw.Fuics.Wrapper
{
    public enum EventTypeAdapter
    {
        None = 0,
        Press = 1,
        Release = 2,
        Hold = 3,
    }

    public static class EventTypeAdapterMethods
    {
        public static EventType ToProtobuf(this EventTypeAdapter eventTypeAdapter)
        {
            switch (eventTypeAdapter)
            {
                case EventTypeAdapter.Press:
                    return EventType.Press;
                case EventTypeAdapter.Release:
                    return EventType.Release;
                case EventTypeAdapter.Hold:
                    return EventType.Hold;
                default:
                    throw new ArgumentOutOfRangeException("eventTypeAdapter", eventTypeAdapter, null);
            }
        }

        public static string DisplayString(this EventTypeAdapter eventTypeAdapter)
        {
            switch (eventTypeAdapter)
            {
                case EventTypeAdapter.None:
                    return "None";
                case EventTypeAdapter.Press:
                    return "Press";
                case EventTypeAdapter.Release:
                    return "Release";
                case EventTypeAdapter.Hold:
                    return "Hold";
                default:
                    throw new ArgumentOutOfRangeException("eventTypeAdapter", eventTypeAdapter, null);
            }
        }

        public static EventTypeAdapter ToAdapter(EventType protobufType)
        {
            switch (protobufType)
            {
                case EventType.Press:
                    return EventTypeAdapter.Press;
                case EventType.Release:
                    return EventTypeAdapter.Release;
                case EventType.Hold:
                    return EventTypeAdapter.Hold;
                default:
                    throw new ArgumentOutOfRangeException("protobufType", protobufType, null);
            }
        }
    }
}
