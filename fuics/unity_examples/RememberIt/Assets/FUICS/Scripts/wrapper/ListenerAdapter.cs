﻿
namespace Fhw.Fuics.Wrapper
{
    public delegate void OnFuicsEventCallback(string info); 

    public class ListenerAdapter
    {
        public int Id { get; set; }

        public string Info { get; set; }

        public EventTypeAdapter ListensTo { get; set; }

        /// <summary>
        /// Reference to the <see cref="UiElementAdapter"/>
        /// to which this listener is attached. Needs to be maintained by this
        /// library, as it is not maintained by the server.
        /// </summary>
        public UiElementAdapter ParentElement { get; set; }

        /// <summary>
        /// Delegate called when this listener is invoked. Used for library internal processing.
        /// </summary>
        internal InternalInvokeButtonClick onTriggerInternal;

        public OnFuicsEventCallback onEventTriggered;



        public ListenerAdapter()
        {
            
        }

        public ListenerAdapter(int id, string info, EventTypeAdapter listensTo)
        {
            Id = id;
            Info = info;
            this.ListensTo = listensTo;
        }

        public ListenerAdapter(Listener listener)
        {
            Id = listener.Id;
            Info = listener.Info;
            ListensTo = EventTypeAdapterMethods.ToAdapter(listener.ListensTo);
        }

        public Listener ToProtobuf()
        {
            Listener  result = new Listener()
            {
                Id = Id,
                Info = Info,
                ListensTo = ListensTo.ToProtobuf()
            };

            return result;
        }

        /// <summary>
        /// Invokes the listeners that are attached to this 
        /// </summary>
        public void Invoke()
        {
            if (onTriggerInternal != null)
            {
                onTriggerInternal();
            }

            if (onEventTriggered != null)
            {
                onEventTriggered(Info);
            }
           
        }
    }
}
