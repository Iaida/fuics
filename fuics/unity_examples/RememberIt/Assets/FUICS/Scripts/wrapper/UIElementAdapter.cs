﻿using System.Collections;
using System.Collections.Generic;
using Fhw.Fuics.Exception;
using JetBrains.Annotations;

namespace Fhw.Fuics.Wrapper
{
    /// <summary>
    /// A fuics UI element that represents a logical 
    /// </summary>
    public class UiElementAdapter
    {
        public DimensionsAdapter DimensionsAdapter { get; set; }
        public int Id { get; set; }
        public ElementTypeAdapter Type { get; set; }

        [NotNull]
        public List<ListenerAdapter> Listeners = new List<ListenerAdapter>();

        public UiElementAdapter()
        {
            
        }

        public UiElementAdapter(DimensionsAdapter dimensionsAdapter, int id, ElementTypeAdapter type, List<ListenerAdapter> listeners)
        {
            DimensionsAdapter = dimensionsAdapter;
            Id = id;
            Type = type;
            Listeners = listeners;
        }

        public UiElementAdapter(UIElement element)
        {
            DimensionsAdapter = new DimensionsAdapter(element.Dimensions);
            Id = element.Id;
            Type = ElementTypeAdapterMethods.FromProtobuf(element.Type);
            Listeners = new List<ListenerAdapter>();
            foreach (Listener listener in element.Listeners)
            {
                Listeners.Add(new ListenerAdapter(listener));
            }
        }

        /// <summary>
        /// Adds a listener to this ui Element. This operation does not invoke any network operation, adding a listener to this adapter
        /// will not invoke any ui server changes unless explicitly committed to the server. 
        /// <para>
        /// This will also overwirte the <see cref="ListenerAdapter.ParentElement"/> field.
        /// </para>
        /// </summary>
        /// <param name="listener"></param>
        public void AddListener(ListenerAdapter listener)
        {
            listener.ParentElement = this;
            Listeners.Add(listener);
        }

        public void RemoveListener(int id)
        {
            if(Listeners.RemoveAll(x => x.Id == id) <= 0)
                throw new InternalConstraintViolation("Listener with id " + id + " not found in listeners");
        }

        public global::UIElement ToProtobuf()
        {
            global::UIElement result = new global::UIElement();
            result.Id = Id;
            result.Dimensions = DimensionsAdapter.ToProtobuf();
            foreach (ListenerAdapter listener in Listeners)
            {
                result.Listeners.Add(listener.ToProtobuf());   
            }
            result.Type = Type.ToProtobuf();
            return result;
        }
    }
}
