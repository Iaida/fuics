﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Core;

namespace Assets.FUICS.Scripts.logging
{

    [System.Serializable]
    public enum LogLevel
    {
        All,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
        Off,
    }

    public static class LogLevelMethods
    {
        public static Level ToLog4NetLevel(this LogLevel level)
        {
            switch (level)
            {
                case LogLevel.All:
                    return Level.All;
                case LogLevel.Debug:
                    return Level.Debug;
                case LogLevel.Info:
                    return Level.Info;
                case LogLevel.Warn:
                    return Level.Warn;
                case LogLevel.Error:
                    return Level.Error;
                case LogLevel.Fatal:
                    return Level.Fatal;
                case LogLevel.Off:
                    return Level.Off;
                default:
                    throw new ArgumentOutOfRangeException("level", level, null);
            }   
        }
    }
}
