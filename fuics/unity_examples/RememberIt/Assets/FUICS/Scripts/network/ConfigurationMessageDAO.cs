﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using Castle.Core.Internal;
using fuics.exception;
using Fhw.Fuics.Wrapper;
using Google.Protobuf;
using JetBrains.Annotations;
using log4net;
using log4net.Repository.Hierarchy;
using Debug = UnityEngine.Debug;
[assembly: InternalsVisibleTo("Assembly-CSharp-Editor")]
[assembly: InternalsVisibleTo(InternalsVisible.ToDynamicProxyGenAssembly2)]

namespace Fhw.Fuics.Network
{
    // ReSharper disable once InconsistentNaming
    internal class ConfigurationMessageDAO
    {
        /// <summary>
        /// Timeout value of a sending timeout. In ms
        /// </summary>
        private static readonly int SEND_TIMEOUT = 1;

        /// <summary>
        /// Timeout value of a receiving timeout. In ms
        /// </summary>
        private static readonly int RECEIVE_TIMEOUT = 1;

        /// <summary>
        /// The endpoint to which the socket connects. May not be rewritten. For another endpoint, simply
        /// created another <see cref="ConfigurationMessageDAO"/>
        /// </summary>
        private readonly IPEndPoint _fuicsEndPoint;

        /// <summary>
        /// The Socket that is used to write and read messages. Note that this socket should be closed after each Send/Receive transaction. 
        /// </summary>
        private Socket _sendingSocket;

        private static readonly ILog LOG = LogManager.GetLogger(typeof(ConfigurationMessageDAO));

        /// <summary>
        /// Default constructor necessary for testing. 
        /// </summary>
        public ConfigurationMessageDAO()
        {
            

        }

        internal ConfigurationMessageDAO(String hostname, int port)
        {
            IPHostEntry host = Dns.GetHostEntry(hostname);
            IPAddress ipAdress = host.AddressList[0];
            _fuicsEndPoint = new IPEndPoint(ipAdress, port);
            
        }


        /// <summary>
        /// Attempts to create a socket and open a connection to <see cref="_fuicsEndPoint"/>.
        /// <para>
        /// The timeout values used for this connection are configured via <see cref="RECEIVE_TIMEOUT"/> and <see cref="SEND_TIMEOUT"/>
        /// </para>
        /// </summary>
        private bool OpenConnection()
        {
            bool connectionPossible = false;
            LOG.Info("Opening connection to fuics endpoint " + _fuicsEndPoint);
            try
            {
                _sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                IAsyncResult ar = _sendingSocket.BeginConnect(_fuicsEndPoint, null, null);
                bool success = ar.AsyncWaitHandle.WaitOne(3000, true);
                if (success)
                {
                    LOG.InfoFormat("Connection established.");
                    connectionPossible = true;
                }
                else
                {
                    _sendingSocket.Close();
                    LOG.Error("Could not establish connection to fuics endpoint");
                }
                
            }
            catch (System.Exception e)
            {
                LOG.Error("Could not establish connection to fuics endpoint");
            }
            return connectionPossible;;
            
           
        }

        /// <summary>
        /// Attempts to close the connection to <see cref="_fuicsEndPoint"/>. May only be used if a connection exists.
        /// This closes the <see cref="_sendingSocket"/> and performs a close-handshake, so that both parties know the connection
        /// has been disabled. 
        /// </summary>
        private void CloseConnection()
        {
            LOG.Info("Closing connection to fuics endpoint " + _fuicsEndPoint);
            _sendingSocket.Shutdown(SocketShutdown.Both);
            _sendingSocket.Close();
            LOG.Info("Connection closed.");

        }

  

     

        /// <summary>
        /// Sends a given <see cref="ConfigurationMessage"/> to the connected <see cref="_fuicsEndPoint"/>.
        /// <para>
        /// This methods calculates the message header, serializes the message and then waits for a response. This operation
        /// is blocking. The response is then returned. During this process, a new connection is opened and closed after
        /// a response was received. 
        /// </para>
        /// <para>
        /// This is meant for internal use only, as arbitary messages should never be sent. It is encouraged to work on the
        /// existing messages that are request type specific. 
        /// </para>
        /// </summary>
        /// <param name="msg">The message to be serialized and sent</param>
        /// <returns></returns>
        [NotNull]
        private ConfigurationMessage SendMessage([NotNull] ConfigurationMessage msg)
        {
            // Default message -> Unknown error -> Socket problem
            ConfigurationMessage response = new ConfigurationMessage();
            response.Error = ConfigurationError.ErrUnknown;
            LOG.Debug("Attempting to send Configuration Message of type " + msg.RequestType + " with size " + msg.CalculateSize());
            if (OpenConnection())
            {
                Int32 msgSize = msg.CalculateSize();
                byte[] header = Util.Int32ToByteArray(msgSize);
                byte[] msgAsBytes = msg.ToByteArray();
                byte[] msgAndHeaderAsBytes = new byte[msgSize + 4];
                Array.Copy(header, msgAndHeaderAsBytes, 4);
                Array.Copy(msgAsBytes, 0, msgAndHeaderAsBytes, 4, msgSize);
                LOG.Debug("Message as hex bytes:" + Util.ByteArrayToString(msgAndHeaderAsBytes));
                int bytesSend = _sendingSocket.Send(msgAndHeaderAsBytes);
                LOG.Debug(bytesSend + " bytes sent. Waiting for response");
                //Wait for response
                // First, read header.
                byte[] readHeaderBytes = new byte[4];
                _sendingSocket.Receive(readHeaderBytes, 0, 4, SocketFlags.None);
                Int32 bytesToRead = Util.ByteArrayToInt32(readHeaderBytes);
                byte[] readMsg = new byte[bytesToRead];
                _sendingSocket.Receive(readMsg);
                response = ConfigurationMessage.Parser.ParseFrom(readMsg);
                LOG.Debug("Response received: " + response);
                CloseConnection();
                return response;
            }
            return response;
        }

        /// <summary>
        /// Attempts to send register the UI Element and its listeners with fuics. 
        /// <p>
        /// If this operations succeeds, <see cref="ConfigurationError.ErrNoerror"/> is returned and the ID given to the element and its listeners
        /// is given to the uiElement (the object uiElement passed as argument is altered)
        /// </p>
        /// 
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        internal virtual ConfigurationError AddUiElement(UiElementAdapter uiElement)
        {
            ConfigurationMessage msg = new ConfigurationMessage
            {
                RequestType = ConfigurationRequestType.AddElement,
                ElementInfo = uiElement.ToProtobuf()
            };
            ConfigurationMessage response = SendMessage(msg);
            uiElement.Id = response.ElementInfo.Id;
            return response.Error;
        }

        internal virtual ConfigurationError AddListener(ListenerAdapter listener, Int32 id)
        {
            ConfigurationMessage msg = new ConfigurationMessage
            {
                ListenerInfo = listener.ToProtobuf(),
                RequestType = ConfigurationRequestType.AddListener,
                ElementInfo = new UIElement()
                {
                    Id = id
                }
            };
            ConfigurationMessage response = SendMessage(msg);
            if (response.Error.Equals(ConfigurationError.ErrNoerror))
            {
                listener.Id = response.ListenerInfo.Id;
            }
            return response.Error;
        }

        internal virtual ConfigurationError RemoveUiElement(Int32 id)
        {
            
            ConfigurationMessage msg = new ConfigurationMessage
            {
                RequestType = ConfigurationRequestType.RemoveElement,
                ElementInfo = new UIElement(){Id = id}
            };
            return SendMessage(msg).Error;
        }



        internal virtual ConfigurationError RemoveListener(Int32 id)
        {
            ConfigurationMessage msg = new ConfigurationMessage
            {
                RequestType = ConfigurationRequestType.RemoveListener,
                ListenerInfo = {Id = id}
            };
            return SendMessage(msg).Error;
        }

        internal virtual ConfigurationError StartAssist(DimensionsAdapter initialDimensions)
        {
            ConfigurationMessage msg = new ConfigurationMessage();
            msg.Dimensions = initialDimensions.ToProtobuf();
            msg.RequestType = ConfigurationRequestType.Assist;
            return SendMessage(msg).Error;
        }

        internal virtual ConfigurationError FinalizeAssist()
        {
            ConfigurationMessage msg = new ConfigurationMessage();
            msg.RequestType = ConfigurationRequestType.Finalize;
            return SendMessage(msg).Error;
        }

        internal virtual ConfigurationError UpdateCoordinates(DimensionsAdapter dimensions)
        {
            ConfigurationMessage msg = new ConfigurationMessage();
            msg.Dimensions = dimensions.ToProtobuf();
            msg.RequestType = ConfigurationRequestType.UpdateDimensions;
            return SendMessage(msg).Error;
        }

        internal virtual UiStatusAdapter ReplaceStatus(UiStatusAdapter status)
        {
            ConfigurationMessage msg = new ConfigurationMessage();
            msg.RequestType = ConfigurationRequestType.ReplaceStatus;
            msg.Status = status.ToProtobuf();
            return new UiStatusAdapter(SendMessage(msg).Status);
        }

        /// <summary>
        /// Compiles a <see cref="ConfigurationMessage"/> that is used to request
        /// the current status from the server. 
        /// </summary>
        /// <returns>The status which was present on the server during this request</returns>
        internal UiStatusAdapter GetStatus()
        {
            ConfigurationMessage msg = new ConfigurationMessage {RequestType = ConfigurationRequestType.Debug};
            return new UiStatusAdapter(SendMessage(msg).Status);
        }
    }
}
