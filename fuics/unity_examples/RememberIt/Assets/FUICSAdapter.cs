﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets;
using Fhw.Fuics;
using Fhw.Fuics.Wrapper;
using log4net.Core;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FUICSAdapter : MonoBehaviour
{
    private FuicsCore _core;

    private readonly string _host = "172.16.9.122";

    public Camera MainCamera;

    public Button ProgressButton;

    public Controller controller;

    /// <summary>
    /// Called by unity. Used here to instantiate <see cref="FuicsCore"/>, which serves as primary communication point with the server. 
    /// </summary>
    void Awake()
    {
        // This will create a core that connects to 172.16.9.5 as a server.
        // The following tasks will be performed 
        // - Both data access objects are instantiated
        // - A connection to the server is opened, and the dimensions of the virtual canvas
        //   are requested. This is necessary to perform normalization. For more info, see documentation
        _core = FuicsCore.CreateCore(_host);
        // Fuics uses log4net. log4net is configured with the two lines below. 
        // Enable logging. 
        FuicsCore.ConfigureAllLogging("logfile.log");
        // All log events are supposed to be visible.
        FuicsCore.SetLogLEvel(Level.All);
    }



    /// <summary>
    /// This method is used to convert one of the games existing elements into a virtual UI Element used by fuics.
    /// To convert an element, it has to be normalized. This is due the fact that the server expects the dimensions between [0..1], but the
    /// game works in a space defined by the camera. 
    /// </summary>
    /// <returns></returns>
    private UiElementAdapter UiElementFromRememberItElement(RememberItElement element)
    {
        UiElementAdapter uiElement = new UiElementAdapter();
        // To Normalize the element, we need the 'width' and 'height' of our own virtual 'canvas'
        // This is no real canvas, but the viewport created by the camera. This game uses an orthographic
        // camera, which makes the transformation straightforward. The size of our virtual canvas is 
        // camera.size * 2 * ViewportRect.W for width
        // camera.size * 2 * ViewportRect.H for height
        // The game should be running in an 1:1 aspect ratio, so the viewport rect should be irrelevant - in theory.
        float virtualXMin = -MainCamera.orthographicSize * MainCamera.rect.width;
        float virtualXMax = MainCamera.orthographicSize * MainCamera.rect.width;

        float virtualZMin = -MainCamera.orthographicSize * MainCamera.rect.height;
        float virtualZMax = MainCamera.orthographicSize * MainCamera.rect.height;
        // Before normalization, the position has to be offsettet first.
        // In unity, the transform.position is defined as the center
        // of a cube. fuics intepretes a position as the bottom left anchor. 
        float offsettedX = element.transform.position.x - element.Width / 2.0f;
        float offsettedY = element.transform.position.z - element.Height / 2.0f;
        Vector3 normalizedPosition = new Vector3
        {
            x = _core.NormalizeDimX(offsettedX, virtualXMin, virtualXMax),
            // Note that y is assigned here, but the z position is used. This is because the Vector3 in Dimensions is in fact a 2D Vector, with x = width and y = height.
            y = _core.NormalizeDimY(offsettedY, virtualZMin, virtualZMax)
        };
        // The width and height is calculated within the RememberItElement. It only has to be normalized. 
        float normalizedHeight = _core.NormalizeDimY(element.Height, 0, virtualZMax * 2.0f);
        float normaliezdWidth = _core.NormalizeDimX(element.Width, 0, virtualXMax * 2.0f);
        DimensionsAdapter dimensionsAdapter = new DimensionsAdapter(normalizedPosition, normalizedHeight, normaliezdWidth);
        uiElement.DimensionsAdapter = dimensionsAdapter;
        // Now an element was created, but this element has no Listeners. Listeners are necessary to process events sent by the server.
        // Each listener listens to one event type, and only events that are destined for that exact listener are processed.
        // The element will need one listener for 'press'
        ListenerAdapter listenerPress = new ListenerAdapter();
        // This step assigs an ID used by this package to the listener. 
        // This is necessary because with some request types sent by this package, listeners are discarded and rebuilt. When
        // this happens, associations get lost. To avoid this, the Info field is used. The ID field may not be used, as it
        // is overwritten by fuics. 
        // tl;dr: Simply set the info field each time you create a listener. It's safter that way. More detail can be found in the documentation
        listenerPress.Info = FuicsCore.NextId().ToString();
        listenerPress.ListensTo = EventTypeAdapter.Press;
        // This is the important association of listener and delegate. The method assigned as delegate will
        // be invoked if the listener is triggered, which happens when the server sents an event that
        // the listener is interested in. 
        listenerPress.onEventTriggered = element.ClickElement;
        // Now an element has been composed. This will need processing, which is done outside. 
        // Always add a listener this way, and not this way:
        // uiElement.Listeners.Add(listenerPress);
        // The above will not add the backreference from listener to element
        uiElement.AddListener(listenerPress);
        return uiElement;
    }

    /// <summary>
    /// Used to add an remember it element as virtual element to the fuics server.
    /// </summary>
    /// <param name="element"></param>
    /// <returns></returns>
    public void AddRememberItElement(RememberItElement element)
    {
        // This will create a virtual element for the server.
        UiElementAdapter newElement = UiElementFromRememberItElement(element);
        // Elements that are correctly normalized may simply be added with this method. 
        // This will open a connection, send the element, and close it right away. If an error
        // happens, an exception will be thrown. 
        // As not being able to add an element is a critical error, this exception should be left
        // untreated or logged out to Unity. Not being able to add an element is most likely the result
        // of the server being unreachable or some internal server error. 
        //
        // tl;dr: AddUiElement should always work, unless server is down or not working correctly
        _core.AddUiElement(newElement);
        // The core has now added this element to its own internal structures. But this game will
        // need to add and remove elements at will, and for this, IDs are necessary
        //
        // The ID is given by the server. The newElement we passed to the core was mutated in the process
        // and the ID has been set correctly. 
        // Note that this does NOT work with sessioning!
        element.FuicsElementId = newElement.Id;

    }

    public void RemoveRememberItElement(RememberItElement element)
    {
        // We are using -1 as an ID to express: This element is not present on the server
        // This is possible because the server will never assign -1 as an ID. 
        if (element.FuicsElementId != -1)
        {
            _core.RemoveUiElement(element.FuicsElementId);
            element.FuicsElementId = -1;
        }
    }

    private void AddProgressButton()
    {
        UiElementAdapter element = new UiElementAdapter();
        DimensionsAdapter dimensionsAdapter = new DimensionsAdapter();
        Vector3 posNormalized = new Vector3();
        posNormalized.x = _core.NormalizeDimX(-200, -800.0f, 800.0f);
        posNormalized.y = _core.NormalizeDimY(200 - 60, -800.0f, 800.0f);
        dimensionsAdapter.Width = _core.NormalizeDimX(400, 0, 1600);
        dimensionsAdapter.Height = _core.NormalizeDimY(120, 0, 1600);
        dimensionsAdapter.Pos = posNormalized;
        element.DimensionsAdapter = dimensionsAdapter;
        ListenerAdapter listener = new ListenerAdapter();
        listener.ListensTo = EventTypeAdapter.Press;
        listener.Info = FuicsCore.NextId().ToString();
        listener.onEventTriggered = PressButton;
        element.AddListener(listener);
        _core.AddUiElement(element);
    }

    /// <summary>
    /// Completely clears the remote servers status. All elements and listeners will be removed, creating a blank slate. 
    /// <para>
    /// Note that this will clear all existing elements, not only those added by this adapter.
    /// </para>
    /// </summary>
    /// <returns></returns>
    public void Clear()
    {
        // Sessions are used to replace a remote ui status
        // by opening a session and closing it right away, an empty status will be sent to the server, effectivly purging it. 
        _core.OpenReplacingSession();
        _core.CloseReplacingSession();
    }

	// Use this for initialization
	void Start ()
	{
	    AddProgressButton();
        _core.StartListening(); 

       
	}

    public void PressButton(String info)
    {
        ProgressButton.onClick.Invoke();
        ExecuteEvents.Execute(ProgressButton.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

        controller.ButtonClicked = true;
    }
	
	// The update method is used to invoke event processing from the core.
	void Update () {
        // This is everything that is necessary to retreive the servers event and process them
        // by calling this, all listeners that have an incoming event will be notified, and once notified, they
        // will invoke their assigned delegate.
	    _core.InvokeEventProcessing();
	}


}
