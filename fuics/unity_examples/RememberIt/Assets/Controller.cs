﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Random = System.Random;



namespace Assets
{
    public class Controller : MonoBehaviour
    {
        private enum GameState
        {
            Initializing,
            ColorRolouette,
            Initialized,
            Remember,
            Started,
            Done,
            Highscore
        }

        private List<RememberItElement> _elements;

        public Highscore Highscore;
        public float XGap = 0.1f;
        public float YGap = 0.1f;

        public RememberItElement RememberItElementPrefab;

        public EndScreen SuccessScreen;

        public EndScreen FailureScreen;

        private Stopwatch _stopwatch;

        private static Controller _instance;

        private GameState _gameState;

        private Stopwatch _colorChangeStopWatch;

        public EventSystem eventSystem;

        public bool ButtonClicked = false;


        public bool GameRunning
        {
            get { return _gameState == GameState.Started; }
        }

        public static Controller Instance
        {
            get { return _instance; }
        }


        public FUICSAdapter FuicsAdapter;

        public void Awake()
        {
            _stopwatch = new Stopwatch();
            _colorChangeStopWatch = new Stopwatch();
            // Check if this class has already been instantiated. If it has been instantiated,
            // destroy this and throw an error.
            if (_instance != null && _instance != this)
            {
                Debug.LogError("Instance of Controller already existing.Destroying Game object.");
                Destroy(gameObject);
            }
            else
            {
                _instance = this;
            }
            _elements = new List<RememberItElement>();
        }

        
        public void Start () {
            // Create a blank slate to work on. 
            FuicsAdapter.Clear();
            CreateElements();
            _colorChangeStopWatch.Start();

        }

        // Update is called once per frame
        public void Update ()
        {
            ProgressGame();
            ButtonClicked = false;
        }

        public bool MayProgress()
        {
            return Input.GetKeyDown("s") || ButtonClicked;
        }

        /// <summary>
        /// Progresses the game into the next state if any progression is applicable. 
        /// </summary>
        private void ProgressGame()
        {
            if (_gameState == GameState.Initializing)
            {
                ResetElements();
                RandomizeElementStates();
                SuccessScreen.Hide();
                FailureScreen.Hide();
                _gameState = GameState.ColorRolouette;
            }
            else if (_gameState == GameState.ColorRolouette && !MayProgress())
            {
                if (_colorChangeStopWatch.ElapsedMilliseconds >= 800)
                {
                    _colorChangeStopWatch.Reset();
                    _colorChangeStopWatch.Start();
                    RandomizeElementStates();
                }
            }
            else if (_gameState == GameState.ColorRolouette && MayProgress())
            {
                InitializeGame(2, 5);
                _stopwatch.Reset();
                _stopwatch.Start();
                _gameState = GameState.Remember;
            } else if (_gameState == GameState.Remember && MayProgress())
            {   
                StartGame();
            } else if (_gameState == GameState.Started)
            {
                MaybeEndGame();
            } else if (_gameState == GameState.Done && MayProgress())
            {
                Highscore.Show();
                SuccessScreen.Hide();
                FailureScreen.Hide();
                _gameState = GameState.Highscore;
            } else if (_gameState == GameState.Highscore && MayProgress())
            {
                Highscore.Hide();
                ResetElements();
                _gameState = GameState.Initializing;
            }
        }

        /// <summary>
        /// Checks if the game may be ended and ends it if possible. 
        /// </summary>
        private void MaybeEndGame()
        {
            bool done = true;
            bool fail = false;
            // Check all elements for invalid clicks
            foreach (RememberItElement rememberItElement in _elements)
            {
                fail = fail || rememberItElement.DisplayState == ElementState.Explosive;
                if (rememberItElement.OriginalState != ElementState.Explosive)
                {
                    done = !fail && done && (rememberItElement.DisplayState == rememberItElement.OriginalState);
                }
            }
            if (fail)
            {
                _stopwatch.Stop();
                FailureScreen.Show(new TimeSpan(_stopwatch.ElapsedTicks));
                _gameState = GameState.Done;
                Debug.Log("Failed");
            }
            else if (done)
            {
                _stopwatch.Stop();
                HighScoreElement highScoreElement = new HighScoreElement();
                highScoreElement.Score = new TimeSpan(_stopwatch.ElapsedTicks);
                highScoreElement.Actives = 4;
                highScoreElement.Bombs = 4;
                Highscore.AddHighScore(highScoreElement);
                SuccessScreen.Show(new TimeSpan(_stopwatch.ElapsedTicks));
                _gameState = GameState.Done;
                Debug.Log("Success");
            }

        }

        private void StartGame()
        {
            _elements.ForEach(e => e.DisplayState = ElementState.Silent);
            _gameState = GameState.Started;
        }

                


        private void InitializeGame(int explosiveCount, int activeCount)
        {
           
            _elements.ForEach(delegate(RememberItElement e)
            {
                e.OriginalState = ElementState.Silent;
                e.DisplayState = ElementState.Silent;
            });

            Random rand = new Random();
            List<int> indexes = new List<int>(_elements.Count);
            for(int i = 0; i < _elements.Count; i++)
            {
                indexes.Add(i);
         
            }
            // Get activeCount elements randomly from the array
            for (int i = 0; i < explosiveCount; i++)
            {
                // Generate a random index that points toward a real _elements index
                int randIndex = rand.Next(0, indexes.Count);
                // use the random index to access the indexes-array, which points to element-indexes
                _elements[indexes[randIndex]].OriginalState = ElementState.Explosive;
                indexes.RemoveAt(randIndex);
            }
            for (int i = 0; i < activeCount; i++)
            {
                // Generate a random index that points toward a real _elements index
                int randIndex = rand.Next(0, indexes.Count);
                // use the random index to access the indexes-array, which points to element-indexes
                _elements[indexes[randIndex]].OriginalState = ElementState.Active;
                indexes.RemoveAt(randIndex);
            }
            // This is one of the very few points where an association
            // to fuics has to be created. We want all elements that are active or
            // explosive to react to incoming events. If they are not active or explosive,
            // their virtual UI Elements will need to be removed.  
            //
            // Note that there is no logical difference between active and explosive for the server.
            // The server is not interested in game logic. All it does is detect collisions and forward these
            // to clients. 
            // Reset all to silent
            Debug.Log("Writing new Elements to FUICS");
            _elements.ForEach(x =>
            {
                if (x.OriginalState == ElementState.Active || x.OriginalState == ElementState.Explosive)
                {
                    FuicsAdapter.RemoveRememberItElement(x);
                    FuicsAdapter.AddRememberItElement(x);
                }
                else
                {
                    FuicsAdapter.RemoveRememberItElement(x);
                }
            });
        }

        private void ResetElements()
        {
            foreach (var rememberItElement in _elements)
            {
                rememberItElement.DisplayState = ElementState.Silent;
                rememberItElement.OriginalState = ElementState.Silent;
            }
        }

        /// <summary>
        /// Assigns a random elmenet state to each element
        /// </summary>
        private void RandomizeElementStates()
        {
            _elements.ForEach(delegate(RememberItElement e)
            {
                e.DisplayState = ElementStateMethods.GetRandomElementState();
            });
        }

        public void ClickButton()
        {
            ButtonClicked = true;
        }

        private void CreateElements()
        {

            for (int i = -4; i <= 4; i++)
            {
                for (int j = -4; j <= 4; j++)
                {
                    bool isCenter = (i <= 1 && i >= -1 && j <= 1 && j >= -1);
                    //bool isU = 
                    if (!isCenter && 
                        !(j <= -2))
                    {
                        RememberItElement element = Instantiate(RememberItElementPrefab);
                        element.DisplayState = ElementState.Silent;
                        element.transform.position = new Vector3(i + i * XGap, 0, j + j * YGap);
                        _elements.Add(element);
                    }
                }
            }
        }
    }
}
