﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public enum ElementState { Silent = 0, Explosive = 1, Active = 2 }

    public static class ElementStateMethods
    {
        private static Random rand = new Random();
        public static ElementState GetRandomElementState()
        {
            int randNum = rand.Next(0, 3);
            return (ElementState)randNum;
        }
    }
}
