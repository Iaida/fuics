﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public struct HighScoreElement
    {
        public TimeSpan Score;
        public int Bombs;
        public int Actives;

        public int CompareTo(HighScoreElement other)
        {
            return Score.CompareTo(other);
        }
    }
    public class Highscore : MonoBehaviour
    {

        public Text HighScoreText;
        private List<HighScoreElement> _elements;

        void Awake()
        {
            _elements = new List<HighScoreElement>();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
        public void Show()
        {
            gameObject.SetActive(true);
            String highscoreString = "";
            //_elements.Sort();
            _elements.Sort((e1, e2) => e1.Score.CompareTo(e2.Score));
            int index = 1;
            _elements.ForEach(delegate(HighScoreElement e)
            {
                if (index <= 10)
                {
                    highscoreString += index + ". - " + e.Score.Seconds + ":" + e.Score.Milliseconds + " seconds on " + e.Bombs + "b and " + e.Actives + "a\n";
                }
                index++;
            });
            HighScoreText.text = highscoreString;
        }

        public void AddHighScore(HighScoreElement e)
        {
            _elements.Add(e);
        }
    }
}
