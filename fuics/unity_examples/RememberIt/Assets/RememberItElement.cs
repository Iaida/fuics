﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
   

    public class RememberItElement : MonoBehaviour
    {

        public Material MaterialSilent;
        public Material MaterialExplosive;
        public Material MaterialActive;
        public int Position;

        // Clicked by user yes/no
        public bool Clicked = false;

        private Dictionary<ElementState, Material> _materials;

        //private Controller _controller;

        private ElementState _originalState;

        private ElementState _displayState;

        private Controller _controller;

        /// <summary>
        /// The ID of the fuics element that represents this element. -1 is never assgiend by the server,
        /// this is why -1 represents: Not present at the server. 
        /// </summary>
        public int FuicsElementId = -1;

        /// <summary>
        /// The width of this rememberItElement, but not normalized. This is needed for communication with fuics,
        /// and represents the width of the virtual UI element. This makes use of the fact that a scale of 1/1/1 represents
        /// a size of 1/1/1
        /// </summary>
        public float Width
        {
            get { return transform.lossyScale.x * 1.0f; }
        }

        /// <summary>
        /// The height of this rememberItElement, but not normalized. This is needed for communication with fuics,
        /// and represents the height of the virtual UI element. This makes use of the fact that a scale of 1/1/1 represents
        /// a size of 1/1/1
        /// </summary>
        public float Height
        {
            get { return transform.lossyScale.z * 1.0f; }
        }



        public ElementState OriginalState
        {
            get { return _originalState; }
            set
            {
                MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
                meshRenderer.material = _materials[value];
                _originalState = value;
            }
        }

        public ElementState DisplayState
        {
            get { return _displayState; }
            set
            {
                MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
                meshRenderer.material = _materials[value];
                _displayState = value;
            }
        }

        // Use this for initialization
        public void Awake ()
        {

		    _materials = new Dictionary<ElementState, Material>();
            _materials.Add(ElementState.Silent, MaterialSilent);
            _materials.Add(ElementState.Explosive, MaterialExplosive);
            _materials.Add(ElementState.Active, MaterialActive);

        }

        public void Start()
        {
            _controller = Controller.Instance;
        }

        // Update is called once per frame
        public void Update () {
		
        }

        /// <summary>
        /// This is used as callback given to fuics. 
        /// </summary>
        /// <param name="info">not relevant</param>
        public void ClickElement(string info)
        {
            OnMouseDown();
        }

        public void OnMouseDown()
        {
            if (_controller.GameRunning)
            {
                if (OriginalState == ElementState.Explosive)
                {
                    DisplayState = ElementState.Explosive;
                }
                else if (OriginalState == ElementState.Active)
                {
                    DisplayState = ElementState.Active;
                }
            }
        }

    }
}
