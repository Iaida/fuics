﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Assets.FUICS;
using Castle.Core.Internal;
using Fhw.Fuics.Exception;
using Fhw.Fuics.Network;
using Fhw.Fuics.Wrapper;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;
using UnityEngine;
using UnityEngine.UI;
[assembly: InternalsVisibleTo("Assembly-CSharp-Editor")]

/// <summary>
/// Delegate describing a Unity button click invocation. Only for library internal use.
/// </summary>
internal delegate void InternalInvokeButtonClick();


namespace Fhw.Fuics
{
    /// <summary>
    /// Provides core functionalties for fuics server access. 
    /// <p>
    /// The core is not intended to be used seperately. Insteady, high- and lowlevel APIs make use of this Core.
    /// </p>
    /// <p>
    /// Duplicated cores are problematic, especially when it comes to event processing. Multiple cores could receive the same event, resulting in the same
    /// event being forwarded to unity twice. To work around this issues, cores can only created with a factory method (<see cref="CreateCore"/>). This factory method
    /// only allows creation of cores unique per hostname and port combination. 
    /// </p>
    /// <p>
    /// The core also allows working on sessions. By default, all operations are simply translated into fuics protocol messages and immediatly sent to the server. 
    /// After the message has been sent, the connection is closed.
    /// Alternatively, a 'replacing' sessions can be opened. All operations are cached and, once completed, are sent via an ReplaceStatus request. This is useful to easily
    /// compose a new status by calling add and remove methods, and then, once completed, replace the UI status entirely. 
    /// </p>
    /// </summary>
    public class FuicsCore
    {

        private static readonly Int32 CONFIG_PORT = 5000;
        private static readonly Int32 EVENT_PORT = 5001;

        /// <summary>
        /// Stores all created cores. Cores are unique per combination of hostname and port. 
        /// </summary>
        private static Dictionary<string, FuicsCore> _cores = new Dictionary<string, FuicsCore>();

        /// <summary>
        /// Map of all UiElements currently managed by the core. Elements registered <em>by this</em> core added to the dictionary, and elements
        /// removed by this core removed from it. 
        /// </summary>
        private readonly Dictionary<int, UiElementAdapter> _elements = new Dictionary<int, UiElementAdapter>();

        private readonly Dictionary<int, ListenerAdapter> _listeners = new Dictionary<int, ListenerAdapter>();

        private bool _sessionOpen = false;

        private float _fuicslCanvasWidthHigh = 1.0f;
        private float _fuicsCanvasWidthLow = 0.0f;

        /// <summary>
        /// 
        /// </summary>
        private float _fuicsCanvasHeightHigh = 1.0f;

        private float _fuicsCanvasHeightLow = 0.0f;

        /// <summary>
        /// Used to handle network actions for configuration messages
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        // ReSharper disable once InconsistentNaming
        private ConfigurationMessageDAO configurationMessageDao;


        /// <summary>
        /// Used to handle network actions for event messages. Peristent TPC connection
        /// </summary>
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private EventMessageDAO eventMessageDao;

        /// <summary>
        /// Status into which session based operations are written. 
        /// </summary>
        private UiStatusAdapter _currentStatus;

        private static int _currentId = 200;

        private static readonly ILog Log = LogManager.GetLogger(typeof(FuicsCore));

        /// <summary>
        /// Only allows private construction to avoid duplicated cores connected to the same host.
        /// </summary>
        private FuicsCore(string hostname)
        {
            configurationMessageDao = new ConfigurationMessageDAO(hostname, CONFIG_PORT);
            eventMessageDao = new EventMessageDAO(hostname, EVENT_PORT);
        }

        public static int NextId()
        {
            return ++_currentId;
        }

        /// <summary>
        /// Initializes the size of the virtual canvas - to which normalization will be performed - with the values
        /// defined by the server. This performs a synchronous network accesss.
        /// </summary>
        public void InitCanvasDimensions()
        {
            UiStatusAdapter initialStatus = configurationMessageDao.GetStatus();
            _fuicsCanvasHeightHigh = initialStatus.CanvasHeight;
            _fuicslCanvasWidthHigh = initialStatus.CanvasWidth;
            Log.Debug("New Canvas dimensions: " + _fuicsCanvasHeightHigh + " / " + _fuicslCanvasWidthHigh);
        }

        /// <summary>
        /// Creates or retreives a fuics core. If no core for the given hostname exists, a new core
        /// is created. If a core already exists, the instance with the given hostname is returned. 
        /// </summary>
        /// <param name="hostname"></param>
        /// <returns></returns>
        public static FuicsCore
            CreateCore(string hostname)
        {
            FuicsCore result;
            if (!_cores.TryGetValue(hostname, out result))
            {
                result = new FuicsCore(hostname);
                _cores.Add(hostname, result);
            }
            return result;
        }

        public static void ClearCores()
        {
            _cores.Clear();
        }



        private float map(float oldMin, float oldMax, float newMin, float newMax, float value)
        {
            return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
        }


        public float NormalizeDimX(float x, float low, float high)
        {
            return map(low, high, _fuicsCanvasWidthLow, _fuicslCanvasWidthHigh, x);
        }


        public float NormalizeDimY(float y, float low, float high)
        {
            return map(low, high, _fuicsCanvasHeightLow, _fuicsCanvasHeightHigh, y);
        }

        /// <summary>
        /// Removes the listener with the given id. 
        /// </summary>
        /// <param name="id"></param>
        public void RemoveListener(Int32 id)
        {

            if (_listeners.ContainsKey(id))
            {
                ConfigurationError error = configurationMessageDao.RemoveListener(id);
                if (error == ConfigurationError.ErrNoerror)
                {
                    ListenerAdapter l = _listeners[id];
                    _listeners.Remove(id);
                    // Listener must have been attached to an element. Remove also.
                    l.ParentElement.RemoveListener(id);
                    
                }
                else
                {
                    throw new InternalConstraintViolation("Server respondend with " + error);
                }
            }
            else
            {
                Log.Error("No listener for id " + id + " found.");        
            }
            
        }



        /// <summary>
        /// Removes the element with the given ID. Also removes all listeners attached to it. 
        /// <para>
        /// This represents a single call sent to the server.
        /// </para>
        /// </summary>
        /// <param name="id"></param>
        public void RemoveUiElement(Int32 id)
        {
            if (_elements.ContainsKey(id))
            {
                UiElementAdapter element = _elements[id];
                // Check if every listener attached to the element is represented in the
                // _listeners list.
                element.Listeners.ForEach(x =>
                {
                    if(!_listeners.ContainsKey(x.Id))
                        throw new InternalConstraintViolation("No listener for id " + id + " found. Was attached to element with id " + element.Id);
                });
                // Remove the element. Only progress if no error occured.
                ConfigurationError error = configurationMessageDao.RemoveUiElement(id);
                if (error == ConfigurationError.ErrNoerror)
                {
                    element.Listeners.ForEach(x => _listeners.Remove(x.Id));
                    _elements.Remove(id);
                }
                else
                {
                    throw new InternalConstraintViolation("Server respondend with " + error);
                }
            }
            else
            {
                throw new InternalConstraintViolation("No element for id " + id + " found.");
            }
        }




        /// <summary>
        /// Adds an element and forwards it to the fuics server. Backreferences from the eleements
        /// listeners back to the element do not have to be set. They will be overwritten
        /// in this method. 
        /// </summary>
        /// <param name="element"></param>
        public void AddUiElement(UiElementAdapter element)
        {
            if (_sessionOpen)
            {
                // No need to overwrite the backreferences here, as
                // the original elements will be discarded an replaced by
                // element sent from the server.
                _currentStatus.AddElement(element);
            }
            else
            {
                // For reasons of simplification, remove the listeners from the element before adding it. 
                // The listeners will be added one by one
                // TODO: might be an option to use replace status
                List<ListenerAdapter> listeners = element.Listeners;
                element.Listeners = new List<ListenerAdapter>();
                configurationMessageDao.AddUiElement(element);
                // Add the element into the dictionary. 
                _elements.Add(element.Id, element);
                // Adding the listeners later. This way, the info-strings can safely be ignored, and no
                // additional matching has to be performed, simplifying the whole process.
                listeners.ForEach(l =>
                {
                    l.ParentElement = element;
                    AddListener(element.Id, l);
                });
            }
        }

        /// <summary>
        /// Adds a listener.
        /// <para>
        /// The listeners <see cref="ListenerAdapter.ParentElement"/> doesn't have to be set, as
        /// the reference will be overwritten by this method. 
        /// </para>
        /// </summary>
        /// <param name="elementId"></param>
        /// <param name="listener"></param>
        /// <returns></returns>
        public Int32 AddListener(Int32 elementId, ListenerAdapter listener)
        {
            UiElementAdapter element;
            Int32 id = -1;
            if (_elements.TryGetValue(elementId, out element))
            {
                // Add it to the element (this will persist into the list)
                element.AddListener(listener);
                // This will write the ID into the listener
                configurationMessageDao.AddListener(listener, elementId);
                if (id == 0) throw new InternalConstraintViolation("Id may not be 0.");
                _listeners.Add(listener.Id, listener);
                id = listener.Id;
            }
            else
            {
                throw new InternalConstraintViolation("Element with the id " + elementId + " does not exist.");
            }
            return id;
        }



        /// <summary>
        /// Opens a replacing session. A replacing session will cache all <see cref="AddUiElement(UiElementAdapter)"/> operations
        /// until <see cref="FlushReplacingSession"/> or <see cref="CloseReplacingSession"/> is called. This way, a whole UI status can be
        /// progressibly generated. 
        /// </summary>
        /// <returns></returns>
        public bool OpenReplacingSession()
        {
            if (!_sessionOpen)
            {
                ClearReplacingSession();
                _sessionOpen = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Flushes the replacing session without discarding it's actions. The session remains open, the current status
        /// is transmitted to the server, but remains.
        /// </summary>
        public void FlushReplacingSession()
        {
            if (_sessionOpen)
            {
                ReplaceStatus();
            }
        }

        /// <summary>
        /// Closes the replacing sessions, sends the status to the server and discards the current status afterwards. 
        /// </summary>
        public void CloseReplacingSession()
        {
            if (_sessionOpen)
            {
                ReplaceStatus();
                ClearReplacingSession();
            }
        }

        /// <summary>
        /// Clears the whole internal status and all data structures associated with it.
        /// Does not clear <see cref="_currentStatus"/>
        /// </summary>
        private void ClearInternalStructures()
        {
            _elements.Clear();
            _listeners.Clear();
        }

        /// <summary>
        /// FIXME nt comment !IMPORTANT
        /// </summary>
        /// <param name="referenceListeners"></param>
        /// <param name="listener"></param>
        private void SetDelegatesOfListener(List<ListenerAdapter> referenceListeners, ListenerAdapter listener)
        {
            ListenerAdapter reference = referenceListeners.Find(x => x.Info == listener.Info);
            listener.onEventTriggered = reference.onEventTriggered;
            listener.onTriggerInternal = reference.onTriggerInternal;
        }

        private void ReplaceStatus()
        {
            ClearInternalStructures();
            List<ListenerAdapter> oldlisteners = _currentStatus.GetAllListeners();
            HashSet<String> infos = new HashSet<string>();
            // Validate that alle infos are unique. They are necessary to map the listeners
            // returned by the server back to their originals
            // for more info, refer to wiki. 
            oldlisteners.ForEach(l =>
            {
                if (infos.Contains(l.Info))
                    throw new InternalConstraintViolation(l.Info + " already existing in listeners" +
                                                          "given to replace status");
                infos.Add(l.Info);
            });
            _currentStatus = configurationMessageDao.ReplaceStatus(_currentStatus);
            foreach (var currentStatusElement in _currentStatus.Elements)
            {
                _elements.Add(currentStatusElement.Id, currentStatusElement);
                foreach (var listenerAdapter in currentStatusElement.Listeners)
                {
                    _listeners.Add(listenerAdapter.Id, listenerAdapter);
                    SetDelegatesOfListener(oldlisteners, listenerAdapter);
                }
            }
        }

        /// <summary>
        /// Resets the current replacing sessions. All cached operations are discarded.
        /// </summary>
        public void ClearReplacingSession()
        {
            _currentStatus = new UiStatusAdapter();
        }

        public void StartListening()
        {
            eventMessageDao.Open();
        }

        public void EndListening()
        {
            eventMessageDao.Close();
        }

        public void StartCalibration(DimensionsAdapter dimensions)
        {
            configurationMessageDao.StartAssist(dimensions);
        }

        public void EndCalibration()
        {
            configurationMessageDao.FinalizeAssist();
        }


        /// <summary>
        /// <list type="number">
        ///     <item>
        ///          <description>Invokes reading of the event channel. Reads all bytes from the socket, interpretes them as EventMessage</description>
        ///     </item>
        ///     <item>
        ///          <description>For each event, determines the listener that caused the event to fire. </description>   
        ///     </item>
        ///     <item>
        ///          <description>On each listener, <see cref="ListenerAdapter.Invoke()"/>  is called. </description>
        ///     </item>
        /// </list>
        /// </summary>
        public void InvokeEventProcessing()
        {
            List<EventMessageAdapter> messages = eventMessageDao.ReadEvents();
            foreach (EventMessageAdapter msg in messages)
            {
                Log.Debug("Event of type " + msg.EventType + " received.");
                ListenerAdapter listener;
                _listeners.TryGetValue(msg.ListenerId, out listener);
                if (listener != null)
                {

                    listener.Invoke();
                }
                else
                {
                    // This core did not register the listener. Simply ignore it. 
                }
            }
        }

        /// <summary>
        ///  Configure logging to write to Logs\EventLog.txt and the Unity console output.
        /// Used instead of an xml file to simplify dependencies. 
        /// </summary>
        public static void ConfigureAllLogging(String filename)
        {
            
            var patternLayout = new PatternLayout
            {
                ConversionPattern = "%date %-5level %logger - %message%newline"
            };
            patternLayout.ActivateOptions();

            // setup the appender that writes to Log\EventLog.txt
            var fileAppender = new RollingFileAppender
            {
                AppendToFile = false,
                File = @"Logs\" + filename,
                Layout = patternLayout,
                MaxSizeRollBackups = 5,
                MaximumFileSize = "5mb",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            fileAppender.ActivateOptions();

            var patternLayoutUnity = new PatternLayout
            {
                ConversionPattern = "%date %-5level %logger - %message%newline"
            };
           

            var unityLogger = new UnityAppender
            {
                Layout = patternLayoutUnity
            };
            patternLayoutUnity.ActivateOptions();
            unityLogger.ActivateOptions();

            BasicConfigurator.Configure(unityLogger, fileAppender);
        }

        public static void SetLogLEvel(Level level)
        {
            ((log4net.Repository.Hierarchy.Hierarchy) LogManager.GetRepository()).Root.Level = level;
        }


    }
}


