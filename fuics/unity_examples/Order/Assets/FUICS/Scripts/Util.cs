﻿using System;
using System.Text;

namespace fuics.exception
{
    internal class Util
    {
        /// <summary>
        /// IO
        /// </summary>
        /// <param name="ba"></param>
        /// <returns></returns>
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b).Append(" ");
            return hex.ToString();
        }

        /// <summary>
        /// Interpretes the given byte array as <see cref="int"/> 32 bit.
        /// </summary>
        /// <param name="bytes">Byte array. May only be 4 bytes long. Little Endian encoding</param>
        /// <returns>Int represented by the byte array.</returns>
        public static Int32 ByteArrayToInt32(byte[] bytes)
        {
            if(bytes.Length != 4) throw new SystemException("Only 4 bytes may be converted to int32");
            return BitConverter.ToInt32(bytes, 0);
        }

        public static byte[] Int32ToByteArray(Int32 value)
        {
            return BitConverter.GetBytes(value);
        }
    }
}
