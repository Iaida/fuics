﻿using System;
using System.Collections.Generic;
using Assets.FUICS.Scripts.logging;
using Fhw.Fuics;
using Fhw.Fuics.Wrapper;
using log4net;
using log4net.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts
{

 
    /// <summary>
    /// Allows association of Unity's buttons with fuics event types.
    /// <p>
    /// Used to bind Unity's OnClick event to various different fuicsEvents.
    /// </p>
    /// </summary>
    /// 
    [System.Serializable]
    public class ButtonCapsule
    {

        public Button UnityButton;
        public EventTypeAdapter FuicsEventType;
        internal UiElementAdapter UiElementAdapter;

        /// <summary>
        /// The position the button had last update. Used to detectet button movement. 
        /// </summary>
        internal Vector2 PreviousPosition;

        /// <summary>
        /// Used to bind <see cref="Button.onClick"/> with a delegate. 
        /// Only for library internal use. If working with Unity, work directly with <see cref="Button.onClick"/>
        /// </summary>
        public void InvokeButtonClick()
        {
            ExecuteEvents.Execute(UnityButton.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
        }

    }

    /// <summary>
    /// High Level API that integrates into Unitys available component based system. 
    /// <para>
    /// This class is intended to be attached to a Unity game object and it's values are initialized with 
    /// the Unity inspector. Upon initialization, the API will connect itself to the server, configure it, start listening to the
    /// servers event channel and, when an event is fired, forward it to Unitys event system. 
    /// </para>
    /// <para>
    /// If wished, the API may also disable all other input except the one received from the fuics server. This way, only fuics server events
    /// will trigger unity events. Manual, mouse or key based UI interaction is locked for all given buttons. 
    /// </para>
    /// A note on internal behavior: This class only serves as easy-initialization component for <see cref="FuicsCore"/>. All logical operations are performed within
    /// the core. 
    /// </summary>
    public class FuicsHighLevelApi : MonoBehaviour
    {
        /// <summary>
        /// FIXME @nt comment
        /// </summary>
        public string Hostname = "172.16.9.5";

        public String LogFileName = "fuicsLog.log";
        /// <summary>
        /// FIXME @nt comment
        /// </summary>
        public int Port = 5000;

        /// <summary>
        /// Log level this lib uses. Only used during launch.
        /// </summary>
        public LogLevel LibLogLevel;

        /// <summary>
        /// Disables all manual input and only allows event forwarding from the fuics server to Unity's event system.
        /// FIXME @nt implement
        /// </summary>
        public bool DisableManualInput = false;

        /// <summary>
        /// Used to intiailize the structure. This may be modified at will, as each call of <see cref="Start"/> or <see cref="ReloadButtons"/>
        /// will copy the current array into a list. 
        /// <see cref="ReloadButtons"/> 
        /// </summary>
        public ButtonCapsule[] Buttons;

        /// <summary>
        /// Defines if the API should be initialized immediatly - within the <see cref="Start"/> method. 
        /// <para>
        /// If not initialized immediatly, manually call <see cref="ReloadButtons"/>
        /// </para>
        /// </summary>
        public bool InitializeImmediatly = true;

        /// <summary>
        /// Defines if this component should start listening for events imemdiatly after initialization is done. This only defines the behaviour
        /// for the <see cref="Start"/> method, as its paramters may not be influenced. 
        /// <para>
        /// If listening is not starting immediatly, it has to be manually enabled with <see cref="StartListening"/>. If listening is not started, no events
        /// will be processed.
        /// </para>
        /// <para>
        /// Listening immediatly after initialization is encouraged.
        /// </para>
        /// </summary>
        public bool StartListeningImmediatly = true;

        /// <summary>
        /// Contains all necessary logical operations to perform client<> server communication. Also maintains a constant 
        /// representations of the servers status and all necessary operations to perform conversions. 
        /// </summary>
        private FuicsCore _core;

        /// <summary>
        /// The <see cref="Buttons"/> array is saved into this list to allow runtime modifications of <see cref="Buttons"/>
        /// </summary>
        private List<ButtonCapsule> _internalButtons;

        private static readonly ILog Log = LogManager.GetLogger(typeof(FuicsHighLevelApi));
        

        private Canvas GetParentCanvas(Button button)
        {
            return button.gameObject.GetComponentInParent<Canvas>();
        }

        internal UiElementAdapter UnityButtonToNormalizedElement(Button button)
        {
            UiElementAdapter result = new UiElementAdapter();
            RectTransform rect = button.image.rectTransform;

            
            Rect canvasRect = GetParentCanvas(button).pixelRect;
            float normX = _core.NormalizeDimX(rect.anchorMin.x + rect.offsetMin.x, -canvasRect.width / 2.0f, canvasRect.width / 2.0f);
            float normY = _core.NormalizeDimY(rect.anchorMin.y + rect.offsetMin.y, -canvasRect.height / 2.0f, canvasRect.height / 2.0f);
            DimensionsAdapter dimensions = 
                new DimensionsAdapter(
                    new Vector3(normX, normY), 
                    _core.NormalizeDimX(rect.rect.height, 0.0f, canvasRect.height), 
                    _core.NormalizeDimY(rect.rect.width, 0.0f, canvasRect.width));
            result.DimensionsAdapter = dimensions;
            Log.Debug("Scaled button with dimensions [" + rect.position.x + "/" + rect.position.y + "][" + rect.rect.width + "/" + rect.rect.height + "] to" +
                    dimensions.ToString());
            return result;
        }

        /// <summary>
        /// Invokes re-reading of all available buttons in the <see cref="Buttons"/> array. This will replace the existing
        /// UI Status at the server with a new one.
        /// <para>
        /// Use this method to re-load the UI during runtime of the game</para>
        /// </summary>
        public void ReloadButtons(bool listenImmediatly)
        {
            _core.OpenReplacingSession();
            foreach (var button in Buttons)
            {
                if (button.UnityButton != null)
                {
                    // Add the button into the                  
                    ListenerAdapter listener = new ListenerAdapter
                    {
                        Info = FuicsCore.NextId().ToString(),
                        ListensTo = button.FuicsEventType,
                        onTriggerInternal = button.InvokeButtonClick
                    };
                    UiElementAdapter element = UnityButtonToNormalizedElement(button.UnityButton);
                    element.Type = ElementTypeAdapter.Rectangle;
                    element.AddListener(listener);
                    button.UiElementAdapter = element;
                    button.PreviousPosition = button.UnityButton.image.rectTransform.anchoredPosition;
                    _internalButtons.Add(button);
                    // Add the element. During this process, the ID is added to it. 
                    _core.AddUiElement(element);
                }
            }
            _core.CloseReplacingSession();
            if(listenImmediatly)
            {
                _core.StartListening();
            }
        }

        public void StartListening()
        {
            _core.StartListening();
        }

        public void EndListening()
        {
            _core.EndListening();
        }


        public void StartCalibration(DimensionsAdapter initialPos)
        {
            // Normalize Dimensions
            Log.Info("Starting calibration. Initial pos " + initialPos);
            initialPos.Height = _core.NormalizeDimY(initialPos.Height, 0, Screen.height);
            initialPos.Width = _core.NormalizeDimX(initialPos.Width, 0, Screen.width);
            initialPos.Pos.y = _core.NormalizeDimY(initialPos.Pos.y, 0, Screen.height);
            initialPos.Pos.x = _core.NormalizeDimX(initialPos.Pos.x, 0, Screen.width);
            Log.Info("Normalized to " + initialPos);
            _core.StartCalibration(initialPos);

        }

        public void EndCalibration()
        {
            _core.EndCalibration();
        }

        void Awake()
        {
            FuicsCore.ConfigureAllLogging(LogFileName);
            FuicsCore.SetLogLEvel(LibLogLevel.ToLog4NetLevel());
        }
        /// <summary>
        /// Called by Unity during initialization of an instance.
        /// <para>
        /// Never call manually. 
        /// </para>
        /// </summary>
        void Start()
        {
            Log.Info(".NET Version: " + System.Environment.Version);
            _internalButtons = new List<ButtonCapsule>();
            _core = FuicsCore.CreateCore(Hostname);
            _core.InitCanvasDimensions();
            if(InitializeImmediatly)
            {
                ReloadButtons(StartListeningImmediatly);
            }
        }

        

        /// <summary>
        /// Buttons may be removed during runtime. When this happens, the fuics ui element has to be removed, too. 
        /// </summary> 
        private void HandlePossibleDesturctions()
        {
            _internalButtons.RemoveAll(delegate (ButtonCapsule button)
            {
                if(button.UnityButton.IsDestroyed())
                {
                    Log.Debug("Removing destructed UI Element" + button.UnityButton);
                    _core.RemoveUiElement(button.UiElementAdapter.Id);
                    return true;
                }
                return false;
            });
        }

        /// <summary>
        /// Moves an ui element by remove and re-adding it directly afterwards. 
        /// </summary>
        /// <param name="toMove"></param>
        /// <param name="newPos"></param>
        private void MoveUiElement(UiElementAdapter toMove, Vector2 newPos)
        {
            _core.RemoveUiElement(toMove.Id);
            toMove.DimensionsAdapter.Pos = new Vector3(newPos.x, newPos.y);
            _core.AddUiElement(toMove);
        }

        private void HandlePossibleMovement()
        {
            foreach (var internalButton in _internalButtons)
            {
                Vector2 currentPos = internalButton.UnityButton.image.rectTransform.anchoredPosition;
                if (currentPos != internalButton.PreviousPosition)
                {
                    MoveUiElement(internalButton.UiElementAdapter, currentPos);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            _core.InvokeEventProcessing();
            HandlePossibleDesturctions();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                foreach (var buttonCapsule in Buttons)
                {
                    buttonCapsule.InvokeButtonClick();
                }
            }

        }

        

    }
}
