﻿using System;

namespace Fhw.Fuics.Exception
{
    /// <summary>
    /// Exception thrown when an internal fuics constraint was violated.
    /// </summary>
    public class InternalConstraintViolation : System.Exception {

        public InternalConstraintViolation()
        {
            
        }

        public InternalConstraintViolation(String message) :base(message)
        {
            
        }

        public InternalConstraintViolation(String message, System.Exception inner) 
            : base(message, inner)
        {

        }

    }
}
