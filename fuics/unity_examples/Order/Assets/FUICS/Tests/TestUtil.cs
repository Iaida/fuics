﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Assets.Tests
{
    class TestUtil
    {
        public static void Inject(Object o, String fieldName, Object value)
        {
            var prop = o.GetType().GetField(fieldName, BindingFlags.NonPublic
                | BindingFlags.Instance);
            prop.SetValue(o, value);
        }

        public static Object Read(Object o, String fieldName, Type type)
        {
            var fieldInfo = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (fieldInfo != null)
                return fieldInfo.GetValue(o);
            else return null;
        }
    }
}
