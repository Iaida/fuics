﻿using System.Collections.Generic;
using Castle.Core.Internal;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace Assets.Order
{
    public enum GameState
    {
        NotRunning,
        Counting,
        Glowing,
        Running, 
        FinSucc,
        FinFail
    }

    public class OrderController : MonoBehaviour
    {
        #region InspectorFields
        public OrderButton ButtonUp;
        public OrderButton ButtonLeft;
        public OrderButton ButtonRight;

        public Text CurrentRoundsText;

        private int _rounds = 0;

        public float ColoredForSeconds = 0.5f;

        public Text CounterText;
        #endregion
        private List<int> _currentOrder = new List<int>();

        private readonly Dictionary<int, OrderButton> _buttonsById = new Dictionary<int, OrderButton>();

        /// <summary>
        /// Buttons that still need to glow
        /// </summary>
        private List<OrderButton> _remaining = new List<OrderButton>();

        private readonly Random _rand = new Random();

        private GameState _state = GameState.NotRunning;

        private float _bufferTime = 0.0f;
        private float _maxBufferTime = 0.2f;

        private float _flashTime = 0.0f;
        private float _maxFlashTime = 0.1f;

        private readonly float _totalMaxFlashTime = 5.0f;
        private float _totalFlashTime = 0.0f;

        private readonly float _countSecondsMax = 4.0f;
        private float _countSeconds = 0.0f;

        private void LogOrder()
        {
            string res = "";
            _currentOrder.ForEach(
                x => res += "->" + x);
            Debug.Log(res);
        }

        public void GenerateOrder()
        {
            _currentOrder = new List<int>(_rounds);
            for(int i = 0; i < _rounds; i++)
            {
                _currentOrder.Add(_rand.Next(0, 3));     
            }

            LogOrder();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="btn"></param>
        public void Pressed(OrderButton btn)
        {
            if (_state == GameState.Running && _currentOrder.Count > 0)
            {
                if (_currentOrder[0] == btn.Id)
                {
                    btn.GlowCorrect(ColoredForSeconds);
                    _currentOrder.RemoveAt(0);
                }
                else
                {
                    btn.GlowFail(ColoredForSeconds);
                    FailGame();
                }
            }
            
        }

        public void FailGame()
        {
            Debug.Log("Game failed");
            _currentOrder.Clear();
            _state = GameState.FinFail;
        }

        public void StartGame()
        {
            _rounds++;
            CurrentRoundsText.text = "Current Round\n" + _rounds;
            GenerateOrder();
            _state = GameState.Counting;
            _remaining = new List<OrderButton>();
            _currentOrder.ForEach(x => _remaining.Add(_buttonsById[x]));
            
            
        }

        public void StopGame()
        {
            _rounds = 0;
            CurrentRoundsText.text = "Current Round\n--";
            CounterText.text = "";
            _buttonsById.Values.ForEach(x => x.GlowNormal());
            _state = GameState.NotRunning;  
        }

        private void FlashFailing()
        {
           
            if (_flashTime >= _maxFlashTime)
            {
                _flashTime = 0.0f;
                foreach (var orderButton in _buttonsById.Values)
                {
                    orderButton.GlowFail(_maxFlashTime * 2);
                }
            }
            else if (!_buttonsById[0].Glowing)
            {
                _flashTime += Time.deltaTime;
            }
        }

        private void FlashSuccess()
        {
            if (_totalFlashTime < _totalMaxFlashTime)
            {
                if (_flashTime >= _maxFlashTime)
                {
                    _flashTime = 0.0f;
                    foreach (var orderButton in _buttonsById.Values)
                    {
                        orderButton.GlowCorrect(_maxFlashTime * 2);
                    }
                }
                else if (!_buttonsById[0].Glowing)
                {
                    _flashTime += Time.deltaTime;
                }
            }
            else
            {
                _totalFlashTime += Time.deltaTime;
            }

        }

        

        private void Count()
        {
            if (_countSeconds >= _countSecondsMax)
            {
                CounterText.text = "";
                _state = GameState.Glowing;
                _countSeconds = 0.0f;
                _remaining[0].GlowCorrect(ColoredForSeconds);
            }
            else
            {
                if (_countSeconds >= 3.0)
                {
                    CounterText.text = "GO!";
                }
                else
                {
                    CounterText.text = (3 - (int)Mathf.Floor(_countSeconds)).ToString();
                }
                _countSeconds += Time.deltaTime;
            }
        }

        // Use this for initialization
        void Start ()
        {
            ButtonUp.Id = 0;
            ButtonUp.Controller = this;
            ButtonUp.GlowNormal();
            _buttonsById.Add(0, ButtonUp);

            ButtonLeft.Id = 1;
            ButtonLeft.Controller = this;
            ButtonUp.GlowNormal();
            _buttonsById.Add(1, ButtonLeft);

            ButtonRight.Id = 2;
            ButtonRight.Controller = this;
            ButtonUp.GlowNormal();
            _buttonsById.Add(2, ButtonRight);

            CurrentRoundsText.text = "Current Rounds\n --";
        }
	
        // Update is called once per frame
        void Update () {
            if (_state == GameState.Counting)
            {
                Count();
            }
            else if (_state == GameState.Running && _currentOrder.Count == 0)
            {
                Debug.Log("Success");
                _state = GameState.FinSucc;
            }
            else if(_state == GameState.Glowing) 
            {
                if (_remaining.Count > 0)
                {
                    if (_remaining[0].Glowing)
                    {

                    }
                    else
                    {
                        
                        if (_bufferTime >= _maxBufferTime)
                        {
                            _bufferTime = 0.0f;
                            _remaining.RemoveAt(0);
                            if (_remaining.Count > 0)
                            {
                                _remaining[0].GlowCorrect(ColoredForSeconds);
                            }
                            else
                            {
                                _state = GameState.Running;
                                Debug.Log("Starting game");
                            }
                        }
                        else
                        {
                            _bufferTime += Time.deltaTime;
                        }
                    }
                }
            }
            else if (_state == GameState.FinFail)
            {
                _rounds = 0;
                FlashFailing();
            } else if (_state == GameState.FinSucc)
            {
                _totalFlashTime = 0.0f;
                FlashSuccess();
            }
        }
    }
}
