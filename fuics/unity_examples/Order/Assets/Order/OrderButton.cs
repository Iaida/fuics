﻿using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;

namespace Assets.Order
{
    public class OrderButton : MonoBehaviour
    {
        public int Id { set; get; }

        public OrderController Controller;

        private float _glownFor = 0.0f;

        private float _glowEnd = 0.0f;

        private bool _glowing;

        public bool Glowing
        {
            get { return _glowing; }
        }

        /// <summary>
        /// Button that belongs to this script. 
        /// </summary>
        private Button myButton;

        private Image img;

        private void Glow(Color color, float time)
        {
            img.color = color;
            _glowing = true;
            _glownFor = 0.0f;
            _glowEnd = time;
        }

        public void GlowCorrect(float time)
        {
            Debug.Log("Button id " + Id + "glowing correct.");
           Glow(new Color(0.2f,0.6f, 0.1f), time);
        }

        public void GlowFail(float time)
        {
            Debug.Log("Button id " + Id + "glowing fail.");
            Glow(Color.red, time);
        }

        public void GlowNormal()
        {
            Debug.Log("Button id " + Id + "glowing normal.");
            img.color = Color.blue;
        }

        // Use this for initialization
        void Start ()
        {
            myButton = GetComponent<Button>();
            img = GetComponent<Image>();
            myButton.onClick.AddListener(ButtonClicked);
        }
	
        // Update is called once per frame
        void Update () {
            if (_glowing)
            {  
                if (_glownFor > _glowEnd)
                {
                    GlowNormal();
                    _glowing = false;
                }
                else
                {
                    _glownFor += Time.deltaTime;
                }
            }
        }

        public void ButtonClicked()
        {
            Debug.Log("Button id =" + Id + " clicked");
            Controller.Pressed(this);
        }
    }
}
