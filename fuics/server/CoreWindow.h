#pragma once
#include "Core.h"
#include "rendering/Renderer.h"

/// class that extends the functionality of the core base class with additional rendering to a debug window
class CoreWindow : public Core
{
public:
    /// default constructor, which calls Core::Core() and initializes additional members
    CoreWindow();

private:
    /// instance of renderer class to provide additional functionality for the CoreWindow class
    Renderer renderer;

    /// overrides Core::update(), but calls the base class update and extends it
    /// with processing of window events such as keyboard input and rendering to a debug window
    void update() override;

    /// processes the event queue, and sends keyboard input to handleKeyboardInput()
    void processWindowEvents();

    /// implements the mapping of pressed keys and resulting actions
    /// \param keySym 29-bit integer value which identifies characters or functions associated with each key of a keyboard layout
    void handleKeyboardInput(const KeySym keySym);

    /// renders fps, calibration, feet and buttons to the debug window
    void renderToWindow();
};
