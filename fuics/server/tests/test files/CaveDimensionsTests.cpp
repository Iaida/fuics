#include <gtest/gtest.h>
#include "../physics/CaveDimensions.h"

TEST(CaveDimensionsTests, normalizedToCaveAxis)
{
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveX(0.0f), CaveDimensions::minXCoordinate);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveY(0.0f), CaveDimensions::minYCoordinate);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveZ(0.0f), CaveDimensions::minZCoordinate);

    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveX(0.5f), CaveDimensions::midXCoordinate);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveY(0.5f), CaveDimensions::midYCoordinate);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveZ(0.5f), CaveDimensions::midZCoordinate);

    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveX(1.0f), CaveDimensions::maxXCoordinate);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveY(1.0f), CaveDimensions::maxYCoordinate);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveZ(1.0f), CaveDimensions::maxZCoordinate);
}

TEST(CaveDimensionsTests, normalizedToCaveEdgeLength)
{
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveEdgeLength(0.0f), 0.0f);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveEdgeLength(0.5f), CaveDimensions::edgeLength / 2.0f);
    ASSERT_FLOAT_EQ(CaveDimensions::normalizedToCaveEdgeLength(1.0f), CaveDimensions::edgeLength);
}

TEST(CaveDimensionsTests, caveToScreenAxis)
{
    ASSERT_EQ(CaveDimensions::caveToScreenX(CaveDimensions::minXCoordinate, 1920), 0);
    ASSERT_EQ(CaveDimensions::caveToScreenX(CaveDimensions::midXCoordinate, 1920), 1920/2);
    ASSERT_EQ(CaveDimensions::caveToScreenX(CaveDimensions::maxXCoordinate, 1920), 1920);

    // flipped
    ASSERT_EQ(CaveDimensions::caveToScreenY(CaveDimensions::minYCoordinate, 1080), 1080);
    ASSERT_EQ(CaveDimensions::caveToScreenY(CaveDimensions::midYCoordinate, 1080), 1080/2);
    ASSERT_EQ(CaveDimensions::caveToScreenY(CaveDimensions::maxYCoordinate, 1080), 0);
}

