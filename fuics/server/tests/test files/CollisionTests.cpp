
#include <gtest/gtest.h>
#include "../physics/Collision.h"
#include "../../physics/Button.h"
#include "../../physics/Foot.h"


TEST(CollisionTests, CornerPokesRectangleFace)
{
    // base class Rectangle constructor protected, using Foot constructor instead
    Foot a;
    a.setFromVertices(glm::vec3(0.0f, 0.0f, 0.0f),
                      glm::vec3(1.0f, 0.0f, 0.0f),
                      glm::vec3(0.0f, 1.0f, 0.0f));
    //std::cout << a.verticesString() << "\n\n";

    Foot b;
    b.setFromVertices(glm::vec3(0.5f, 0.5f, -0.1f),
                      glm::vec3(0.5f, 0.0f, 1.0f),
                      glm::vec3(0.0f, 0.5f, 1.0f));
    //std::cout << b.verticesString() << "\n\n";

    Collision c(a, b);
    bool result = c.collision;
    ASSERT_EQ(result, true);
}

TEST(CollisionTests, OneInsideOther)
{
    // base class Rectangle constructor protected, using Foot constructor instead
    Foot a;
    a.setFromVertices(glm::vec3(0.0f, 0.0f, 0.0f),
                      glm::vec3(1.0f, 0.0f, 0.0f),
                      glm::vec3(0.0f, 1.0f, 0.0f));
    //std::cout << a.verticesString() << "\n\n";

    Foot b;
    b.setFromVertices(glm::vec3(0.1f, 0.1f, 0.0f),
                      glm::vec3(0.9f, 0.0f, 0.0f),
                      glm::vec3(0.0f, 0.9f, 0.0f));
    //std::cout << b.verticesString() << "\n\n";

    Collision c(a, b);
    bool result = c.collision;
    ASSERT_EQ(result, true);
}

TEST(CollisionTests, PointCollision)
{
    // base class Rectangle constructor protected, using Foot constructor instead
    Foot a;
    a.setFromVertices(glm::vec3(0.0f, 0.0f, 0.0f),
                      glm::vec3(1.0f, 0.0f, 0.0f),
                      glm::vec3(0.0f, 1.0f, 0.0f));
    //std::cout << a.verticesString() << "\n\n";

    Foot b;
    b.setFromVertices(glm::vec3(0.0f, 0.0f, 0.0f),
                      glm::vec3(-1.0f, 0.0f, 0.0f),
                      glm::vec3(0.0f, -1.0f, 0.0f));
    //std::cout << b.verticesString() << "\n\n";

    Collision c(a, b);
    bool result = c.collision;
    ASSERT_EQ(result, true);
}

TEST(CollisionTests, SamePlaneNoCollision)
{
    // base class Rectangle constructor protected, using Foot constructor instead
    Foot a(0.1f, 0.1f, 1.0f, 1.0f);
    //std::cout << a.verticesString() << "\n\n";

    Foot b(-0.1f, -0.1f, -1.0f, -1.0f);
    //std::cout << b.verticesString() << "\n\n";

    Collision c(a, b);
    bool result = c.collision;
    ASSERT_EQ(result, false);
}

TEST(CollisionTests, EdgeCollision)
{
    // base class Rectangle constructor protected, using Foot constructor instead
    Foot a(0.0f, 0.0f, 1.0f, 1.0f);
    //std::cout << a.verticesString() << "\n\n";

    Foot b(0.0f, 0.0f, -1.0f, 1.0f);
    //std::cout << b.verticesString() << "\n\n";

    Collision c(a, b);
    bool result = c.collision;
    ASSERT_EQ(result, true);
}