#include <gtest/gtest.h>
#include "../util/AlgorithmUtils.h"

TEST(AlgorithmUtilsTests, removeElementsIfNoChange)
{
    std::string s = "Hello World";
    AlgorithmUtils::removeElementsIf(
            s,
            [](const char c) { return c == 'Z'; } );
    ASSERT_EQ(s, "Hello World");
}

TEST(AlgorithmUtilsTests, removeElementsIfWithChange)
{
    std::string s = "Hello World";
    AlgorithmUtils::removeElementsIf(
            s,
            [](const char c) { return c == ' '; } );
    ASSERT_EQ(s, "HelloWorld");
}

TEST(AlgorithmUtilsTests, removeElementsIfToFoundTrue)
{
    std::string s = "Hello World";
    bool resultFound = AlgorithmUtils::removeElementsIfToFound(
            s,
            [](const char c) { return c == ' '; } );
    ASSERT_EQ(s, "HelloWorld");
    ASSERT_EQ(resultFound, true);
}

TEST(AlgorithmUtilsTests, removeElementsIfToFoundFalse)
{
    std::string s = "Hello World";
    bool resultFound = AlgorithmUtils::removeElementsIfToFound(
            s,
            [](const char c) { return c == 'Z'; } );
    ASSERT_EQ(s, "Hello World");
    ASSERT_EQ(resultFound, false);
}

TEST(AlgorithmUtilsTests, removeElementsIfToFoundLastElement)
{
    std::string s = "Hello World";
    bool resultFound = AlgorithmUtils::removeElementsIfToFound(
            s,
            [](const char c) { return c == 'd'; } );
    ASSERT_EQ(s, "Hello Worl");
    ASSERT_EQ(resultFound, true);
}
