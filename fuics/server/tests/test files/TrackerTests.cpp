
#include <gtest/gtest.h>
#include <glm/gtx/quaternion.hpp>
#include "../tracking/Tracker.h"


TEST(TrackerTests, TrackerDifference)
{
    Tracker a;
    a.id = 1;
    a.rotMatrix = glm::toMat3(glm::quat(glm::radians(glm::vec3(0.0f, 0.0f, 90.0f))));
    Tracker b = a;

    Tracker result = a.difference(b);
    Tracker expected;
    expected.id = 1;
    expected.rotMatrix = glm::mat3x3(1.0f, 0.0f, 0.0f,
                                     0.0f, 1.0f, 0.0f,
                                     0.0f, 0.0f, 1.0f);
    ASSERT_EQ(result, expected);
}


TEST(TrackerTests, calcTransformationMatrixFromOrientationVector)
{
    Tracker input;
    input.position = glm::vec3(1.0f, 2.0f, 3.0f);
    input.orientation = glm::vec3(0.0f, 0.0f, 0.0f);

    glm::mat4x4 result = input.calcTransformationMatrixFromOrientationVector();

    glm::mat4x4 expected = glm::mat4x4(glm::mat3x3(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
    expected[3] = glm::vec4(1.0f, 2.0f, 3.0f, 1.0f);

    ASSERT_EQ(result, expected);
}