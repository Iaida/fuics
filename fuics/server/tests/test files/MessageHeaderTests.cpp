
#include <gtest/gtest.h>
#include "../network/MessageHeader.h"

TEST(MessageHeaderTests, MessageHeader0)
{
    int32_t input = 0;
    MessageHeader inputMessageHeader(input);
    MessageHeader resultMessageHeader(inputMessageHeader.toBytes());
    int32_t result = resultMessageHeader.messageLength;
    ASSERT_EQ(result, input);
}

TEST(MessageHeaderTests, MessageHeader128)
{
    int32_t input = 128;
    MessageHeader inputMessageHeader(input);
    MessageHeader resultMessageHeader(inputMessageHeader.toBytes());
    int32_t result = resultMessageHeader.messageLength;
    ASSERT_EQ(result, input);
}

TEST(MessageHeaderTests, MessageHeader256)
{
    int32_t input = 256;
    MessageHeader inputMessageHeader(input);
    MessageHeader resultMessageHeader(inputMessageHeader.toBytes());
    int32_t result = resultMessageHeader.messageLength;
    ASSERT_EQ(result, input);
}

TEST(MessageHeaderTests, MessageHeaderMax)
{
    int32_t input = std::numeric_limits<int32_t>::max();
    MessageHeader inputMessageHeader(input);
    MessageHeader resultMessageHeader(inputMessageHeader.toBytes());
    int32_t result = resultMessageHeader.messageLength;
    ASSERT_EQ(result, input);
}
