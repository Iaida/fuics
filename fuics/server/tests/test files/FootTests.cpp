
#include <gtest/gtest.h>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/epsilon.hpp>
#include "../physics/Foot.h"


TEST(FootTests, FootMatrixMultiplicationIdentity)
{
    Foot input(0.0f, 0.0f, 1.0f, 1.0f);
    Foot result = input * glm::mat4x4(1.0f, 0.0f, 0.0f, 0.0f,
                                      0.0f, 1.0f, 0.0f, 0.0f,
                                      0.0f, 0.0f, 1.0f, 0.0f,
                                      0.0f, 0.0f, 0.0f, 1.0f);
    ASSERT_EQ(result, input);
}

TEST(FootTests, FootMatrixMultiplicationTranslation)
{
    Foot input(0.0f, 0.0f, 1.0f, 1.0f);
    Foot result = input * glm::mat4x4(1.0f, 0.0f, 0.0f, 0.0f,
                                      0.0f, 1.0f, 0.0f, 0.0f,
                                      0.0f, 0.0f, 1.0f, 0.0f,
                                      1.0f, 2.0f, 3.0f, 1.0f);
    Foot expected(glm::vec3(1.0f, 2.0f, 3.0f),
                  glm::vec3(1.0f, 0.0f, 0.0f),
                  glm::vec3(0.0f, 1.0f, 0.0f));
    ASSERT_EQ(result, expected);
}

TEST(FootTests, FootMatrixMultiplicationRotationX)
{
    Foot input(0.0f, 0.0f, 1.0f, 1.0f);
    // rotation works counterclockwise viewed from positive side
    glm::mat4x4 rotationMatrix = glm::toMat4(glm::quat(glm::radians(glm::vec3(90.0f, 0.0f, 0.0f))));
    Foot result = input * rotationMatrix;
    Foot expected(glm::vec3(0.0f, 0.5f, -0.5f),
                  glm::vec3(1.0f, 0.0f, 0.0f),
                  glm::vec3(0.0f, 0.0f, 1.0f));

    glm::bvec3 comparisonP = glm::epsilonEqual(result.p(), expected.p(), FLT_EPSILON);
    ASSERT_EQ(comparisonP, glm::bvec3(true));
    glm::bvec3 comparisonS = glm::epsilonEqual(result.s(), expected.s(), FLT_EPSILON);
    ASSERT_EQ(comparisonS, glm::bvec3(true));
    glm::bvec3 comparisonT = glm::epsilonEqual(result.t(), expected.t(), FLT_EPSILON);
    ASSERT_EQ(comparisonT, glm::bvec3(true));
}


TEST(FootTests, FootMatrixMultiplicationRotationY)
{
    Foot input(0.0f, 0.0f, 1.0f, 1.0f);
    // rotation works counterclockwise viewed from positive side
    glm::mat4x4 rotationMatrix = glm::toMat4(glm::quat(glm::radians(glm::vec3(0.0f, 90.0f, 0.0f))));
    Foot result = input * rotationMatrix;
    Foot expected(glm::vec3(0.5f, 0.0f, 0.5f),
                  glm::vec3(0.0f, 0.0f, -1.0f),
                  glm::vec3(0.0f, 1.0f, 0.0f));

    glm::bvec3 comparisonP = glm::epsilonEqual(result.p(), expected.p(), FLT_EPSILON);
    ASSERT_EQ(comparisonP, glm::bvec3(true));
    glm::bvec3 comparisonS = glm::epsilonEqual(result.s(), expected.s(), FLT_EPSILON);
    ASSERT_EQ(comparisonS, glm::bvec3(true));
    glm::bvec3 comparisonT = glm::epsilonEqual(result.t(), expected.t(), FLT_EPSILON);
    ASSERT_EQ(comparisonT, glm::bvec3(true));
}

TEST(FootTests, FootMatrixMultiplicationRotationZ)
{
    Foot input(0.0f, 0.0f, 1.0f, 1.0f);
    // rotation works counterclockwise viewed from positive side
    glm::mat4x4 rotationMatrix = glm::toMat4(glm::quat(glm::radians(glm::vec3(0.0f, 0.0f, 90.0f))));
    Foot result = input * rotationMatrix;
    // TODO
    Foot expected(glm::vec3(1.0f, 0.0f, 0.0f),
                  glm::vec3(0.0f, 1.0f, 0.0f),
                  glm::vec3(-1.0f, 0.0f, 0.0f));

    glm::bvec3 comparisonP = glm::epsilonEqual(result.p(), expected.p(), FLT_EPSILON);
    ASSERT_EQ(comparisonP, glm::bvec3(true));
    glm::bvec3 comparisonS = glm::epsilonEqual(result.s(), expected.s(), FLT_EPSILON);
    ASSERT_EQ(comparisonS, glm::bvec3(true));
    glm::bvec3 comparisonT = glm::epsilonEqual(result.t(), expected.t(), FLT_EPSILON);
    ASSERT_EQ(comparisonT, glm::bvec3(true));
}