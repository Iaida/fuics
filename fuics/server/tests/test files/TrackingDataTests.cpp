
#include <gtest/gtest.h>
#include "../tracking/TrackingData.h"


TEST(TrackingDataTests, TrackingDataHistoryConstructorSimple)
{
    std::deque<TrackingData> input =
            {
                    TrackingData(),
                    TrackingData()
            };

    TrackingData result = TrackingData(input);
    ASSERT_EQ(result, TrackingData());
}

TEST(TrackingDataTests, TrackingDataHistoryConstructorDifferentIDs)
{
    Tracker tracker1;
    tracker1.id = 1;
    Tracker tracker2;
    tracker2.id = 2;
    std::deque<TrackingData> input =
            {
                    TrackingData(1, 0.0f, std::vector<Tracker>()),
                    TrackingData(3, 1.0f, std::vector<Tracker>{tracker1}),
                    TrackingData(1, 0.0f, std::vector<Tracker>{tracker2, tracker1}),
                    TrackingData(3, 1.0f, std::vector<Tracker>{Tracker(), tracker2})
            };


    TrackingData result = TrackingData(input);
    TrackingData expected = TrackingData(2, 0.5f, std::vector<Tracker>{Tracker(), tracker1, tracker2});
    ASSERT_EQ(result, expected);
}

TEST(TrackingDataTests, TrackingDataHistoryConstructorSameIDAverage)
{
    Tracker tracker1Old;
    tracker1Old.id = 1;
    tracker1Old.position = glm::vec3(0.0f);
    tracker1Old.orientation = glm::vec3(0.0f);
    tracker1Old.rotMatrix = glm::mat3x3(0.0f);

    Tracker tracker1New;
    tracker1New.id = 1;
    tracker1New.position = glm::vec3(1.0f);
    tracker1New.orientation = glm::vec3(1.0f);
    tracker1New.rotMatrix = glm::mat3x3(1.0f);

    std::deque<TrackingData> input =
            {
                    TrackingData(1, 0.0f, std::vector<Tracker>{tracker1Old}),
                    TrackingData(1, 0.0f, std::vector<Tracker>{tracker1New})
            };

    TrackingData result = TrackingData(input);

    Tracker tracker1Averaged;
    tracker1Averaged.id = 1;
    tracker1Averaged.position = glm::vec3(0.5f);
    tracker1Averaged.orientation = glm::vec3(0.5f);
    tracker1Averaged.rotMatrix = glm::mat3x3(0.5f);

    TrackingData expected = TrackingData(1, 0.0f, std::vector<Tracker>{tracker1Averaged});
    ASSERT_EQ(result, expected);
}