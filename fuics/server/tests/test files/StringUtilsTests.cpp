
#include <gtest/gtest.h>
#include "../util/StringUtils.h"


TEST(StringUtilsTests, SplitStringWithString)
{
    std::string input = "Hello<<World";
    std::string token = "<<";
    std::vector<std::string> result = StringUtils::split(input, token);
    ASSERT_EQ(result.size(), 2);
    ASSERT_EQ(result[0], "Hello");
    ASSERT_EQ(result[1], "World");
}


TEST(StringUtilsTests, SplitStringWithChar)
{
    std::string input = "Hello World";
    char token = ' ';
    std::vector<std::string> result = StringUtils::split(input, token);
    ASSERT_EQ(result.size(), 2);
    ASSERT_EQ(result[0], "Hello");
    ASSERT_EQ(result[1], "World");
}

TEST(StringUtilsTests, RemoveAllWithChar)
{
    std::string input = "Hello World";
    std::string result = StringUtils::removeAll(input, ' ');
    ASSERT_EQ(result, "HelloWorld");
}

TEST(StringUtilsTests, RemoveAllWithString)
{
    std::string input = "Hello World";
    std::string result = StringUtils::removeAll(input, "o W");
    ASSERT_EQ(result, "Hellorld");
}

TEST(StringUtilsTests, ReplaceAll)
{
    std::string input = "Hello World";
    std::string result = StringUtils::replaceAll(input, "o W", "O w");
    ASSERT_EQ(result, "HellO world");
}