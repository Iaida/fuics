
#include <gtest/gtest.h>
#include "../tracking/TrackingStringParsing.h"


TEST(TrackingStringParsingTests, ParseTrackingManualSample)
{
	// std::stof() is dependent on the locale of the computer
	// without calling at least once, parsing strings as float results in truncated values after decimal-point
	// https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/
	// C uses "." as decimal-point separator
	std::setlocale(LC_ALL, "C");

	std::string input = "fr 21753";
	input += "\r\n";
	input += "ts 39596.024831";
	input += "\r\n";
	input += "6d 1 [0 1.000][326.848 -187.216 109.503 -160.4704 -3.6963 -7.0913][-0.940508 -0.339238 -0.019025 0.333599 -0.932599 0.137735 -0.064467 0.123194 0.990286]";
	input += "\r\n";
	TrackingData result = TrackingStringParsing::parse(input);
	TrackingData expected;
	expected.frame = 21753;
	expected.timestamp = 39596.024831f;
	Tracker tracker;
	tracker.id = 0;
	tracker.quality = 1.000f;
	tracker.position[0] = 326.848f;
	tracker.position[1] = -187.216f;
	tracker.position[2] = 109.503f;
	tracker.orientation[0] = -160.4704f;
	tracker.orientation[1] = -3.6963f;
	tracker.orientation[2] = -7.0913f;
	tracker.rotMatrix[0][0] = -0.940508f;
	tracker.rotMatrix[0][1] = -0.339238f;
	tracker.rotMatrix[0][2] = -0.019025f;
	tracker.rotMatrix[1][0] = 0.333599f;
	tracker.rotMatrix[1][1] = -0.932599f;
	tracker.rotMatrix[1][2] = 0.137735f;
	tracker.rotMatrix[2][0] = -0.064467f;
	tracker.rotMatrix[2][1] = 0.123194f;
	tracker.rotMatrix[2][2] = 0.990286f;
	expected.trackers.push_back(tracker);
	ASSERT_EQ(result, expected);
}

TEST(TrackingStringParsingTests, ParseMultipleTrackers)
{
	// std::stof() is dependent on the locale of the computer
	// without calling at least once, parsing strings as float results in truncated values after decimal-point
	// https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/
	// C uses "." as decimal-point separator
	std::setlocale(LC_ALL, "C");

	std::string input = "fr 21753";
	input += "\r\n";
	input += "ts 39596.024831";
	input += "\r\n";
	input += "6d 2 [0 1.000][326.848 -187.216 109.503 -160.4704 -3.6963 -7.0913][-0.940508 -0.339238 -0.019025 0.333599 -0.932599 0.137735 -0.064467 0.123194 0.990286]";
	input += " [1 1.000][326.848 -187.216 109.503 -160.4704 -3.6963 -7.0913][-0.940508 -0.339238 -0.019025 0.333599 -0.932599 0.137735 -0.064467 0.123194 0.990286]";
	input += "\r\n";
	TrackingData result = TrackingStringParsing::parse(input);
	TrackingData expected;
	expected.frame = 21753;
	expected.timestamp = 39596.024831f;
    Tracker tracker;
	tracker.id = 0;
	tracker.quality = 1.000f;
	tracker.position[0] = 326.848f;
	tracker.position[1] = -187.216f;
	tracker.position[2] = 109.503f;
	tracker.orientation[0] = -160.4704f;
	tracker.orientation[1] = -3.6963f;
	tracker.orientation[2] = -7.0913f;
	tracker.rotMatrix[0][0] = -0.940508f;
	tracker.rotMatrix[0][1] = -0.339238f;
	tracker.rotMatrix[0][2] = -0.019025f;
	tracker.rotMatrix[1][0] = 0.333599f;
	tracker.rotMatrix[1][1] = -0.932599f;
	tracker.rotMatrix[1][2] = 0.137735f;
	tracker.rotMatrix[2][0] = -0.064467f;
	tracker.rotMatrix[2][1] = 0.123194f;
	tracker.rotMatrix[2][2] = 0.990286f;
	expected.trackers.push_back(tracker);
	tracker.id = 1;
	expected.trackers.push_back(tracker);
	ASSERT_EQ(result, expected);
}

TEST(TrackingStringParsingTests, parseTrackerLine)
{
	// std::stof() is dependent on the locale of the computer
	// without calling at least once, parsing strings as float results in truncated values after decimal-point
	// https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/
	// C uses "." as decimal-point separator
	std::setlocale(LC_ALL, "C");

	std::string input = "6d 2 [0 1.000][326.848 -187.216 109.503 -160.4704 -3.6963 -7.0913][-0.940508 -0.339238 -0.019025 0.333599 -0.932599 0.137735 -0.064467 0.123194 0.990286]";
	input += " [1 1.000][326.848 -187.216 109.503 -160.4704 -3.6963 -7.0913][-0.940508 -0.339238 -0.019025 0.333599 -0.932599 0.137735 -0.064467 0.123194 0.990286]";

	std::vector<Tracker> result = TrackingStringParsing::parseTrackerLine(input, 2);
	std::vector<Tracker> expected;

	Tracker tracker;
	tracker.id = 0;
	tracker.quality = 1.000f;
	tracker.position[0] = 326.848f;
	tracker.position[1] = -187.216f;
	tracker.position[2] = 109.503f;
	tracker.orientation[0] = -160.4704f;
	tracker.orientation[1] = -3.6963f;
	tracker.orientation[2] = -7.0913f;
	tracker.rotMatrix[0][0] = -0.940508f;
	tracker.rotMatrix[0][1] = -0.339238f;
	tracker.rotMatrix[0][2] = -0.019025f;
	tracker.rotMatrix[1][0] = 0.333599f;
	tracker.rotMatrix[1][1] = -0.932599f;
	tracker.rotMatrix[1][2] = 0.137735f;
	tracker.rotMatrix[2][0] = -0.064467f;
	tracker.rotMatrix[2][1] = 0.123194f;
	tracker.rotMatrix[2][2] = 0.990286f;
	expected.push_back(tracker);
	tracker.id = 1;
	expected.push_back(tracker);
	ASSERT_EQ(result, expected);
}

TEST(TrackingStringParsingTests, trackerStringToTracker)
{
	// std::stof() is dependent on the locale of the computer
	// without calling at least once, parsing strings as float results in truncated values after decimal-point
	// https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/
	// C uses "." as decimal-point separator
	std::setlocale(LC_ALL, "C");

	std::string input = "0 1.000 326.848 -187.216 109.503 -160.4704 -3.6963 -7.0913 -0.940508 -0.339238 -0.019025 0.333599 -0.932599 0.137735 -0.064467 0.123194 0.990286";
	Tracker result = TrackingStringParsing::trackerStringToTracker(input);
	Tracker expected;
	
	expected.id = 0;
	expected.quality = 1.000f;
	expected.position[0] = 326.848f;
	expected.position[1] = -187.216f;
	expected.position[2] = 109.503f;
	expected.orientation[0] = -160.4704f;
	expected.orientation[1] = -3.6963f;
	expected.orientation[2] = -7.0913f;
	expected.rotMatrix[0][0] = -0.940508f;
	expected.rotMatrix[0][1] = -0.339238f;
	expected.rotMatrix[0][2] = -0.019025f;
	expected.rotMatrix[1][0] = 0.333599f;
	expected.rotMatrix[1][1] = -0.932599f;
	expected.rotMatrix[1][2] = 0.137735f;
	expected.rotMatrix[2][0] = -0.064467f;
	expected.rotMatrix[2][1] = 0.123194f;
	expected.rotMatrix[2][2] = 0.990286f;

	ASSERT_EQ(result, expected);
}

TEST(TrackingStringParsingTests, ParseWithoutAnyTrackers)
{
	// std::stof() is dependent on the locale of the computer
	// without calling at least once, parsing strings as float results in truncated values after decimal-point
	// https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/
	// C uses "." as decimal-point separator
	std::setlocale(LC_ALL, "C");

	std::string input = "fr 21753";
	input += "\r\n";
	input += "ts 39596.024831";
	input += "\r\n";
	input += "6d 0";
	input += "\r\n";
	TrackingData result = TrackingStringParsing::parse(input);
	TrackingData expected;
	expected.frame = 21753;
	expected.timestamp = 39596.024831f;
	ASSERT_EQ(result, expected);
}

