
#include "ConfigurationMessageWrapper.h"
#include "DimensionsWrapper.h"
#include "ListenerWrapper.h"
#include "UIElementWrapper.h"
#include "UIStatusWrapper.h"

ConfigurationMessageWrapper::ConfigurationMessageWrapper() {

}

std::string ConfigurationMessageWrapper::toString() const
{
    return ConfigurationRequestType_Name(wrapped.requesttype()) + "\n" +
           DimensionsWrapper(wrapped.dimensions()).toString() + "\n" +
           ConfigurationError_Name(wrapped.error()) + "\n" +
           UIElementWrapper(wrapped.elementinfo()).toString() + "\n" +
           ListenerWrapper(wrapped.listenerinfo()).toString() + "\n" +
           "status: \n " + UIStatusWrapper(wrapped.status()).toString() + "\n";
}

std::string ConfigurationMessageWrapper::requestType() const
{
    return ConfigurationRequestType_Name(wrapped.requesttype());
}
