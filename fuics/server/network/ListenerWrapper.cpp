#include "ListenerWrapper.h"

EventMessageWrapper ListenerWrapper::invoke(const int32_t elementID) const
{
    return EventMessageWrapper(elementID, wrapped.listensto(), wrapped.info(), wrapped.id());
}

ListenerWrapper::ListenerWrapper(const Listener &listener)  : Wrapper(listener)
{
}

bool ListenerWrapper::sameEventType(const EventType eventType) const
{
    return eventType == wrapped.listensto();
}

std::string ListenerWrapper::toString() const
{
    return "id: " + std::to_string(wrapped.id()) +
           " info: " + wrapped.info() +
           " listensTo: " + EventType_Name(wrapped.listensto());
}
