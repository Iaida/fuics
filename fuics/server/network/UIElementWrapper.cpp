

#include "UIElementWrapper.h"
#include "ListenerWrapper.h"
#include "DimensionsWrapper.h"

UIElementWrapper::UIElementWrapper() :
        oldCollision(false), eventOccurredTypePair(false, EventType())
{
}

UIElementWrapper::UIElementWrapper(const UIElement &uiElement) :
        Wrapper(uiElement), oldCollision(false)
{
    // wrap UIElementWrappers raw dimensions
    DimensionsWrapper dimensionsWrapper(wrapped.dimensions());
    // construct a button with wrapped dimensions
    button = Button(dimensionsWrapper);
}

std::vector<EventMessageWrapper> UIElementWrapper::invokeListeners(const EventType eventType) const
{
    std::vector<EventMessageWrapper> result;
    for (const Listener & l : wrapped.listeners())
    {
        ListenerWrapper lw(l);
        if (lw.sameEventType(eventType))
            result.push_back(lw.invoke(wrapped.id()));
    }
    return result;
}


void UIElementWrapper::addListener(ListenerWrapper &listenerWrapper)
{
    wrapped.add_listeners()->CopyFrom(listenerWrapper);
}

std::string UIElementWrapper::toString() const
{
    std::string result = "id: " + std::to_string(wrapped.id()) + " " +
                         ElementType_Name(wrapped.type()) + " " +
                         DimensionsWrapper(wrapped.dimensions()).toString() + " \n" +
                         "Listeners: \n";

    for (const Listener & l : wrapped.listeners())
        result += ListenerWrapper(l).toString() + "\n";

    return result;
}
