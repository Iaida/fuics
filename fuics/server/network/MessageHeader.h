#pragma once

#include <stdint-gcc.h>
#include <vector>

/// class used to manage a message's header containing the message length
class MessageHeader
{
public:
    /// constructor setting messageLength member
    /// \param messageLength
    MessageHeader(int32_t messageLength);
    /// constructor setting messageLength member by converting multiple bytes
    /// \param headerBytes vector of characters/bytes to be converted to messageLength
    MessageHeader(const std::vector<char> headerBytes);
    /// converts messageLength member to a vector of characters/bytes
    /// \return messageLength represented as a vector of characters/bytes
    std::vector<char> toBytes() const;

    /// number of bytes used to represent messageLength in a vector
    static const unsigned char HEADER_BYTE_LENGTH;
    /// the message length following the header
    int32_t messageLength;
};
