#pragma once

/// generic abstract wrapper base class allowing access of the wrapped objects functionality via the 'arrow'-operator
/// \tparam T type of object to be wrapped
template <typename T>
class Wrapper
{
protected:
    /// default constructor
    Wrapper() {}
    /// constructor initializing wrapped member
    Wrapper(const T & wrapped) : wrapped(wrapped) {}
    /// wrapped object of type T
    T wrapped;
public:
    /// overloads 'arrow'-operator to gain access to wrapped members functions
    /// is a non-const function which gains access to const as well as non-const functions,
    /// therefore const-qualified objects of classes derived from Wrapper cannot use this operator
    /// \return returns pointer to wrapped member
    T* operator->()
    {
        return &wrapped;
    }

    /// implicit conversion to wrapped object
    /// \return copy of wrapped member of type T
    operator T() const
    {
        return wrapped;
    }
};
