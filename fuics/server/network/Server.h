#pragma once

#include "Session.h"
#include "../logger/Logger.h"
#include "../util/StringUtils.h"
#include "MessageHeader.h"
#include "../util/AlgorithmUtils.h"

#include <vector>
#include <utility> // std::pair
#include <iostream>
#include <queue>

#include <boost/bind.hpp>
#include <boost/asio.hpp>

#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/coded_stream.h>

/// class responsible for sending and receiving messages of type M
/// \tparam M google protobuf message type
template <typename M>
class Server
{
public:
    /// constructor initializing the members
    /// \param port endpoint member initialized with the given port
    Server(unsigned short port);
    /// polls the io_service member and starts reading from socket
    void poll();
    /// checks if messages are present in messageQueue
    /// \return true if messages are present in messageQueue, else false
    bool hasMessages();
    /// returns the next message from messageQueue and removes it
    /// \return copy of the first message in messageQueue
    M nextMessage();
    /// sends a message prefixed with a header to all opened sockets in sessions,
    /// calls async_write with handleWrite as handler
    /// \param message of type M to be sent
    void invokeWrite(M & message);

private:
    /// waits for a new connection to be added to sessions, then calls handleAccept()
    void startAccept();
    /// constructs a new session for a new connection
    /// \param socketPTR boost socket created by startAccept()
    /// \param error boost error code
    void handleAccept(std::shared_ptr<boost::asio::ip::tcp::socket> socketPTR,
                      const boost::system::error_code &error);
    /// calls async_read_some on all opened sockets that are not already reading
    /// with handleReadFromSocket() as handler
    void invokeReadFromSocket();
    /// handler called by invokeReadFromSocket(), reads as many messages as present in buffer
    /// and adds them to the message queue, also closes disconnected sessions
    /// \param error boost error code
    /// \param bytes_transferred the number of bytes that were transferred
    /// \param session contains socket and buffer
    void handleReadFromSocket(const boost::system::error_code& error,
                              const int bytes_transferred,
                              Session & session);
    /// handler called by invokeWrite(), only checks for errors
    /// \param error boost error code
    /// \param length message length
    void handleWrite(const boost::system::error_code& error, const std::size_t length);
    /// removes sessions which do not have opened sockets from sessions member
    void removeSessionsWithClosedSockets();

    /// buffer size needed for construction of Session objects
    static const size_t READ_BUFFER_SIZE;

    /// template type M as string
    std::string type;
    /// boost io service which is constantly polled
    boost::asio::io_service io_service;
    /// boost tcp endpoint on port given to the constructor
    boost::asio::ip::tcp::endpoint endpoint;
    /// boost acceptor needed to accept new connections
    boost::asio::ip::tcp::acceptor acceptor;
    /// vector of Session structs representing the client connections
    std::vector<Session> sessions;
    /// message queue containing protobuf messages of type M
    std::queue<M> messageQueue;
};


template <typename M>
const size_t Server<M>::READ_BUFFER_SIZE = 1024;

template <typename M>
inline void Server<M>::poll()
{
    io_service.reset(); // necessary ???
    io_service.poll();
    invokeReadFromSocket();
}

template <typename M>
inline Server<M>::Server(unsigned short port)
        : endpoint(boost::asio::ip::tcp::v4(), port), acceptor(io_service, endpoint), type(M()->GetTypeName())
{
    LOG4CXX_INFO(Logger::at("Server " + type),  "Initializing Server for Protocol " + type);
    LOG4CXX_DEBUG(Logger::at("Server " + type),  "Endpoint opened(IPv4) on port " + std::to_string(port));
    startAccept();
}

template <typename M>
inline bool Server<M>::hasMessages()
{
    return messageQueue.size() > 0;
}

template <typename M>
inline M Server<M>::nextMessage()
{
    M copy = messageQueue.front();
    messageQueue.pop();
    return copy;
}

template <typename M>
inline void Server<M>::invokeWrite(M &message)
{
    // create header bytes
    MessageHeader messageHeader(static_cast<int32_t>(message->ByteSizeLong()));
    std::vector<char> headerBytes = messageHeader.toBytes();

    // copy the header bytes to the buffer
    std::vector<char> buffer(headerBytes.size() + messageHeader.messageLength);
    std::transform(headerBytes.begin(),
                   headerBytes.end(),
                   buffer.begin(),
                   [](const char c) { return c; });

    // create message bytes, starts after header
    message->SerializeToArray(buffer.data() + MessageHeader::HEADER_BYTE_LENGTH, messageHeader.messageLength);

    removeSessionsWithClosedSockets();
    if (sessions.empty())
        LOG4CXX_DEBUG(Logger::at("Server " + type),  "no socket to write to");

    for(Session & s : sessions)
    {
        // TODO : Broken Pipe error, maybe flag like read -> no longer occurring ???
        boost::asio::async_write(
                *s.socketPTR,
                boost::asio::buffer(buffer, buffer.size()),
                boost::bind(
                        &Server::handleWrite,
                        this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred)
        );
    }
}

template <typename M>
inline void Server<M>::startAccept()
{
    LOG4CXX_DEBUG(Logger::at("Server " + type),  "Start accepting");
    std::shared_ptr<boost::asio::ip::tcp::socket> socketPTR = std::make_shared<boost::asio::ip::tcp::socket>(io_service);
    acceptor.async_accept(*socketPTR,
                          boost::bind(&Server::handleAccept, this, socketPTR, boost::asio::placeholders::error));
}

template <typename M>
inline void Server<M>::handleAccept(std::shared_ptr<boost::asio::ip::tcp::socket> socketPTR,
                          const boost::system::error_code &error)
{
    LOG4CXX_DEBUG(Logger::at("Server " + type),  "HandleAccept called");
    if (!error)
    {
        // construct session in-place with provided socket and char buffer of size READ_BUFFER_SIZE
        sessions.emplace_back(socketPTR, std::make_shared<std::vector<char>>(READ_BUFFER_SIZE));
    }
    else
    {
        LOG4CXX_ERROR(Logger::at("Server " + type), error);
    }
    startAccept();
}

template <typename M>
inline void Server<M>::invokeReadFromSocket()
{
    removeSessionsWithClosedSockets();

    for(Session & session : sessions)
    {
        // use sessions reading flag to check if an async_read_some is already running for the sessions socket
        if (!session.reading)
        {
            session.reading = true; // set false at end of handler

            // Invoke the async_read
            // The async read is called immediately, the boost::bind binds a handler
            // The handler will be called as soon as all available bytes (Bytes in the buffer) are read.
            // If there are 2048 Bytes in the buffer and 50 bytes in the socket, 50 bytes are read and the
            // handler is NOT called
            // To read all available bytes, async_read_some can be used.
            session.socketPTR->async_read_some(
                    boost::asio::buffer(*session.bufferPTR),
                    boost::bind(
                            &Server::handleReadFromSocket,
                            this,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred,
                            boost::ref(session))
            );
        }
    }
}

template <typename M>
inline void Server<M>::handleReadFromSocket(const boost::system::error_code &error,
                                  const int bytes_transferred,
                                  Session &session)
{
    LOG4CXX_TRACE(Logger::at("Server " + type),  "handleReadFromSocket enter");

    if (!error)
    {
        LOG4CXX_DEBUG(Logger::at("Server " + type),  "Bytes transferred: " + std::to_string(bytes_transferred));

        // TODO : use CodedInputStream ??? see: https://developers.google.com/protocol-buffers/docs/techniques#streaming

        int32_t bytesParsed = 0; // functions as offset
        // parse messages as long as bytes are left
        while (bytesParsed < bytes_transferred)
        {
            LOG4CXX_DEBUG(Logger::at("Server " + type),  "bytes already parsed: " + std::to_string(bytesParsed));

            // header are the first MessageHeader::HEADER_BYTE_LENGTH bytes
            std::vector<char> header(&session.bufferPTR->data()[0 + bytesParsed],
                                     &session.bufferPTR->data()[MessageHeader::HEADER_BYTE_LENGTH + bytesParsed]);

            MessageHeader messageHeader(header);

            if (bytes_transferred - bytesParsed - MessageHeader::HEADER_BYTE_LENGTH < messageHeader.messageLength)
                throw std::runtime_error(" not enough bytes transferred to parse message of length " + std::to_string(messageHeader.messageLength));

            // message starts after MessageHeader::HEADER_BYTE_LENGTH bytes and ends after an additional messageHeader.messageLength bytes
            std::vector<char> message(&session.bufferPTR->data()[MessageHeader::HEADER_BYTE_LENGTH + bytesParsed],
                                      &session.bufferPTR->data()[MessageHeader::HEADER_BYTE_LENGTH + messageHeader.messageLength + bytesParsed]);

            LOG4CXX_DEBUG(Logger::at("Server " + type), "Message as hex string: \n" +
                                                        StringUtils::byteArrayToHexString(message.data(), static_cast<size_t>(messageHeader.messageLength)));

            // parse message and check if successful
            M parsedMessage;
            if (parsedMessage->ParseFromArray(message.data(), messageHeader.messageLength))
            {
                // add parsed message to messageQueue
                messageQueue.push(parsedMessage);

                bytesParsed += MessageHeader::HEADER_BYTE_LENGTH + messageHeader.messageLength;
            }
            else
                throw std::runtime_error("ParseFromArray failed");
        }
    }
    else
    {
        if (error == boost::asio::error::eof || error == boost::asio::error::connection_reset)
        {
            LOG4CXX_DEBUG(Logger::at("Server " + type),  "disconnected");

            // TODO FH socket shutdown before close should be safer, but leads to error: "Transport endpoint is not connected"

            // check if still open, else do nothing and wait until closed sockets removed
            if (session.socketPTR->is_open())
            {
                /*boost::system::error_code shutdownError;
                socketPTR->shutdown(boost::asio::ip::tcp::socket::shutdown_both, shutdownError);
                if (!shutdownError)
                {
                    std::cout << "shutdown success \n";*/

                boost::system::error_code closeError;
                session.socketPTR->close();
                if (!closeError)
                {
                    LOG4CXX_DEBUG(Logger::at("Server " + type),  "socket closed");
                }
                else
                {
                    LOG4CXX_ERROR(Logger::at("Server " + type),  "Error on socket close: " + closeError.message());
                }

                /*}
                else
                    std::cout << "Error on socket shutdown: " + shutdownError.message() + "\n";*/
            }
        }
        else
        {
            LOG4CXX_ERROR(Logger::at("Server " + type),  "Error inside handleReadFromSocket: " + error.message());
        }
    }
    session.reading = false;
    LOG4CXX_TRACE(Logger::at("Server " + type),  "handleReadFromSocket exit");
}

template <typename M>
inline void Server<M>::handleWrite(const boost::system::error_code &error, const std::size_t length)
{
    if (!error)
    {
        LOG4CXX_TRACE(Logger::at("Server " + type),  "wrote to socket successfully");
    }
    else
    {
        LOG4CXX_ERROR(Logger::at("Server " + type),  "Error inside handleWrite of socket: " + error.message());
    }
}

template <typename M>
inline void Server<M>::removeSessionsWithClosedSockets()
{
    AlgorithmUtils::removeElementsIf(sessions,
                                     [](const Session & session)
                                     {
                                         return !(session.socketPTR->is_open());
                                     });
}
