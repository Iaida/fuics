#pragma once

#include <memory>
#include <vector>
#include <boost/asio/ip/tcp.hpp>

/// simple struct representing a connection
struct Session
{
    /// constructor setting members
    /// \param socketPTR shared pointer to a boost socket
    /// \param bufferPTR shared pointer to a vector of characters
    Session(std::shared_ptr<boost::asio::ip::tcp::socket> socketPTR, std::shared_ptr<std::vector<char>> bufferPTR) :
            socketPTR(socketPTR), bufferPTR(bufferPTR), reading(false) {};

    /// used to check if an async_read_some call is already running for a session
    bool reading;
    /// shared pointer to a boost socket
    std::shared_ptr<boost::asio::ip::tcp::socket> socketPTR;
    /// shared pointer to a buffer, represented as a vector of characters
    std::shared_ptr<std::vector<char>> bufferPTR;
};
