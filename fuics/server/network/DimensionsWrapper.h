#pragma once

#include "Wrapper.h"
#include "ConfigurationMessage.pb.h"

/// wrapper class for protobuf Dimensions
class DimensionsWrapper : public Wrapper<Dimensions>
{
public:
    /// default constructor
    DimensionsWrapper();
    /// constructor setting the wrapped member
    /// \param dimensions wrapped member value
    DimensionsWrapper(const Dimensions & dimensions);
    /// constructor setting the wrapped member's members
    /// \param x starting vertex x-axis coordinate
    /// \param y starting vertex y-axis coordinate
    /// \param z starting vertex z-axis coordinate
    /// \param width rectangle width
    /// \param height rectangle height
    DimensionsWrapper(const float x, const float y, const float z, const float width, const float height);

    /// sums up wrapped dimensions and deltaDimensions
    /// \param deltaDimensions DimensionsWrapper to be added to wrapped dimensions
    /// \return sum of wrapped dimensions and deltaDimensions
    DimensionsWrapper updateWithDeltaDimensions(DimensionsWrapper &deltaDimensions) const;
    /// maps normalized wrapped dimensions values to cave dimensions
    void normalizedToCave();
    /// tests the wrapped dimensions against the default instance for equality
    /// \return true if the wrapped member is the same as the default instance, else false
    bool isDefault() const;
    /// converts the object to a readable string
    /// \return string containing member values
    std::string toString() const;
};
