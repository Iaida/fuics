#pragma once

#include "ConfigurationMessage.pb.h"
#include "Wrapper.h"
#include "EventMessageWrapper.h"

/// wrapper class for protobuf Listener
class ListenerWrapper : public Wrapper<Listener>
{
public:
    /// constructor setting the wrapped member
    /// \param listener wrapped member value
    ListenerWrapper(const Listener & listener);
    /// returns an event message based on the wrapped's member values
    /// \param elementID the element id of the returned EventMessageWrapper
    /// \return EventMessageWrapper with wrapped's members and elementID
    EventMessageWrapper invoke(const int32_t elementID) const;
    /// checks the wrapped listener's EventType and the input eventType for equality
    /// \param eventType EventType to be checked against
    /// \return true if they are the same, else false
    bool sameEventType(const EventType eventType) const;
    /// converts the object to a readable string
    /// \return string containing member values
    std::string toString() const;
};
