#pragma once

#include "Wrapper.h"
#include "ConfigurationMessage.pb.h"
#include "EventMessageWrapper.h"
#include "ListenerWrapper.h"
#include "../physics/Button.h"

/// wrapper class for protobuf UIElement
class UIElementWrapper : public Wrapper<UIElement>
{
public:
    /// default constructor
    UIElementWrapper();
    /// constructor setting the wrapped member
    /// \param uiElement wrapped member value
    UIElementWrapper(const UIElement & uiElement);

    /// returns event messages invoked by the element's listeners if they have the same eventType
    /// \param eventType EventType to be compared against listener's EventType
    /// \return vector of EventMessageWrappers
    std::vector<EventMessageWrapper> invokeListeners(const EventType eventType) const;
    /// adds a listener object to the UIElement's listeners
    /// \param listenerWrapper listener to be added
    void addListener(ListenerWrapper & listenerWrapper);
    /// converts the object to a readable string
    /// \return string containing member values
    std::string toString() const;

    /// saves the collision bool value from the last iteration
    bool oldCollision;
    /// pair of bool for 'event occurred' and an EventType,
    /// EventType only relevant if bool true
    std::pair<bool, EventType> eventOccurredTypePair;
    /// button rectangle corresponding to the UIElement,
    /// used for collision detection
    Button button;
};
