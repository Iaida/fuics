
#include "DimensionsWrapper.h"
#include "../physics/CaveDimensions.h"

#include <google/protobuf/util/message_differencer.h>

DimensionsWrapper::DimensionsWrapper()
{
}

DimensionsWrapper::DimensionsWrapper(const Dimensions &dimensions) : Wrapper(dimensions)
{
}

DimensionsWrapper::DimensionsWrapper(const float x, const float y, const float z, const float width,
                                     const float height)
{
    wrapped.set_x(x);
    wrapped.set_y(y);
    wrapped.set_z(z);
    wrapped.set_width(width);
    wrapped.set_height(height);
}

std::string DimensionsWrapper::toString() const
{
    return "[X/Y/Z][" +
           std::to_string(wrapped.x()) + "/" +
           std::to_string(wrapped.y()) + "/" +
           std::to_string(wrapped.z()) + "] \n" +
           "[H/W][" +
           std::to_string(wrapped.height()) + "/" +
           std::to_string(wrapped.width()) + "]";
}



DimensionsWrapper DimensionsWrapper::updateWithDeltaDimensions(DimensionsWrapper &deltaDimensions) const
{
    DimensionsWrapper result;
    result.wrapped.set_x(wrapped.x() + deltaDimensions->x());
    result.wrapped.set_y(wrapped.y() + deltaDimensions->y());
    result.wrapped.set_z(wrapped.z() + deltaDimensions->z());
    result.wrapped.set_width(wrapped.width() + deltaDimensions->width());
    result.wrapped.set_height(wrapped.height() + deltaDimensions->height());
    return result;

}

void DimensionsWrapper::normalizedToCave()
{
    wrapped.set_x(CaveDimensions::normalizedToCaveX(wrapped.x()));
    wrapped.set_y(CaveDimensions::normalizedToCaveY(wrapped.y()));
    wrapped.set_z(CaveDimensions::normalizedToCaveZ(wrapped.z()));
    wrapped.set_width(CaveDimensions::normalizedToCaveEdgeLength(wrapped.width()));
    wrapped.set_height(CaveDimensions::normalizedToCaveEdgeLength(wrapped.height()));
}

bool DimensionsWrapper::isDefault() const
{
    return google::protobuf::util::MessageDifferencer::Equals(wrapped.default_instance(), wrapped);
}

