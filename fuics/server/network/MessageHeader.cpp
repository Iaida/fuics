#include "MessageHeader.h"
#include <string>
#include "../logger/Logger.h"
#include "../util/StringUtils.h"

const unsigned char MessageHeader::HEADER_BYTE_LENGTH = 4;


MessageHeader::MessageHeader(int32_t messageLength)
        : messageLength(messageLength)
{}

MessageHeader::MessageHeader(const std::vector<char> headerBytes)
{
    std::vector<unsigned char> headerUnsigned(headerBytes.begin(), headerBytes.end());

    std::string headerString = "header bytes: ";
    for (unsigned char c : headerUnsigned)
        headerString += std::to_string(c) + " ";
    //LOG4CXX_DEBUG(Logger::at("MessageHeader"), headerString);

    auto littleEndianHeaderToMessageSize = [](const std::vector<unsigned char>& h) -> int32_t
    {
        int32_t acc = 0;
        for (size_t i = 0; i < h.size(); i++)
            acc += h[i] << (i * 8);
        return acc;
    };

    messageLength = littleEndianHeaderToMessageSize(headerUnsigned);

    //LOG4CXX_DEBUG(Logger::at("MessageHeader"), "read header as hex: " + StringUtils::byteArrayToHexString(headerUnsigned.data(), headerUnsigned.size()));
    //LOG4CXX_DEBUG(Logger::at("MessageHeader"), "message length from header: " + std::to_string(messageLength));

}

std::vector<char> MessageHeader::toBytes() const
{
    const char * plainArray = static_cast<const char*>(static_cast<const void*>(&messageLength));
    std::vector<char> bytes(plainArray, plainArray + HEADER_BYTE_LENGTH);
    //LOG4CXX_DEBUG(Logger::at("MessageHeader"), "header as hex: " + StringUtils::byteVectorToHexString(bytes));
    return bytes;
}
