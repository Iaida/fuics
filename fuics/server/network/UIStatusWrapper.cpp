
#include "UIStatusWrapper.h"
#include <algorithm>

UIStatusWrapper::UIStatusWrapper() {

}

UIStatusWrapper::UIStatusWrapper(const UIStatus &uiStatus) : Wrapper(uiStatus) {

}

UIStatusWrapper::UIStatusWrapper(const float canvasWidth, const float canvasHeight,
                                 std::vector<UIElementWrapper> &uiElements)
{
    wrapped.set_canvas_height(canvasHeight);
    wrapped.set_canvas_width(canvasWidth);

    wrapped.mutable_elements()->Reserve(static_cast<int>(uiElements.size()));
    for (UIElementWrapper & e : uiElements)
        *wrapped.add_elements()= e;
}

std::vector<UIElementWrapper> UIStatusWrapper::getWrappedElements() const
{
    // allocate resources
    std::vector<UIElementWrapper> result(static_cast<size_t>(wrapped.elements().size()));
    // UIStatusWrapper repeated field of UIElement -> vector of UIElementWrapper
    std::transform(wrapped.elements().begin(),
                   wrapped.elements().end(),
                   result.begin(),
                   [](const UIElement & e)
                   {
                       return UIElementWrapper(e);
                   }
    );
    return result;
}

std::string UIStatusWrapper::toString() const
{
    std::string result = "canvas width: " + std::to_string(wrapped.canvas_width()) + "\n" +
                         "canvas height: " + std::to_string(wrapped.canvas_height()) + "\n" +
                         "elements: \n";

    for (const UIElement & e : wrapped.elements())
        result += UIElementWrapper(e).toString();

    return result;
}


void UIStatusWrapper::assignIds(int32_t & elementIdCounter, int32_t & listenerIdCounter)
{
    // iterate through elements and assign them ids if it is the default value 0
    for (UIElement & e : *wrapped.mutable_elements())
    {
        if (e.id() == 0)
            e.set_id(elementIdCounter++);

        // iterate through listeners and assign them ids if it is the default value 0
        for (Listener & l : *e.mutable_listeners())
        {
            if (l.id() == 0)
                l.set_id(listenerIdCounter++);
        }
    }
}
