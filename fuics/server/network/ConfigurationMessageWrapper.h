#pragma once

#include "ConfigurationMessage.pb.h"
#include "Wrapper.h"

/// wrapper class for protobuf ConfigurationMessage
class ConfigurationMessageWrapper : public Wrapper<ConfigurationMessage>
{
public:
    /// default constructor
    ConfigurationMessageWrapper();
    /// converts the wrapped ConfigurationMessage's request type enum into a readable string
    /// \return wrapped ConfigurationMessage's request type as string
    std::string requestType() const;
    /// converts the object to a readable string
    /// \return string containing member values
    std::string toString() const;
};

