#include "EventMessageWrapper.h"

EventMessageWrapper::EventMessageWrapper()
{
}

EventMessageWrapper::EventMessageWrapper(const int32_t elementID, const EventType eventType, const std::string &info,
                                         const int32_t listenerID)
{
    wrapped.set_element_id(elementID);
    wrapped.set_eventtype(eventType);
    wrapped.set_info(info);
    wrapped.set_listener_id(listenerID);
}

std::string EventMessageWrapper::toString() const
{
    return "elementID: " + std::to_string(wrapped.element_id()) + "\n" +
           EventType_Name(wrapped.eventtype()) + "\n" +
           wrapped.info() + "\n" +
           "listenerID: " + std::to_string(wrapped.listener_id()) + "\n";
}
