

#include "ConfigurationMessage.pb.h"
#include "Wrapper.h"
#include "UIElementWrapper.h"

/// wrapper class for protobuf UIStatus
class UIStatusWrapper : public Wrapper<UIStatus>
{
public:
    /// default constructor
    UIStatusWrapper();
    /// constructor setting the wrapped member
    /// \param uiStatus wrapped member value
    UIStatusWrapper(const UIStatus & uiStatus);
    /// constructor setting the wrapped member's members
    /// \param canvasWidth wrapped's canvasWidth
    /// \param canvasHeight wrapped's canvasHeight
    /// \param uiElements wrapped's uiElements
    UIStatusWrapper(const float canvasWidth, const float canvasHeight,
                    std::vector<UIElementWrapper> &uiElements);

    /// converts google protobuf repeated field to vector
    /// \return resulting vector of UIElementWrappers
    std::vector<UIElementWrapper> getWrappedElements() const;
    /// assigns unique ids to the UIStatus' elements and their listeners
    /// \param elementIdCounter reference to the id counter for elements, gets incremented
    /// \param listenerIdCounter reference to the id counter for listeners, gets incremented
    void assignIds(int32_t & elementIdCounter, int32_t & listenerIdCounter);
    /// converts the object to a readable string
    /// \return string containing member values
    std::string toString() const;
};
