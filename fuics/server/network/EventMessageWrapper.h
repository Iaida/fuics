#pragma once

#include "Wrapper.h"
#include "EventMessage.pb.h"

/// wrapper class for protobuf EventMessage
class EventMessageWrapper : public Wrapper<EventMessage>
{
public:
    /// default constructor
    EventMessageWrapper();
    /// constructor setting the wrapped EventMessage's members
    /// \param elementID element id field
    /// \param eventType EventType field
    /// \param info optional string field
    /// \param listenerID listener id field
    EventMessageWrapper(const int32_t elementID, const EventType eventType, const std::string & info,
                        const int32_t listenerID);
    /// converts the object to a readable string
    /// \return string containing member values
    std::string toString() const;
};
