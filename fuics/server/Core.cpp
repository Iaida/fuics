#include "Core.h"
#include "network/UIStatusWrapper.h"
#include "util/Timer.h"
#include "util/AlgorithmUtils.h"
#include "physics/Collision.h"

#include <glm/gtx/string_cast.hpp>


Core::Core() :
        eventServer(5001),
        configurationServer(5000),
        assistActive(false),
        elementIdCounter(1),
        listenerIdCounter(1)
{
    LOG4CXX_INFO(Logger::at("Core"), "starting core");
}

void Core::update()
{
    // time step only makes sense if it matches DTrack
    static Timer timer(Timer::hz60);
    if (timer.check())
    {
        trackingDataManager.update();
        trackingData = trackingDataManager.getTrackingData();
    }
    // communication independent of DTrack/physics time step
    processServerMessages();

    if (timer.check())
    {
        feet = calculateFeetRectangles();
        updateButtonCollisions();
    }
}

std::vector<Foot> Core::calculateFeetRectangles() const
{
    // calculate feet rectangles with current trackingData and calibration

    std::vector<Foot> feet;

    // iterate through trackers set by calibration
    for (const Calibration & c : calibration.calibrations)
    {
        // search current tracker data for tracker with same id
        auto trackerIt = std::find_if(
                trackingData.trackers.begin(),
                trackingData.trackers.end(),
                [&c](const Tracker & t) { return t.sameID(c.tracker); });

        // check if found
        if (trackerIt != trackingData.trackers.end())
        {
            // calculate the difference between current tracker data and calibrated tracker data
            Tracker difference = trackerIt->difference(c.tracker);
            // calculate the transformation matrix from the difference
            glm::mat4x4 matrix = difference.calcTransformationMatrixFromRotationMatrix();
            LOG4CXX_TRACE(Logger::at("Core"), glm::to_string(matrix));
            // create a new foot rectangle by applying the transformation matrix to the foot rectangle from the calibration
            Foot transformedCalibrationFoot = matrix * c.foot;
            // add to feet vector
            feet.push_back(transformedCalibrationFoot);
        }
        else
            LOG4CXX_TRACE(Logger::at("Core"), "no tracker found for calibrated tracker with id " + std::to_string(c.tracker.id));
    }
    return feet;
}

void Core::updateButtonCollisions()
{
    // Broad Phase: remove feet not in z range
    std::vector<Foot> filteredFeet = feet;
    AlgorithmUtils::removeElementsIfToFound(filteredFeet,
                                            [](const Foot &f) { return !f.inZRange(CaveDimensions::minZCoordinate); });

    // cancel early if no feet left after filtering
    if (filteredFeet.empty())
        return;

    // Narrow Phase
    for (UIElementWrapper & element : uiElements)
    {
        // update button collisions by testing buttons against filteredFeet rectangles
        // iterate through filteredFeet until at least one collision is found or end is reached
        auto footIt = std::find_if(filteredFeet.begin(),
                                   filteredFeet.end(),
                                   [&element](const Foot & f) -> bool
                                   {
                                       // collision of Rectangles
                                       Collision c(f, element.button);
                                       return c.collision;
                                   });

        // check if collision found or end reached and assign bool
        bool collision = (footIt != filteredFeet.end());

        element.eventOccurredTypePair = eventTypeFromCollisions(element.oldCollision, collision);

        if (element.eventOccurredTypePair.first)
        {
            LOG4CXX_DEBUG(Logger::at("Core"), "found Event of type " + EventType_Name(element.eventOccurredTypePair.second) + ", invoking listeners...");

            // send event message to clients for each event evoked by the element's listeners
            for (EventMessageWrapper & eventMessage : element.invokeListeners(element.eventOccurredTypePair.second))
                eventServer.invokeWrite(eventMessage);
        }

        // save old button collisions for next iteration
        element.oldCollision = collision;
    }
}


void Core::run()
{
    while (true)
    {
        update();
    }
}

void Core::processServerMessages()
{
    // TODO : keep-alive, heartbeat ???

    // fill message queues with available messages in buffer
    configurationServer.poll();
    // process message queue and write appropriate responses
    while (configurationServer.hasMessages())
    {
        // get next message from queue
        ConfigurationMessageWrapper configurationMessage = configurationServer.nextMessage();
        // print to console
        LOG4CXX_TRACE(Logger::at("Core"), configurationMessage.toString());
        // create response
        ConfigurationMessageWrapper response = interpretMessage(configurationMessage);
        // write response
        configurationServer.invokeWrite(response);
    }

    // TODO : necessary for writing events ???
    eventServer.poll();
}


ConfigurationMessageWrapper Core::interpretMessage(ConfigurationMessageWrapper &msg)
{
    ConfigurationMessageWrapper response;

    // default response is a copy
    response->CopyFrom(msg);
    response->set_error(ERR_NOERROR);

    LOG4CXX_DEBUG(Logger::at("Core"), msg.requestType());

    switch (msg->requesttype())
    {
        case ASSIST:
        {
            // starts assist for receiving of update_dimensions messages
            // sets initial dimensions for calibration with dimensions field
            if (!assistActive)
            {
                assistActive = true;

                DimensionsWrapper dimensionsWrapper(msg->dimensions());
                // check if dimensions message field is set by comparing it to the default instance
                if (!dimensionsWrapper.isDefault())
                {
                    LOG4CXX_DEBUG(Logger::at("Core"), dimensionsWrapper.toString())
                    // 0-1 to cave dimensions
                    dimensionsWrapper.normalizedToCave();
                    LOG4CXX_DEBUG(Logger::at("Core"), "normalized")
                    LOG4CXX_DEBUG(Logger::at("Core"), dimensionsWrapper.toString())
                    // add assist message dimensions to the default dimensions
                    calibration.updateWithDeltaDimensionsFromDefault(dimensionsWrapper);
                }

                // send calibrated dimensions back as response
                response->mutable_dimensions()->CopyFrom(calibration.dimensions);
            }
            else
                response->set_error(ERR_ASSIST_ACTIVE);
            break;
        }

        case UPDATE_DIMENSIONS:
        {
            if (assistActive)
            {
                // update calibration with wrapped dimensions
                DimensionsWrapper dimensionsWrapper(msg->dimensions());
                dimensionsWrapper.normalizedToCave();
                calibration.updateWithDeltaDimensions(dimensionsWrapper);

                // send calibrated dimensions back as response
                response->mutable_dimensions()->CopyFrom(calibration.dimensions);
            }
            else
                response->set_error(ERR_ASSIST_INACTIVE);
            break;
        }

        case FINALIZE:
        {
            // when feet positioned inside fitting rectangles, assign current tracking values to the calibration
            if (assistActive)
            {
                assistActive = false;

                if (calibration.setCalibrations(trackingData))
                {
                    LOG4CXX_INFO(Logger::at("Core"), "setCalibrations successful");
                }
                else
                {
                    LOG4CXX_INFO(Logger::at("Core"), "setCalibrations failed");
                    response->set_error(ERR_UNKNOWN);
                }
            }
            else
                response->set_error(ERR_ASSIST_INACTIVE);
            break;
        }

        case DEBUG:
        {
            UIStatusWrapper uiStatusWrapper(canvasWidth, canvasHeight, uiElements);
            response->mutable_status()->CopyFrom(uiStatusWrapper);
            response->mutable_status()->set_canvas_width(canvasWidth);
            response->mutable_status()->set_canvas_height(canvasHeight);
            break;
        }

        case ADD_ELEMENT:
        {
            // wrap ui element and set its id
            UIElementWrapper toAdd(msg->elementinfo());
            toAdd->set_id(elementIdCounter++);
            // add the element to vector
            uiElements.push_back(toAdd);
            // respond with new id
            response->mutable_elementinfo()->set_id(toAdd->id());
            break;
        }

        case REMOVE_ELEMENT:
        {
            int32_t id = msg->elementinfo().id();
            bool found = AlgorithmUtils::removeElementsIfToFound(uiElements,
                                          [id, &response](UIElementWrapper &e) {
                                              if (e->id() == id) {
                                                  // if found, set element to be removed as response
                                                  response->mutable_elementinfo()->CopyFrom(e);
                                                  return true;
                                              } else
                                                  return false;
                                          });
            // check if found
            if (!found)
                response->set_error(ERR_UNKNOWN_ID);
            break;
        }

        case REPLACE_STATUS:
        {
            UIStatusWrapper uiStatusWrapper(msg->status());
            LOG4CXX_DEBUG(Logger::at("Core"), uiStatusWrapper.toString())
            uiStatusWrapper.assignIds(elementIdCounter, listenerIdCounter);

            // update member values
            uiElements = uiStatusWrapper.getWrappedElements();

            // send back the newly set status
            response->mutable_status()->set_canvas_width(1.0f);
            response->mutable_status()->set_canvas_height(1.0f);
            response->mutable_status()->CopyFrom(UIStatusWrapper(canvasWidth, canvasHeight, uiElements));
            break;
        }

        case ADD_LISTENER:
        {
            int32_t elementID = msg->elementinfo().id();
            // look for element with same id as message
            auto elementIt = std::find_if(uiElements.begin(),
                                          uiElements.end(),
                                          [elementID](UIElementWrapper & e) { return e->id() == elementID; } );
            // check if found
            if (elementIt != uiElements.end())
            {
                // wrap listener
                ListenerWrapper listenerWrapper(msg->listenerinfo());
                // set unique id
                listenerWrapper->set_id(listenerIdCounter++);
                // add to elements listeners
                elementIt->addListener(listenerWrapper);
                // respond with set id
                response->mutable_listenerinfo()->CopyFrom(listenerWrapper);
            }
            else
                response->set_error(ERR_UNKNOWN_ID);
            break;
        }

        case REMOVE_LISTENER:
        {
            bool elementIdNotFound = false;
            int32_t elementID = msg->elementinfo().id();
            // check if not default value
            if (elementID != 0)
            {
                // look for element with same id as message
                auto elementIt = std::find_if(uiElements.begin(),
                                              uiElements.end(),
                                              [elementID](UIElementWrapper & e) { return e->id() == elementID; } );
                // check if found
                if (elementIt != uiElements.end())
                {
                    int32_t  listenerID = msg->listenerinfo().id();
                    // look for listener with same id as message and erase it
                    bool found = AlgorithmUtils::removeElementsIfToFound(*(*elementIt)->mutable_listeners(),
                                                  [listenerID, &response](const Listener &l)
                                                  {
                                                      if (l.id() == listenerID)
                                                      {
                                                          // if found, set listener to be removed as response
                                                          response->mutable_listenerinfo()->CopyFrom(l);
                                                          return true;
                                                      }
                                                      else
                                                          return false;
                                                  });
                    if (!found)
                        response->set_error(ERR_UNKNOWN_ID);
                }
                else
                {
                    LOG4CXX_INFO(Logger::at("Core"), "element id " + std::to_string(elementID) + " not found, searching instead");
                    elementIdNotFound = true;
                }
            }
            // check if default value or previous step failed
            if (elementID == 0 || elementIdNotFound)
            {
                // search uiElements and their listeners for listener id, remove that listener
                int32_t  listenerID = msg->listenerinfo().id();
                bool found = false;
                for (UIElementWrapper & element : uiElements)
                {
                    // look for listener with same id as message and erase it
                    found = AlgorithmUtils::removeElementsIfToFound(*element->mutable_listeners(),
                                             [listenerID, &response](const Listener &l)
                                             {
                                                 if (l.id() == listenerID)
                                                 {
                                                     // if found, set listener to be removed as response
                                                     response->mutable_listenerinfo()->CopyFrom(l);
                                                     return true;
                                                 }
                                                 else
                                                     return false;
                                             });
                    // stop searching when first found
                    if (found)
                        break;
                }
                if (!found)
                    response->set_error(ERR_UNKNOWN_ID);
            }
            break;
        }
        default:
            throw std::runtime_error("invalid request type");
    }
    return response;
}


std::pair<bool, EventType> Core::eventTypeFromCollisions(const bool previous, const bool current) const
{
    // check if at least one of both boolean values is true
    bool eventOccurred = previous || current;  // NOT (False -> False)
    // initialize with default value
    EventType eventType = PRESS;
    if (eventOccurred)
    {
        // determine eventType based on current and old collision bool value
        if (!previous && current)
            eventType = EventType::PRESS; // False -> True
        else if (previous && !current)
            eventType = EventType::RELEASE; // True -> False
        else
            eventType = EventType::HOLD; // True -> True
    }
    return std::pair<bool, EventType>(eventOccurred, eventType);
}
