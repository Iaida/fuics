#pragma once

#include "tracking/TrackingDataManager.h"
#include "physics/CalibrationData.h"
#include "network/Server.h"
#include "network/ConfigurationMessageWrapper.h"
#include "network/EventMessageWrapper.h"
#include "network/UIElementWrapper.h"

/// class which functions as the center of the whole program
class Core
{
public:
    /// default constructor, which initializes members
    Core();
    /// starts endless update loop
    void run();

protected:
    /// executes an entire iteration step including tracking, client-communication and physics
    virtual void update();

private:
    /// checks the configuration server message queue for new messages, interprets them and sends a response
    void processServerMessages();
    /// interprets a configuration message and returns a response
    /// \param msg message to be interpreted
    /// \return response to the client
    ConfigurationMessageWrapper interpretMessage(ConfigurationMessageWrapper &msg);
    /// calculates the current feet rectangles from calibration and trackingData members
    /// \return resulting vector of feet rectangles
    std::vector<Foot> calculateFeetRectangles() const;
    /// broad and narrow phase collision detection feet and uiElements' buttons
    void updateButtonCollisions();
    /// computes an optional EventType from two boolean values
    /// \param previous collision bool from previous iteration
    /// \param current collision bool from current iteration
    /// \return pair of 'an event occurred' bool and EventType, which is only relevant if the bool was true
    std::pair<bool, EventType> eventTypeFromCollisions(const bool previous, const bool current) const;

protected:
    /// the current calibration which includes feet rectangles and tracker values
    CalibrationData calibration;
    /// part of UIStatus, the current ui elements which represent buttons from the game client
    std::vector<UIElementWrapper> uiElements;
    /// the current feet rectangles, calculated from calculateFeetRectangles
    std::vector<Foot> feet;
    /// bool indicating if the calibration process is currently active,
    /// changed by ASSIST and FINALIZE configuration messages
    bool assistActive;
    /// the current tracking data struct containing multiple trackers, obtained from trackingDataManager
    TrackingData trackingData;

private:
    /// the server responsible for sending and receiving ConfigurationMessages
    Server<ConfigurationMessageWrapper> configurationServer;
    /// the server responsible for sending and receiving EventMessages
    Server<EventMessageWrapper> eventServer;
    /// counter for assigning unique id's to ui elements,
    /// id's error/default value is 0, start with 1
    int32_t elementIdCounter;
    /// counter for assigning unique id's to listeners,
    /// id's error/default value is 0, start with 1
    int32_t listenerIdCounter;
    /// manages tracking string parsing and averaging TrackingData over multiple iterations
    TrackingDataManager trackingDataManager;
    /// part of UIStatus, needed for normalization client-side
    const float canvasHeight = 1.0f;
    /// part of UIStatus, needed for normalization client-side
    const float canvasWidth = 1.0f;
};
