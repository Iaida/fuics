#pragma once
#include "Calibration.h"
#include "../tracking/TrackingData.h"
#include "CaveDimensions.h"
#include "../logger/Logger.h"

#include <vector>
#include <log4cxx/propertyconfigurator.h>


// TODO : calibration save and load from file ???

/// class containing multiple Calibration structs and functionality for manipulating the current calibration
class CalibrationData
{
public:
    /// default constructor,
    /// currently creates hardcoded calibration
	CalibrationData();
    /// checks trackingData and adds the Tracker values to calibration
    /// if they are in the bottom quarter of the cave
    /// \param trackingData used to set the Tracker members of Calibration structs
    /// \return true if one or two trackers were valid and added to the calibration
    bool setCalibrations(const TrackingData &trackingData);
    /// sets the dimensions member to the sum of defaultDimensions and deltaDimensions
    /// \param deltaDimensions dimensions to be added
    void updateWithDeltaDimensionsFromDefault(DimensionsWrapper &deltaDimensions);
    /// adds deltaDimensions to the dimensions member
    /// \param deltaDimensions dimensions to be added
    void updateWithDeltaDimensions(DimensionsWrapper &deltaDimensions);

    /// vector of Calibration structs containing the current calibration,
    /// only trackers with an id present in this vector are relevant for our system,
    /// for example two for feet, other trackers might be used in unity game
    std::vector<Calibration> calibrations;
    /// current dimensions for calibration process,
    /// used to create Foot Rectangles when finished with calibration process,
    /// used to draw two Foot Rectangles onto cave floor
    DimensionsWrapper dimensions;
    /// default dimensions for calibration process,
    /// start with their positions at middle of cave floor and no area
    static const DimensionsWrapper defaultDimensions;
};
