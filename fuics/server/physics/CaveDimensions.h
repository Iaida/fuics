#pragma once

/// namespace containing cave dimensions and mapping functions,
/// x is cave width,
/// y is cave depth,
/// z is cave height,
/// values map to real millimeters,
/// example: value 1000.0f -> 1000 mm, 100 cm, 1 m
namespace CaveDimensions
{
	/// cave's edge length
	const float edgeLength = 2500.0f;

	/// cave's minimum x-axis coordinate
	const float minXCoordinate = -1250.0f;
	/// cave's maximum x-axis coordinate
	const float maxXCoordinate = 1250.0f;
	/// cave's center x-axis coordinate
	const float midXCoordinate = (minXCoordinate + maxXCoordinate) / 2.0f;

	/// cave's minimum y-axis coordinate
	const float minYCoordinate = -1250.0f;
	/// cave's maximum y-axis coordinate
	const float maxYCoordinate = 1250.0f;
	/// cave's center y-axis coordinate
	const float midYCoordinate = (minYCoordinate + maxYCoordinate) / 2.0f;

	/// cave's minimum z-axis coordinate
	const float minZCoordinate = 0.0f;
	/// cave's maximum z-axis coordinate
	const float maxZCoordinate = 2500.0f;
	/// cave's center z-axis coordinate
	const float midZCoordinate = (minZCoordinate + maxZCoordinate) / 2.0f;

	/// maps a normalized x-axis value to cave dimensions
	/// \param x x-axis value between 0 and 1
	/// \return x mapped to a value between minXCoordinate and maxXCoordinate
	inline
	float normalizedToCaveX(const float x)
	{
		return (x * edgeLength) + minXCoordinate;
	}

	/// maps a normalized y-axis value to cave dimensions
	/// \param y y-axis value between 0 and 1
	/// \return y mapped to a value between minXCoordinate and maxXCoordinate
	inline
	float normalizedToCaveY(const float y)
	{
		return (y * edgeLength) + minYCoordinate;
	}

	/// maps a normalized z-axis value to cave dimensions
	/// \param z z-axis value between 0 and 1
	/// \return z mapped to a value between minXCoordinate and maxXCoordinate
	inline
	float normalizedToCaveZ(const float z)
	{
		return (z * edgeLength) + minZCoordinate;
	}

	/// maps a normalized width or height value to cave dimensions
	/// \param value width or height between 0 and 1
	/// \return input value mapped to a value between minXCoordinate and maxXCoordinate
	inline
	float normalizedToCaveEdgeLength(const float value)
	{
		return value * edgeLength;
	}

	/// maps a cave's x-coordinate to a screen's x-coordinate
	/// \param x cave x-coordinate
	/// \param screenWidth maximum x-coordinate to map to
	/// \return screen x-coordinate
	inline
	int caveToScreenX(const float x, const int screenWidth)
	{
		float normalized = (x - minXCoordinate) / edgeLength;
		return static_cast<int>(normalized * screenWidth);
	}

	/// maps a cave's y-coordinate to a screen's y-coordinate
	/// flips top and bottom, changes origin of screen coordinates from top left to bottom left
	/// \param y cave y-coordinate
	/// \param screenHeight maximum y-coordinate to map to
	/// \return screen y-coordinate
	inline
	int caveToScreenY(const float y, const int screenHeight)
	{
		float normalized = (y - minYCoordinate) / edgeLength;
		return static_cast<int>((1.0f - normalized) * screenHeight);
	}
};
