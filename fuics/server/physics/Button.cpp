#include "Button.h"
#include "CaveDimensions.h"

Button::Button()
{
}

Button::Button(const float x, const float y, const float width, const float height) :
        Rectangle(CaveDimensions::normalizedToCaveX(x),
                  CaveDimensions::normalizedToCaveY(y),
                  CaveDimensions::normalizedToCaveEdgeLength(width),
                  CaveDimensions::normalizedToCaveEdgeLength(height))

{
}

Button::Button(DimensionsWrapper &dimensionsWrapper) :
        Rectangle(CaveDimensions::normalizedToCaveX(dimensionsWrapper->x()),
                  CaveDimensions::normalizedToCaveY(dimensionsWrapper->y()),
                  CaveDimensions::normalizedToCaveEdgeLength(dimensionsWrapper->width()),
                  CaveDimensions::normalizedToCaveEdgeLength(dimensionsWrapper->height()))
{
}
