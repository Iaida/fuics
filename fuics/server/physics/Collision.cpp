
#include "Collision.h"


Collision::Collision(const Rectangle &a, const Rectangle &b)
{
    // based on: http://www.dyn4j.org/2010/01/sat/

    // TODO : add face normals to axes ???
    // http://gamedev.stackexchange.com/questions/44500/how-many-and-which-axes-to-use-for-3d-obb-collision-with-sat/

    // axes to test for SAT

    // 2 edges of rectangle A and rectangle B
    // followed by
    // cross product of pairs of edges of rectangle A and rectangle B
    // -> reduce number of tests by restricting rectangle to parallelogram
    // -> only 2 relevant edges per rectangle

    std::array<glm::vec3, 8> axes =
    {
        a.s(), a.t(),
        b.s(), b.t(),
        glm::cross(a.s(), b.s()),
        glm::cross(a.s(), b.t()),
        glm::cross(a.t(), b.s()),
        glm::cross(a.t(), b.t())
    };

    // if every axis had an overlap we can guarantee an intersection
    collision = std::all_of(
            axes.begin(),
            axes.end(),
            [this, &a, &b](const glm::vec3 & axis) -> bool
            {
                // handle zero-case of cross product
                if (axis == glm::vec3(0.0f))
                    return true;
                else
                {
                    // project both shapes onto the axis
                    AxisProjection ap(axis, a.getVertices());
                    AxisProjection bp(axis, b.getVertices());
                    // do the projections overlap?
                    // if they do not overlap, then we can guarantee that the shapes do not overlap
                    return AxisProjection::overlap(ap, bp);
                }
            }
    );
}

