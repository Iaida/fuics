#include "Rectangle.h"
#include "CaveDimensions.h"

Rectangle::Rectangle() :
  p_(glm::vec3(0.0f)), s_(glm::vec3(0.0f)), t_(glm::vec3(0.0f))
{
}

Rectangle::Rectangle(const glm::vec3 & p, const glm::vec3 & s, const glm::vec3 & t)	: 
	p_(p), s_(s), t_(t)
{
}

Rectangle::Rectangle(const float x, const float y, const float width, const float height) :
	p_(glm::vec3(x, y, CaveDimensions::minZCoordinate)), s_(glm::vec3(width, 0.0f, 0.0f)), t_(glm::vec3(0.0f, height, 0.0f))
{
}


const glm::vec3 & Rectangle::getA() const
{
	return p_;
}

glm::vec3 Rectangle::getB() const
{
	return p_ + s_;
}

glm::vec3 Rectangle::getC() const
{
	return p_ + t_;
}

glm::vec3 Rectangle::getD() const
{
	return p_ + t_ + s_;
}

const glm::vec3 & Rectangle::p() const
{
	return p_;
}

const glm::vec3 & Rectangle::s() const
{
	return s_;
}

const glm::vec3 & Rectangle::t() const
{
	return t_;
}

std::array<glm::vec3, 4> Rectangle::getVertices() const
{

	return std::array<glm::vec3, 4>{getA(), getB(), getD(), getC()};
}




glm::vec3 Rectangle::getCenter() const
{
    return (getA() + getD()) / 2.0f;
}

void Rectangle::setFromVertices(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c)
{
	p_ = a;
	s_ = b - a;
	t_ = c - a;
}

std::string Rectangle::verticesString() const
{
	std::string s = "\n";
	for (glm::vec3 & v : getVertices())
		s += std::to_string(v.x) + " " + std::to_string(v.y) + " " + std::to_string(v.z) + "\n";
	return s;
}
