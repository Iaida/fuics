#pragma once
#include "Rectangle.h"
#include "../network/DimensionsWrapper.h"
#include "CaveDimensions.h"

#include <glm/mat4x4.hpp>

/// class representing a foot rectangle with additional functionality
class Foot : public Rectangle
{
public:
    /// default constructor
    Foot();
    /// same constructor as in Rectangle, only default initializes right member
    Foot(const glm::vec3 & p, const glm::vec3 & s, const glm::vec3 & t);
    /// same constructor as in Rectangle, only default initializes right member
    Foot(const float x, const float y, const float width, const float height);
    /// constructor used to initialize foot member of calibration struct
    /// \param dimensionsWrapper dimensions of the constructed rectangle
    /// \param rightHalf rectangle extends to the right if true, else to the left
    Foot(DimensionsWrapper &dimensionsWrapper, const bool rightHalf);

    /// checks if any of the foot rectangle vertices are positioned at a lower z value
    /// than a certain threshold
    /// \param zThreshold z-axis threshold to be tested against
    /// \return true if any vertices lower than threshold, else false
    bool inZRange(const float zThreshold = CaveDimensions::minZCoordinate) const;
    /// calculate vector to the center of the front of the foot rectangle
    /// \return vector to the center of the front of the foot rectangle
    glm::vec3 frontEdgeCenter() const;

    /// bool indicating of the foot rectangle belongs to the right or left foot,
    /// used when rendering an indicator for the debug window
    bool right;
    /// equality operator, used for testing
    /// \param other foot to be compoared against
    /// \return result for equality
    bool operator==(const Foot& other) const;
};

/// applies a transformation matrix to a foot rectangle,
/// foot rotated around center, not vertex at p,
/// order of parameters is not relevant for the result
/// \param matrix transformation matrix
/// \param foot foot to be transformed
/// \return transformed foot
Foot operator*(const glm::mat4x4 & matrix, const Foot & foot);
/// applies a transformation matrix to a foot rectangle,
/// foot rotated around center, not vertex at p,
/// order of parameters is not relevant for the result
/// \param foot foot to be transformed
/// \param matrix transformation matrix
/// \return transformed foot
Foot operator*(const Foot & foot, const glm::mat4x4 & matrix);
