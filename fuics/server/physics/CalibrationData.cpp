#include "CalibrationData.h"


const DimensionsWrapper CalibrationData::defaultDimensions =
        DimensionsWrapper(CaveDimensions::midXCoordinate,
                          CaveDimensions::midYCoordinate,
                          CaveDimensions::minZCoordinate,
                          0.0f,
                          0.0f);


CalibrationData::CalibrationData() :
        // imprecise calibration dimensions for usage without using calibration process, 13cm width, 27cm height
        //  --      --
        // |  |    |  |
        // |  |    |  |
        //  --p    p--
        // starting points p at dimensions position
        // x-position depending on right or left
        // with width and height of dimensions
        dimensions(CaveDimensions::midXCoordinate,
                   CaveDimensions::midYCoordinate,
                   CaveDimensions::minZCoordinate,
                   130.0f,
                   270.0f)
{
    // imprecise calibration feet and tracker for usage without using calibration process
    Foot leftFoot(dimensions, false);
    Foot rightFoot(dimensions, true);

    Tracker leftTracker(2, 1.0, glm::vec3(-103.5f, 191.87f, 44.27f), glm::vec3(-23.6f, -3.7f, -4.74f),
                        glm::mat3x3(0.99f, -0.05f, 0.09f, 0.08f, 0.91f, -0.39f, -0.06f, 0.4f, 0.91f));
    Tracker rightTracker(4, 1.0, glm::vec3(101.0f, 189.44f, 39.0f), glm::vec3(-71.3f, -44.7f, -84.8f),
                         glm::mat3x3(0.06f, -0.26f, 0.96f, 0.7f, 0.69f, 0.14f, -0.7f, 0.67f, 0.23f));

    calibrations = {Calibration(leftFoot, leftTracker),
                    Calibration(rightFoot, rightTracker)};
};



void CalibrationData::updateWithDeltaDimensions(DimensionsWrapper & deltaDimensions)
{
    dimensions = dimensions.updateWithDeltaDimensions(deltaDimensions);
}

void CalibrationData::updateWithDeltaDimensionsFromDefault(DimensionsWrapper & deltaDimensions)
{
    dimensions = defaultDimensions.updateWithDeltaDimensions(deltaDimensions);
}

bool CalibrationData::setCalibrations(const TrackingData &trackingData)
{
    // check if trackers present
    if (trackingData.trackers.empty())
    {
        LOG4CXX_INFO(Logger::at("CalibrationData"), "no trackers found");
        return false;
    }

    std::vector<Calibration> newCalibrations;
    for (const Tracker & t : trackingData.trackers)
    {
        // avoid treating trackers such as shutter glasses like feet trackers
        // by only adding trackers in bottom quarter of cave
        // add pair of Foot (based on half and dimensions) and tracker to calibrations
        if (t.bottomQuarter())
            newCalibrations.emplace_back(Foot(dimensions, t.rightHalf()), t);
    }

    // check how many trackers were added
    bool result;
    switch (newCalibrations.size())
    {
        case 0:
        LOG4CXX_INFO(Logger::at("CalibrationData"), "no trackers added to calibration");
            result = false;
            break;
        case 1:
        LOG4CXX_INFO(Logger::at("CalibrationData"), "1 tracker added to calibration");
            result = true;
            break;
        case 2:
        LOG4CXX_INFO(Logger::at("CalibrationData"), "2 trackers added to calibration");
            result =  true;
            break;
        default:
        LOG4CXX_INFO(Logger::at("CalibrationData"), "too many trackers added to calibration: " + std::to_string(newCalibrations.size()));
            result = false;
            break;
    }

    if (result)
    {
        LOG4CXX_INFO(Logger::at("CalibrationData"), "replacing old with new calibrations");
        calibrations = newCalibrations;
    }
    else
    {
        LOG4CXX_INFO(Logger::at("CalibrationData"), "no changes made to calibrations");
    }
    return result;
}