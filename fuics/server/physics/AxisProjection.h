#pragma once

#include <glm/geometric.hpp>
#include <stdexcept>

/// class representing the projection of a shape onto an axis with a
/// minimum and maximum axis coordinate of the projected vertices
class AxisProjection
{
public:
	/// constructs a projection based on an axis and a number of vertices,
	/// based on: <a href="http://www.dyn4j.org/2010/01/sat/#sat-projshape">dyn4j</a>
	/// \tparam Container must contain values of type glm::vec3
	/// \param axis the axis to project the vertices onto
	/// \param vertices Container with the vertices to project onto axis
	template <typename Container>
	AxisProjection(const glm::vec3 & axis, const Container & vertices)
	{
		static_assert(std::is_same<typename Container::value_type, glm::vec3>::value, "Container must contain glm::vec3 elements");

		// the axis must be normalized to get accurate projections
		glm::vec3 normalizedAxis = glm::normalize(axis);

		min = glm::dot(normalizedAxis, vertices[0]);
		max = min;

		for (size_t i = 1; i < vertices.size(); i++)
		{
			float p = glm::dot(normalizedAxis, vertices[i]);
			if (p < min)
				min = p;
			else if (p > max)
				max = p;
		}
		// check if the resulting projection is valid
		if (!wellDefined())
			throw std::runtime_error("Projection not well defined");
	}

	/// tests two AxisProjection objects for an overlap,
	/// static function in addition to class method,
	/// to indicate that the order of parameters is irrelevant
	/// \param a AxisProjection to be checked for an overlap with b
	/// \param b AxisProjection to be checked for an overlap with a
	/// \return true if they overlap, else false
	static bool overlap(const AxisProjection & a, const AxisProjection & b)
	{
		return a.overlap(b);
	};

	/// tests two AxisProjection objects for an overlap
	/// \param other AxisProjection to be checked for an overlap
	/// \return true if they overlap, else false
	bool overlap(const AxisProjection & other) const
	{
		// based on: http://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap
		return min <= other.max && other.min <= max;
	};

private:
	/// tests if the minimum is lower than the maximum
	bool wellDefined() const
	{
		return min <= max;
	}

	/// minimum value on axis
	float min;
	/// maximum value on axis
	float max;
};
