
#include "Foot.h"
#include "CaveDimensions.h"

Foot::Foot() :
        Rectangle(), right(false)
{
}

Foot::Foot(const glm::vec3 &p, const glm::vec3 &s, const glm::vec3 &t) :
        Rectangle(p, s, t), right(false)
{
}

Foot::Foot(const float x, const float y, const float width, const float height) :
        Rectangle(x, y, width, height), right(false)
{
}

bool Foot::inZRange(const float zThreshold) const
{
    return getA().z <= zThreshold ||
           getB().z <= zThreshold ||
           getC().z <= zThreshold ||
           getD().z <= zThreshold;
}

Foot::Foot(DimensionsWrapper &dimensionsWrapper, const bool rightHalf) :
        Rectangle(dimensionsWrapper->x(),
                  dimensionsWrapper->y(),
                  dimensionsWrapper->width(),
                  dimensionsWrapper->height()),
        right(rightHalf)
{
    p_.z = dimensionsWrapper->z();
    // if left half, negate x and width
    if (!rightHalf)
    {
        p_.x = -p_.x;
        s_.x = -s_.x;
    }
}

glm::vec3 Foot::frontEdgeCenter() const
{
    //  --X--
    // |     |
    // |     |
    // |     |
    // p-----
    return p_ + t_ + (s_ / 2.0f);
}

bool Foot::operator==(const Foot &other) const
{
    return p_ == other.p_ &&
           s_ == other.s_ &&
           t_ == other.t_ &&
           right == other.right;
}



Foot operator*(const glm::mat4x4 &matrix, const Foot &foot)
{
    // rotation around center, not starting corner p_
    glm::vec3 pToOrigin = foot.p() - foot.getCenter();
    // p_ with 4th component = 1, translation part applied
    glm::vec3 pTransformedAtOrigin(matrix * glm::vec4(pToOrigin, 1.0f));
    glm::vec3 pTransformedBack = pTransformedAtOrigin + foot.getCenter();
    // s_ and t_ with 4th component = 0, only rotation applied
    glm::vec3 sTransformed(matrix * glm::vec4(foot.s(), 0.0f));
    glm::vec3 tTransformed(matrix * glm::vec4(foot.t(), 0.0f));

    Foot result(pTransformedBack, sTransformed, tTransformed);
    // copy right or left bool
    result.right = foot.right;
    return result;
}

Foot operator*(const Foot &foot, const glm::mat4x4 &matrix)
{
    // reuse flipped order
    return matrix * foot;
}
