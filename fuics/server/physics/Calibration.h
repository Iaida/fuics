#pragma once
#include "Foot.h"
#include "../tracking/Tracker.h"

/// simple struct defining a Calibration with Tracker and Foot member
struct Calibration
{
    /// default constructor
    Calibration() {};
    /// constructor setting members
    /// \param foot Foot to be set
    /// \param tracker Tracker to be set
    Calibration(const Foot & foot, const Tracker & tracker) :
            foot(foot), tracker(tracker)
    {};

    /// initial Tracker, used to build a difference Tracker with current Tracker values by by Core::calculateFeetRectangles()
    Tracker tracker;
    /// initial Foot, transformed by Core::calculateFeetRectangles()
    Foot foot;
};