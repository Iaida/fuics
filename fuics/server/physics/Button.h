#pragma once
#include "Rectangle.h"
#include "../network/DimensionsWrapper.h"

/// class representing a foot rectangle with differing constructor implementation
class Button : public Rectangle
{
public:
    /// default constructor
    Button();
    /// constructor which sets members and takes normalized values from 0 to 1,
    /// maps normalized values to Cave dimensions,
    /// else same constructor as in Rectangle
    Button(const float x, const float y, const float width, const float height);
    /// constructor which sets members and takes normalized dimensions values,
    /// maps normalized values to Cave dimensions
    /// \param dimensionsWrapper dimensions of the constructed rectangle
    Button(DimensionsWrapper & dimensionsWrapper);
};
