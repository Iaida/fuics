#pragma once
#include <array>
#include <glm/vec3.hpp>

/// base class representing a rectangle in 3D
/// represented by a single vertex and two vectors
/// which lead to the adjacent vertices when added to the vertex
class Rectangle
{
protected:
	/// default constructor
	Rectangle();
	/// constructor setting members
	/// \param p corner vertex
	/// \param s vector to adjacent vertex at p+s
	/// \param t vector to adjacent vertex at p+t
	Rectangle(const glm::vec3 & p, const glm::vec3 & s, const glm::vec3 & t);
	/// Constructor for Buttons or initial Rectangles for calibration
	/// \param x x-axis coordinate
	/// \param y y-axis coordinate
	/// \param width x-axis difference to adjacent vertex from p
	/// \param height y-axis difference to adjacent vertex from p
	Rectangle(const float x, const float y, const float width, const float height);
	/// virtual destructor
	virtual ~Rectangle() {};

public:
	/// getter
	/// \return returns vertex p
	const glm::vec3 & p() const;
	/// getter
	/// \return returns vector from p to adjacent vertex at p+s
	const glm::vec3 & s() const;
	/// getter
	/// \return returns vector from p to adjacent vertex at p+t
	const glm::vec3 & t() const;
	/// returns first vertex
	/// \return vertex at p
	const glm::vec3 & getA() const;
	/// returns second vertex
	/// \return vertex at p+s
	glm::vec3 getB() const;
	/// returns third vertex
	/// \return vertex at p+t
	glm::vec3 getC() const;
	/// returns fourth vertex
	/// \return vertex at p+s+t
	glm::vec3 getD() const;
	/// returns all four vertices in an order suitable for XDrawLines
	/// \return array built from four vertices
	std::array<glm::vec3, 4> getVertices() const;
	/// returns the center of the rectangle
	/// \return point at (A+D)/2
	glm::vec3 getCenter() const;

	/// converts Rectangle to string
	/// \return string containing all four vertices
	std::string verticesString() const;
	/// sets the members to fit three vertices representing three of the four corners,
	/// used for testing purposes
	/// \param a first vertex
	/// \param b second vertex
	/// \param c third vertex
	void setFromVertices(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c);

protected:
	/// vertex
	glm::vec3 p_;
	/// vector to adjacent vertex from p
	glm::vec3 s_;
	/// vector to adjacent vertex from p
	glm::vec3 t_;
};
