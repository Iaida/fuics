#pragma once
#include "AxisProjection.h"
#include "Rectangle.h"

#include <glm/geometric.hpp>
#include <algorithm>

// TODO : extend class to include collision point

/// simple class computing collision results from geometric shapes,
/// could be extended to compute the collision point/line
class Collision
{
public:
    /// constructor computing the collision result and setting the member accordingly
    /// \param a rectangle to test against b
    /// \param b rectangle to test against a
    Collision(const Rectangle & a, const Rectangle & b);
    /// true if a collision occurred, else false
    bool collision;
};


