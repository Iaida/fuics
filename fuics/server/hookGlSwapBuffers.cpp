// Not used, only shows how hook might have worked


#define GL_GLEXT_PROTOTYPES
//#define _GNU_SOURCE
// Link statically with GLEW
#define GLEW_STATIC

#include "Core.h"

#include <dlfcn.h>

typedef void (*origSwapBuffers)(Display * dpy, GLXDrawable drawable);

void glXSwapBuffers(Display* dpy, GLXDrawable drawable)
{
    static origSwapBuffers originalSWP = (origSwapBuffers) dlsym(RTLD_NEXT, "glXSwapBuffers");

    // initialize logger once
    static Logger logger;
    // initialize core once
    static Core core;
    // initialize separate thread decoupled from rendering once to let core run in
    static std::thread serverThread([]() { core.run(); });

    // initialize renderer once with glXSwapBuffers' parameters
    static Renderer renderer(dpy, drawable);

    // update and render fps to corner
    renderer.fpsCounter.update();
    renderer.renderText(std::to_string(renderer.fpsCounter.getFps()), 50, 50);

    // render projected feet rectangles
    // test, probably multi-threading problems with getFeet
//    for (const Foot & f : core.getFeet())
//        renderer.renderFoot(f, Color::darkerGreen);

    originalSWP(dpy, drawable);
}

