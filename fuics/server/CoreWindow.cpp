#include "CoreWindow.h"

CoreWindow::CoreWindow()
        : Core()
{
    LOG4CXX_INFO(Logger::at("CoreWindow"), "starting corewindow");
    Display *dpy = XOpenDisplay(nullptr);
    Window w = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy),
                                   0, 0,
                                   800, 800,
                                   0,
                                   Color::white, Color::white);
    XMapWindow(dpy, w);
    renderer = Renderer(dpy, w);
}

void CoreWindow::renderToWindow()
{
    // clear to background color
    renderer.clear();

    // update and render fps to corner
    renderer.fpsCounter.update();
    renderer.renderText(std::to_string(renderer.fpsCounter.getFps()), 50, 50);


    // TODO : rendering of rotated rectangle so that it appears as a line after projection
//    Foot left (CaveDimensions::midXCoordinate - 100.0f, CaveDimensions::midYCoordinate, 100.0f, 250.0f);
//    Foot right(CaveDimensions::midXCoordinate + 100.0f, CaveDimensions::midYCoordinate, 100.0f, 250.0f);
//    renderer.renderFoot(left,  Color::green);
//    renderer.renderFoot(right, Color::cyan);
//
//    Tracker diff;
//    // works counterclockwise viewed from positive side
//    diff.orientation.x = 89.0f;
//    glm::mat4x4 mat = diff.calcTransformationMatrixFromOrientationVector();
//
//    Foot transformedLeft  = left * mat;
//    Foot transformedRight = right * mat;
//    renderer.renderFoot(transformedLeft, Color::red);
//    renderer.renderFoot(transformedRight, Color::magenta);


    // check if currently calibration in progress
    if (assistActive)
    {
        // drawing for calibration

        // draw dimensions, can be changed while assist active
        DimensionsWrapper calibrationDimensions = calibration.dimensions;
        renderer.renderCalibrationDimensions(calibrationDimensions, Color::blue);

        // draw each calibration foot, represents the currently set calibration
        for (const Calibration & c : calibration.calibrations)
            renderer.renderFoot(c.foot, Color::red);
    }

    // render projected feet rectangles
    for (const Foot & f : feet)
        renderer.renderFoot(f, Color::darkerGreen);

    // render buttons to see if they represent application's buttons
    for (UIElementWrapper & e : uiElements)
    {
        std::string centerString = std::to_string(e->id()) + "\n";
        if (e.eventOccurredTypePair.first)
            centerString += EventType_Name(e.eventOccurredTypePair.second);
        renderer.renderButton(e.button, centerString, Color::black);
    }

    renderer.flush();
}


void CoreWindow::handleKeyboardInput(const KeySym keySym)
{
    // following keys only work during calibration
    if (assistActive)
    {
        switch (keySym)
        {
            // calibration position change in 1cm steps
            case XK_Left:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "decrease x by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_x(-10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            case XK_Right:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "increase x by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_x(+10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            case XK_Down:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "decrease y by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_y(-10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            case XK_Up:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "increase y by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_y(+10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }

                // calibration height and width change in 1cm steps
            case XK_KP_Left:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "decrease width by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_width(-10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            case XK_KP_Right:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "increase width by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_width(+10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            case XK_KP_Down:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "decrease height by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_height(-10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            case XK_KP_Up:
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "increase height by 1cm");
                DimensionsWrapper dimensionsWrapper;
                dimensionsWrapper->set_height(+10.0f);
                calibration.updateWithDeltaDimensions(dimensionsWrapper);
                break;
            }
            default:
                break;
        }
    }

    switch (keySym)
    {
        case XK_a:
        {
            LOG4CXX_INFO(Logger::at("CoreWindow"), "assist");
            assistActive = true;
            break;
        }
        case XK_f:
        {
            LOG4CXX_INFO(Logger::at("CoreWindow"), "finalize");
            assistActive = false;

            // when feet positioned inside fitting rectangles, assign current tracking values to the calibration
            if (calibration.setCalibrations(trackingData))
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "setCalibrations successful");
            }
            else
            {
                LOG4CXX_INFO(Logger::at("CoreWindow"), "setCalibrations failed");
            }

            break;
        }
        default:
            break;
    }
}

void CoreWindow::processWindowEvents()
{
    while (renderer.hasEvents())
    {
        XEvent event = renderer.nextEvent();

        if (event.type == KeyPress)
        {
            std::pair<KeySym, std::string> keySymStringPair = renderer.xKeyEventToKeySymStringPair(&event.xkey);

            std::string outputString = "Key Press:";
            outputString += " Keycode: " + std::to_string(event.xkey.keycode);
            outputString += " Keysym: " + std::to_string(keySymStringPair.first);
            outputString += " String: " + keySymStringPair.second;
            LOG4CXX_DEBUG(Logger::at("CoreWindow"), outputString);

            handleKeyboardInput(keySymStringPair.first);
        }
    }
}

void CoreWindow::update()
{
    Core::update();

    processWindowEvents();
    if (renderer.renderTimer.check())
        renderToWindow();
}
