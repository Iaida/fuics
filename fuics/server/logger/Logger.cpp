#include "Logger.h"

#include <log4cxx/propertyconfigurator.h>

std::map<std::string, log4cxx::LoggerPtr> Logger::stringLoggerMap;

Logger::Logger()
{
    log4cxx::PropertyConfigurator::configure("Log4cxxConfig.cfg");
}

log4cxx::LoggerPtr &Logger::at(const std::string &key)
{
    // ".first" : emplace documentation: "Returns a pair consisting of an iterator to the inserted element,
    // or the already-existing element if no insertion happened and ..."
    // "->second" : iterator to std::pair<std::string, log4cxx::LoggerPtr>
    return stringLoggerMap.emplace(key, log4cxx::Logger::getLogger(key)).first->second;
}
