#pragma once

#include <log4cxx/logger.h>
#include <map>

/// Logging class which has to be initialized once and can then be used from anywhere in the code
/// to write to the console and to a file
class Logger
{
public:
    /// default constructor which loads a config file from the same folder as the program executable,
    /// note: backup copy of config file present in logger folder
    Logger();
    /// returns the LoggerPtr value needed for the Logger macro by accessing stringLoggerMap with a string key,
    /// tries to emplace if key not already present
    /// \param key string which maps to the LoggerPtr value
    /// \return LoggerPtr value which maps to key or newly emplaced value
    static log4cxx::LoggerPtr& at(const std::string & key);
private:
    /// map mapping strings to LoggerPtr
    static std::map<std::string, log4cxx::LoggerPtr> stringLoggerMap;
};
