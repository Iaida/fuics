#pragma once
#include "Tracker.h"

#include <vector>
#include <map>
#include <deque>
#include <algorithm>

/// struct containing multiple trackers
struct TrackingData
{
	/// default constructor
	TrackingData() :
            frame(0), timestamp(0.0f) {};
	/// constructor setting members
	/// \param frame unsigned frame counter, not relevant
	/// \param timestamp float timestamp, not relevant
	/// \param trackers vector of Tracker structs
	TrackingData(const unsigned frame, const float timestamp, const std::vector<Tracker>& trackers) :
			frame(frame), timestamp(timestamp), trackers(trackers) {};

	/// constructor averaging a deque of multiple TrackingData structs
	/// \param toAverage deque of multiple TrackingData structs which are to be averaged
	TrackingData(const std::deque<TrackingData> & toAverage)
	{
		// compute average of scalar members frame and timestamp
		TrackingData accumulator;
		for (const TrackingData & td : toAverage)
		{
			accumulator.frame += td.frame;
			accumulator.timestamp += td.timestamp;
		}
		frame = static_cast<unsigned>(accumulator.frame / toAverage.size());
		timestamp = accumulator.timestamp / toAverage.size();

		// compute average of trackers

		std::map<int, std::vector<Tracker>> idTrackersMap;
		// iterate through input container and add elements to vector corresponding to their id
		for (const TrackingData & td : toAverage)
			for (const Tracker & t : td.trackers)
				idTrackersMap[t.id].push_back(t);


		std::map<int, Tracker> idTrackerMap;
		std::transform(
				idTrackersMap.begin(),
				idTrackersMap.end(),
				std::inserter(idTrackerMap, idTrackerMap.begin()),
				[](const std::pair<int, std::vector<Tracker>> & idVectorPair)
				{
					Tracker average = std::accumulate(idVectorPair.second.begin(),
													  idVectorPair.second.end(),
													  Tracker(idVectorPair.first, 1.0f,
															  glm::vec3(0.0f),
															  glm::vec3(0.0f),
															  glm::mat3x3(0.0f))) / idVectorPair.second.size();
					return std::pair<int, Tracker>(idVectorPair.first, average);
				});

		// TODO : change trackers member from vector to map ???
		// map -> vector, sorted by id ascending
		for (auto & idTrackerPair : idTrackerMap)
			trackers.push_back(idTrackerPair.second);
	}

	/// equality operator
	/// \param other TrackingData to be compared against
	/// \return bool result for equality
	bool operator==(const TrackingData& other) const
	{
		return frame == other.frame &&
			   timestamp == other.timestamp &&
			   trackers == other.trackers;
	}



	/// frame counter contained in TrackingStringListener::trackingString
	unsigned frame;
	/// timestamp contained in TrackingStringListener::trackingString
	float timestamp;
	/// trackers contained in TrackingStringListener::trackingString parsed to Tracker structs
	std::vector<Tracker> trackers;
};
