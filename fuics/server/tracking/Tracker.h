#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/mat3x3.hpp>

/// struct containing all values contained in a single tracker of TrackingStringListener::trackingString
class Tracker
{
public:
    /// default constructor
    Tracker();
    /// construcor setting all members
    /// \param id int unique id
    /// \param quality not relevant
    /// \param position 3D position vector
    /// \param orientation 3D orientation vector
    /// \param rotMatrix 3x3 rotation matrix
    Tracker(const int id, const float quality, const glm::vec3& position, const glm::vec3& orientation, const glm::mat3x3 rotMatrix);

    /// unique tracker id
    /// -1 is error value, DTrack starts ids with 0
    int id;
    /// not relevant, refer to DTrack manual for usages
    float quality;
    /// 3D position vector
    glm::vec3 position;
    /// 3D orientation vector around x,y and z-axes represented as degrees
    glm::vec3 orientation;
    /// 3x3 rotation matrix
    glm::mat3x3 rotMatrix;

    /// checks if Tracker in right half of cave
    /// \return true if in right half of cave, else false
    bool rightHalf() const;
    /// checks if Tracker in bottom quarter of cave
    /// \return true if in bottom quarter of cave, else false
    bool bottomQuarter() const;
    /// checks if two Tracker structs have the same id
    /// \param other
    /// \return true if ids are the same, else false
    bool sameID(const Tracker& other) const;
    /// calculates the difference between two Tracker structs,
    /// not a normal subtraction because of rotMatrix
    /// difference between two identical matrices results in the identity matrix
    /// \param other Tracker to be 'subtracted'
    /// \return resulting difference between two Tracker structs
    Tracker difference(const Tracker& other) const;
    /// calculates a transformation matrix from position and rotMatrix
    /// \return resulting 4x4 transformation matrix
    glm::mat4x4 calcTransformationMatrixFromRotationMatrix() const;
    /// calculates a transformation matrix from position and orientation
    /// \return resulting 4x4 transformation matrix
    glm::mat4x4 calcTransformationMatrixFromOrientationVector() const;

    /// equality operator
    /// \param other Tracker to be compared against
    /// \return bool result for equality
    bool operator==(const Tracker& other) const;
    /// addition operator, only usable for Tracker structs with same id
    /// \param other Tracker to be added
    /// \return resulting Tracker struct
    Tracker operator+(const Tracker& other) const;
    /// addition assignment operator, only usable for Tracker structs with same id
    /// \param other Tracker to be added
    /// \return resulting Tracker struct
    Tracker& operator+=(const Tracker& other);
    /// division operator, only used for averaging
    /// \param div value by which the members are to be divided by
    /// \return resulting Tracker struct
    Tracker operator/(const unsigned div) const;
    /// division assignment operator, only used for averaging
    /// \param div value by which the members are to be divided by
    /// \return resulting Tracker struct
    Tracker operator/=(const unsigned div);
};
