#include "TrackingStringListener.h"
#include "../logger/Logger.h"


TrackingStringListener::TrackingStringListener()
    : io_service(),
      socket(io_service)
{
    LOG4CXX_INFO(Logger::at("TrackingStringListener"), "Initializing Tracking Listener.");

    boost::asio::ip::udp::endpoint receiver_endpoint(udpAddress, 10000);
    LOG4CXX_DEBUG(Logger::at("TrackingStringListener"), "Endpoint created.");

    try
    {
        LOG4CXX_DEBUG(Logger::at("TrackingStringListener"), "Opening UDP websocket on " + udpAddress.to_string() + " on port 10000");

        boost::system::error_code ec;
        socket.open(receiver_endpoint.protocol(), ec);
        socket.bind(receiver_endpoint);

        if (ec)
        {
            LOG4CXX_ERROR(Logger::at("TrackingStringListener"), ec.message());
        }
        else
        {
            LOG4CXX_DEBUG(Logger::at("TrackingStringListener"), "UDP Websocket opened");
        }
    }
    catch (std::exception e)
    {
        LOG4CXX_ERROR(Logger::at("TrackingStringListener"), e.what());
    }


    LOG4CXX_DEBUG(Logger::at("TrackingStringListener"), "starting constant update of tracking string");

    // initialize endless thread with lambda
    trackerThread = std::thread(
            [this]()
            {
                while (true)
                {
                    updateTrackingString();
                    // ca 60 Hz like DTrack
                    std::this_thread::sleep_for(std::chrono::milliseconds(16));
                }
            }
    );
}

void TrackingStringListener::updateTrackingString()
{
    boost::asio::ip::udp::endpoint sender_endpoint;
    size_t len = socket.receive_from(boost::asio::buffer(udpResponseHelpBuffer), sender_endpoint);
    //  Data has max. RESPONSE_BUFFER_LENGTH characters, we only want len characters. The rest is uninitialized.
    trackingString = std::string(udpResponseHelpBuffer.data(), len);
}

const std::string & TrackingStringListener::getTrackingString()
{
    return trackingString;
}
