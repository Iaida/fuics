#pragma once

#include "TrackingData.h"

namespace TrackingStringParsing
{
    /// converts a raw string from DTrack to TrackingData struct
    /// \param dataString input string to be parsed
    /// \return parsed TrackingData struct
    TrackingData parse(const std::string& dataString);
    /// converts a string containing values separated by spaces to Tracker struct
    /// \param trackerString input string containing values separated by spaces
    /// \return parsed Tracker struct
    Tracker trackerStringToTracker(const std::string & trackerString);
    /// converts the tracker line contained in raw DTrack string to a vector of Tracker structs
    /// \param dataString input string to be parsed
    /// \param trackedBodies number of trackers to be parsed
    /// \return resulting vector of parsed Tracker structs
    std::vector<Tracker> parseTrackerLine(const std::string &dataString, const size_t trackedBodies);

    /// delimiter between lines,
    /// Carriage Return followed by Line Feed
    /// http://donsnotes.com/tech/charsets/ascii.html#EOL
    const std::string LINE_SEPARATOR = std::string("\x0D\x0A");
    /// space character between components of a line
    const char DATA_SEPARATOR = ' ';
}
