#include "TrackingDataManager.h"
#include "../logger/Logger.h"
#include "TrackingStringParsing.h"


const TrackingData &TrackingDataManager::getTrackingData() const
{
    return trackingData;
}

void TrackingDataManager::update()
{
    std::string trackingString = trackingStringListener.getTrackingString();
    LOG4CXX_TRACE(Logger::at("TrackingDataManager"), "tracking string: " + trackingString);

    // only parse and update when trackingString not empty
    if (trackingString.size() > 0)
    {
        TrackingData newestTrackingData = TrackingStringParsing::parse(trackingString);
        // add to history
        trackingDataHistory.push_back(newestTrackingData);
        // check if max elements reached
        if (trackingDataHistory.size() > maxHistoryElements)
            trackingDataHistory.pop_front();
        // calculate average of history
        trackingData = TrackingData(trackingDataHistory);

        LOG4CXX_TRACE(Logger::at("TrackingDataManager"), "tracking history constructor finished ");
    }
}

TrackingDataManager::TrackingDataManager()
{
    // std::stof() is dependent on the locale of the computer
    // without calling at least once, parsing strings as float results in truncated values after decimal-point
    // https://www.reddit.com/r/cpp/comments/2e68nd/stdstod_is_locale_dependant_but_the_docs_does_not/
    // C uses "." as decimal-point separator
    std::setlocale(LC_ALL, "C");
}



