#pragma once
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <thread>

/// class which constantly listens for a new tracking string from DTrack in a separate thread
class TrackingStringListener
{
public:
    /// default constructor that initializes members, opens the socket and
    /// starts a separate thread which constantly calls updateTrackingString()
    TrackingStringListener();
    /// getter
    /// \return string that can be interpreted as tracking data.
    const std::string & getTrackingString();
private :
    /// number of characters that fit inside the buffer
    static const int RESPONSE_BUFFER_LENGTH = 2048;
    /// The boost IO service. Necessary to receive input via UDP
    boost::asio::io_service io_service;
    /// The opened websocket which listens to configured port and address.
    boost::asio::ip::udp::socket socket;
    /// The address to which the socket will listen.
    const boost::asio::ip::address_v4 udpAddress = boost::asio::ip::address_v4::any();
    /// Buffer that is filled with response data. This is read each time updateTrackingString() is called.
    boost::array<char, RESPONSE_BUFFER_LENGTH> udpResponseHelpBuffer;
    /// separate thread which constantly calls updateTrackingString()
    std::thread trackerThread;
    /// holds the string from the last call to updateTrackingString()
    std::string trackingString;
    /// Requests a current tracking string from this listener and blocks when no new data is received
    void updateTrackingString();
};
