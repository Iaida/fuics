#pragma once
#include "TrackingData.h"
#include "TrackingStringListener.h"

/// manages tracking string parsing and averaging TrackingData over multiple iterations
class TrackingDataManager
{
public:
	/// default constructor which sets the locale for correct string parsing
	TrackingDataManager();
	/// getter
	/// \return TrackingData struct which was averaged over the last maxHistoryElements iterations
	const TrackingData &getTrackingData() const;
	/// gets the newest string from trackingStringListener, parses it into TrackingData,
	/// updates the trackingDataHistory queue, averages the queue
	/// and saves the result in trackingData
	void update();

private:
	/// constantly listens for a new tracking string from DTrack in a separate thread
	TrackingStringListener trackingStringListener;
	/// holds the result of the last call to averageTrackingData
	TrackingData trackingData;
	/// the history deque of size maxHistoryElements, which is treated as a queue
	std::deque<TrackingData> trackingDataHistory;
	/// maximum number of elements contained in trackingDataHistory
	const size_t maxHistoryElements = 5;
};
