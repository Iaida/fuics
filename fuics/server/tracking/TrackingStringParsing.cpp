#include "TrackingStringParsing.h"
#include "../util/StringUtils.h"
#include "../logger/Logger.h"

namespace TrackingStringParsing
{

    Tracker trackerStringToTracker(const std::string & trackerString)
    {
        std::vector<std::string> splitResult = StringUtils::split(trackerString, ' ');

        // Each data batch for a tracked body has a total of 17 values, where
        // 0 = id, starting with 0
        // 1 = quality value
        // 2, 3, 4 = position
        // 5, 6, 7 = orientation angles
        // 8, 9, 10, 11, 12, 13, 14, 15, 16 = rotation matrix of the bodies rotation

        // TODO : maybe use glm/gtc/type_ptr.hpp to build vector/matrix from plain array or std container,
        // for example glm::mat3x3 mat = glm::make_mat3x3(matvec.data());
        // -> need to parse strings first

        Tracker result;

        result.id = std::stoi(splitResult[0]);
        result.quality = std::stof(splitResult[1]);

        result.position = glm::vec3(
                std::stof(splitResult[2]),
                std::stof(splitResult[3]),
                std::stof(splitResult[4]));

        result.orientation = glm::vec3(
                std::stof(splitResult[5]),
                std::stof(splitResult[6]),
                std::stof(splitResult[7]));

        result.rotMatrix = glm::mat3x3(
                std::stof(splitResult[8]),
                std::stof(splitResult[9]),
                std::stof(splitResult[10]),
                std::stof(splitResult[11]),
                std::stof(splitResult[12]),
                std::stof(splitResult[13]),
                std::stof(splitResult[14]),
                std::stof(splitResult[15]),
                std::stof(splitResult[16]));

        return result;
    }

    std::vector<Tracker> parseTrackerLine(const std::string &dataString, const size_t trackedBodies)
    {
        // TODO : performance improvements
        std::vector<Tracker> result(trackedBodies);
        // check if trackers present
        if (trackedBodies == 0)
            return result;

        // remove indicator and number of bodies
        std::string workingCopy = dataString.substr(5);
        // insert split indicator 'S' between bodies
        workingCopy = StringUtils::replaceAll(workingCopy, "] [", "]S[");
        // remove brackets
        workingCopy = StringUtils::removeAll(workingCopy, '[');
        workingCopy = StringUtils::replaceAll(workingCopy, ']', ' ');

        std::vector<std::string> trackerStrings = StringUtils::split(workingCopy, 'S');

        std::transform(trackerStrings.begin(),
                       trackerStrings.end(),
                       result.begin(),
                       [](const std::string & trackerString)
                       {
                           return trackerStringToTracker(trackerString);
                       });
        return result;
    }


    TrackingData parse(const std::string& dataString)
    {
        try
        {
            TrackingData result;
            std::vector<std::string> lines = StringUtils::split(dataString, LINE_SEPARATOR);

            for (const std::string & line : lines)
            {
                // check if empty
                if (line.size() > 0)
                {
                    std::vector<std::string> splitLine = StringUtils::split(line, DATA_SEPARATOR);

                    // line contains frame count.
                    // First element is "fr", second element is the frame count represented as an integer
                    if (splitLine[0] == "fr")
                    {
                        result.frame = static_cast<unsigned>(std::stoi(splitLine[1]));
                    }
                        // line contains a timestamp.
                        // First element is "ts", second element is the timestamp represented as a float
                    else if (splitLine[0] == "ts")
                    {
                        result.timestamp = std::stof(splitLine[1]);
                    }
                        // This line contains multiple arrays of tracking data.
                        // First element is "6d" followed by the number of tracked bodies
                    else if (splitLine[0] == "6d")
                    {
                        result.trackers = parseTrackerLine(line, static_cast<size_t>(std::stoi(splitLine[1])));
                    }
                }
            }
            return result;
        }
        catch (std::exception e)
        {
            LOG4CXX_ERROR(Logger::at("TrackingStringParsing"), "failed to parse tracking data string")
            return TrackingData();
        }
    }
}