#include "Tracker.h"
#include "../physics/CaveDimensions.h"

#include <glm/gtx/quaternion.hpp>
#include <stdexcept>

Tracker::Tracker()
        : id(-1), quality(1.0f)
{
}

Tracker::Tracker(const int id, const float quality,
                 const glm::vec3 &position,
                 const glm::vec3 &orientation,
                 const glm::mat3x3 rotMatrix)
        : id(id), quality(quality),
          position(position),
          orientation(orientation),
          rotMatrix(rotMatrix)
{
}

bool Tracker::operator==(const Tracker &other) const
{
    return id == other.id &&
           quality == other.quality &&
           position == other.position &&
           orientation == other.orientation &&
           rotMatrix == other.rotMatrix;
}

Tracker Tracker::difference(const Tracker &other) const
{
    if (sameID(other))
    {
        auto rotationMatrixDifference = [](const glm::mat3x3 a, const glm::mat3x3 b) -> glm::mat3x3
        {
            // 2x mat3x3 -> 2x quat -> quat difference by multiplying inverse -> 1x quat -> 1x mat3x3
            return glm::toMat3(glm::quat(a) * glm::inverse(glm::quat(b)));
        };

        return Tracker(
                id, quality,
                position - other.position,
                orientation - other.orientation,
                rotationMatrixDifference(rotMatrix, other.rotMatrix));
    }
    else
        throw std::runtime_error("attempted difference between trackers with different ids");
}

Tracker Tracker::operator/(const unsigned div) const
{
    return Tracker(
            id, quality,
            position / static_cast<float>(div),
            orientation / static_cast<float>(div),
            rotMatrix / static_cast<float>(div));
}

glm::mat4x4 Tracker::calcTransformationMatrixFromRotationMatrix() const
{
    // use constructor to initialize upper left 3x3 part of 4x4 matrix
    glm::mat4x4 matrix = glm::mat4x4(rotMatrix);

    // assign last column, translation part of matrix
    matrix[3] = glm::vec4(position, 1.0f);

    return matrix;
}

bool Tracker::rightHalf() const
{
    return position.x >= CaveDimensions::midXCoordinate;
}

bool Tracker::bottomQuarter() const
{
    return position.z <= CaveDimensions::minZCoordinate + (0.25f * CaveDimensions::edgeLength);
}

bool Tracker::sameID(const Tracker &other) const
{
    return id == other.id;
}

Tracker Tracker::operator+(const Tracker &other) const 
{
    if (sameID(other))
    {
        return Tracker(
                id, quality,
                position + other.position,
                orientation + other.orientation,
                rotMatrix + other.rotMatrix);
    }
    else
        throw std::runtime_error("attempted addition of trackers with different ids");
}

Tracker &Tracker::operator+=(const Tracker &other)
{
    if (sameID(other))
    {
        position += other.position;
        orientation += other.orientation;
        rotMatrix += other.rotMatrix;
        return *this;
    }
    else
        throw std::runtime_error("attempted addition of trackers with different ids");

}

Tracker Tracker::operator/=(const unsigned div)
{
    position /= static_cast<float>(div);
    orientation /= static_cast<float>(div);
    rotMatrix /= static_cast<float>(div);
    return *this;
}

glm::mat4x4 Tracker::calcTransformationMatrixFromOrientationVector() const
{
    // returns identity if orientation is zero-vector
    // vec3 in degrees -> vec3 in radians -> quaternion -> 4x4 matrix
    glm::mat4x4 matrix = glm::toMat4(glm::quat(glm::radians(orientation)));

    // assign last column, translation part of matrix
    matrix[3] = glm::vec4(position, 1.0f);

    return matrix;
}

