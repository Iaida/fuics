#pragma once
#include <algorithm>

namespace AlgorithmUtils
{
    // TODO : const Container input, work on copy and return instead ???

    /// removes elements from Container c for which the UnaryPredicate p holds true
    /// and returns a boolean indicating if an element was removed
    /// based on: <a href="https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom">Wikipedia</a>
    /// \tparam Container std type that has begin and end functions
    /// \tparam UnaryPredicate lambda function that takes an element of the container and returns a boolean value
    /// \param c the Container to remove elements from
    /// \param p the UnaryPredicate lambda function that returns true for elements which should be removed from c
    /// \return returns true if at least 1 element was erased, else returns false
    template<typename Container, typename UnaryPredicate>
    inline bool removeElementsIfToFound(Container &c, UnaryPredicate p)
    {


        // iterator to first element to be removed
        // moves elements so that following statements are true:
        // elements between first and the resulting iterator are not to be removed, rest are to be removed
        auto removeResult = std::remove_if(c.begin(), c.end(), p);
        // check if at least one element to be removed was found
        bool found = removeResult != c.end();
        // erase trailing elements which are to be removed from the container
        c.erase(removeResult, c.end());
        return found;
    };

    /// removes elements from Container c for which the UnaryPredicate p holds true
    /// and returns a boolean indicating if an element was removed
    /// <a href="https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom">Wikipedia</a>
    /// \tparam Container std type that has begin and end functions
    /// \tparam UnaryPredicate lambda function that takes an element of the container and returns a boolean value
    /// \param c the Container to remove elements from
    /// \param p the UnaryPredicate lambda function that returns true for elements which should be removed from c
    template<typename Container, typename UnaryPredicate>
    inline void removeElementsIf(Container &c, UnaryPredicate p)
    {
        c.erase(std::remove_if(c.begin(), c.end(), p), c.end());
    };
}
