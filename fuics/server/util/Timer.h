#pragma once

#include <chrono>

/// simple class which can be used to execute a piece of code every x seconds
class Timer
{
public:
    /// constructor which sets intervalSec and initializes other members with default values
    /// \param intervalSec time interval after which check() returns true once
    Timer(const double intervalSec)
            : timeCounter(0.0),
              oldTime(std::chrono::high_resolution_clock::now()),
              intervalSec(intervalSec)
    {}

    /// checks if the time set by intervalSec has passed since the last call of check or object construction
    /// \return returns bool indicating if the time set by intervalSec has passed
    bool check()
    {
        std::chrono::high_resolution_clock::time_point currTime = std::chrono::high_resolution_clock::now();
        double deltaTime = std::chrono::duration_cast<std::chrono::duration<double>>(currTime - oldTime).count();
        oldTime = currTime; // save time
        timeCounter += deltaTime;

        if (timeCounter > intervalSec)
        {
            timeCounter -= intervalSec;
            return true;
        }
        else
            return false;
    }

    /// compile time double calculation for 1/60th of a second
    static constexpr double hz60 = std::chrono::duration<double>(std::chrono::duration<double, std::ratio<1, 60>>(1.0)).count();
    /// compile time double calculation for 1/120th of a second
    static constexpr double hz120 = std::chrono::duration<double>(std::chrono::duration<double, std::ratio<1, 120>>(1.0)).count();

private:
    /// save time point from last call of check()
    std::chrono::high_resolution_clock::time_point oldTime;
    /// accumulates seconds from last call of check() until it returns true once
    double timeCounter;
    /// the time interval set by the constructor after which check() returns true once
    double intervalSec;
};

