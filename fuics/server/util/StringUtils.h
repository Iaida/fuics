#pragma once
#include <iosfwd>
#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

/// namespace containing string utility functions
namespace StringUtils
{
	/// splits a string into a vector of strings divided by a character delimiter
	/// <a href="http://stackoverflow.com/questions/10058606/c-splitting-a-string-by-a-character/10058725#10058725">Stack Overflow</a>
	/// \param input string to be split
	/// \param delimiter character at which the string should be split
	/// \return vector of strings contained between occurrences of the delimiter
	inline
	std::vector<std::string> split(const std::string& input, char delimiter)
	{
		std::vector<std::string> result;
		std::stringstream ss(input);
		std::string token;

		while(getline(ss, token, delimiter))
			result.push_back(token);

		return result;
	}

	/// splits a string into a vector of strings divided by a string delimiter
	/// based on : <a href="http://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c/14266139#14266139">Stack Overflow</a>
	/// \param input string to be split
	/// \param delimiter string at which the string should be split
	/// \return vector of strings contained between occurrences of the delimiter
	inline
	std::vector<std::string> split(const std::string& input, const std::string& delimiter)
	{
		std::vector<std::string> result;
		std::string workingCopy = input;
		size_t pos = 0;
		std::string token;
		while ((pos = workingCopy.find(delimiter)) != std::string::npos)
		{
			token = workingCopy.substr(0, pos);
			result.push_back(token);
			workingCopy.erase(0, pos + delimiter.length());
		}
        result.push_back(workingCopy);
		return result;
	}

	/// removes any occurrences of a string from another string and returns the result
	/// <a href="http://stackoverflow.com/questions/32435003/how-to-remove-all-substrings-from-a-string/32435076#32435076">Stack Overflow</a>
	/// \param input string to remove pattern from
	/// \param pattern string to remove from input
	/// \return resulting string without any occurrences of pattern
	inline
	std::string removeAll(const std::string& input, const std::string& pattern)
	{
		std::string result = input;
		std::size_t patternLen = pattern.length();
		std::size_t pos = result.find(pattern);
		while (pos != std::string::npos)
		{
			result.erase(pos, patternLen);
			pos = result.find(pattern, pos);
		}
		return result;
	}

	/// removes any occurrences of a character from a string and returns the result
	/// \param input string to remove pattern from
	/// \param character character to remove from input
	/// \return resulting string without any occurrences of pattern
	inline
	std::string removeAll(const std::string& input, const char character)
	{
		std::string result = input;
		result.erase(std::remove_if(result.begin(),
									result.end(),
									[character](const char c)
									{
										return c == character;
									}),
					 result.end());
		return result;
	}

	/// replaces any occurrences of a string with another string inside an input string and returns the result
	/// \param input string to be searched for occurrences of from
	/// \param from string to be replaced
	/// \param to string to replace from
	/// \return resulting string
	inline
	std::string replaceAll(const std::string &input, const std::string& from, const std::string& to)
	{
		std::string result = input;
		size_t start_pos = 0;
		while ((start_pos = result.find(from, start_pos)) != std::string::npos)
		{
			result.replace(start_pos, from.length(), to);
			start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
		}
		return result;
	}

	/// replaces any occurrences of a character with another character inside an input string and returns the result
	/// \param input input string to be searched for occurrences of from
	/// \param from character to be replaced
	/// \param to character to replace from
	/// \return resulting string
	inline
	std::string replaceAll(const std::string &input, const char from, const char to)
	{
		std::string result = input;
		std::replace(result.begin(), result.end(), from, to);
		return result;
	}

	/// converts a c-style array of any type to a string of hexadecimal values, used for debugging/logging
	/// <a href="http://stackoverflow.com/questions/10723403/char-array-to-hex-string-c/10723475#10723475">Stack Overflow</a>
	/// \tparam T type of elements in array
	/// \param array input array to be converted
	/// \param size size of array to be converted
	/// \return string of hexadecimal values
	template<typename T>
	inline
	std::string byteArrayToHexString(T * array, const size_t size)
	{
		std::string string;
		char const hex_chars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		for (size_t i = 0; i < size; ++i )
		{
			char const byte = array[i];

			string += hex_chars[ ( byte & 0xF0 ) >> 4 ];
			string += hex_chars[ ( byte & 0x0F ) >> 0 ];
		}
		return string;
	}
	/// converts a std vector of any type to a string of hexadecimal values, used for debugging/logging
	/// \tparam T type of elements in vector
	/// \param vector input vector to be converted
	/// \return string of hexadecimal values
	template<typename T>
	inline
	std::string byteVectorToHexString(const std::vector<T> & vector)
	{
		return byteArrayToHexString(vector.data(), vector.size());
	}

};

