#include "FpsCounter.h"

FpsCounter::FpsCounter()
        : fps(0),
          timer(1.0),
          frameCounter(0)
{};


unsigned FpsCounter::getFps() const
{
	return fps;
}

void FpsCounter::update()
{
    // check if 1sec passed
    if (timer.check())
    {
        fps = frameCounter;
        frameCounter = 0;
    }
    frameCounter++;
}