#pragma once
#include "../util/Timer.h"

/// simple class to compute frames per second to display on a debug window
class FpsCounter
{
public:
	/// default constructor initializing members
	FpsCounter();
	/// checks the timer and updates fps accordingly
	void update();
	/// getter
	/// \return fps value
	unsigned getFps() const;

private:
	/// timer initialized with 1.0 because fps is updated only every second
	Timer timer;
	/// saves the current fps value
	unsigned fps;
	/// counts the number of frames until reset
    unsigned frameCounter;
};
