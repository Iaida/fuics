#include "Renderer.h"
#include "../util/StringUtils.h"

#include <glm/gtx/string_cast.hpp>



Renderer::Renderer(Display *dpy, GLXDrawable drawable)
        : dpy(dpy),
          drawable(drawable),
          renderTimer(Timer::hz60)
{
    // save window attributes in attr
    XGetWindowAttributes(dpy, drawable, &attr);

    gc = XCreateGC(dpy, drawable, 0, &gcValues);

    // origin in upper left corner

    LOG4CXX_TRACE(Logger::at("Renderer"), "set background is " + std::to_string(gcValues.background));
    LOG4CXX_TRACE(Logger::at("Renderer"), "set foreground is " + std::to_string(gcValues.foreground));

    // select which kind of events are to be reported
    XSelectInput(dpy, drawable, KeyReleaseMask | KeyPressMask);
}



void Renderer::renderRectangle(const Rectangle &rectangle, const unsigned long color) const
{
    setForeground(color);

	std::array<glm::vec3, 4> vertices = rectangle.getVertices();

    // transform 4 vec3 vertices in cave coordinates to 4 XPoint points in screen coordinates
    std::array<XPoint, 5> points;
    std::transform(vertices.begin(),
                   vertices.end(),
                   points.begin(),
                   [&](const glm::vec3 & v)
                   {
                       XPoint p;
                       p.x = static_cast<short>(CaveDimensions::caveToScreenX(v.x, attr.width));
                       p.y = static_cast<short>(CaveDimensions::caveToScreenY(v.y, attr.height));
                       return p;
                   });

    // array needs to repeat first point at the end to connect the last point properly to rectangle
    points[4] = points[0];
    // draw from point to point
    XDrawLines(dpy, drawable, gc, points.data(), 5, CoordModeOrigin);
}


void Renderer::renderText(const std::string &text, int xCo, int yCo, const unsigned long color) const
{
    setForeground(color);

    if (text.length() > INT_MAX)
        throw std::invalid_argument("text in renderText is too long. text length = " + text.length());


    std::vector<std::string> splitStrings = StringUtils::split(text, '\n');
    int yOffset = 0;
    const int deltaOffset = 20;

    for (std::string& s : splitStrings)
    {
        XDrawString(dpy, drawable, gc,
                xCo,
                yCo + yOffset,
                s.c_str(),
                (int) s.length());
        yOffset += deltaOffset;
    }

}


void Renderer::renderCalibrationDimensions(DimensionsWrapper &dimensions, const unsigned long color) const
{
    Foot right(dimensions, true );
    Foot  left(dimensions, false);

    renderFoot(right, color);
    renderFoot(left, color);
}

Renderer::Renderer() : renderTimer(Timer::hz60)
{

}

void Renderer::flush() const
{
    XFlush(dpy);
}

void Renderer::clear() const
{
    XClearWindow(dpy, drawable);
}

void Renderer::setForeground(const unsigned long color) const
{
    XSetForeground(dpy, gc, color);
}

bool Renderer::hasEvents() const
{
    return XQLength(dpy) > 0;
}

XEvent Renderer::nextEvent() const
{
    XEvent event;
    XNextEvent(dpy, &event);
    return event;
}

std::pair<KeySym, std::string> Renderer::xKeyEventToKeySymStringPair(XKeyEvent * xKeyEvent)
{
    char charArray[25];
    KeySym keySym;
    int numBytesInCharArray = XLookupString(xKeyEvent, charArray, 25, &keySym, NULL);
    std::string string(charArray);
    std::string subString = string.substr(0, static_cast<unsigned>(numBytesInCharArray));
    return std::pair<KeySym, std::string>(keySym, subString);
}

void Renderer::renderButton(const Button &button, const std::string &centerString, const unsigned long color) const
{
    renderRectangle(button, color);
    glm::vec3 center = button.getCenter();

    renderText(centerString,
               CaveDimensions::caveToScreenX(center.x, attr.width),
               CaveDimensions::caveToScreenY(center.y, attr.height),
               color);
}

void Renderer::renderFoot(const Foot &foot, const unsigned long color) const
{
    renderFootIndicator(foot, color);
    renderRectangle(foot, color);
}

void Renderer::renderFootIndicator(const Foot &foot, const unsigned long color) const
{
    glm::vec3 indicatorPosition = foot.frontEdgeCenter();

    renderText(foot.right ? "R" : "L",
               CaveDimensions::caveToScreenX(indicatorPosition.x, attr.width),
               CaveDimensions::caveToScreenY(indicatorPosition.y, attr.height),
               color);
};
