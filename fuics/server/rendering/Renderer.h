#pragma once

#include "../physics/Foot.h"
#include "../physics/Button.h"
#include "Color.h"
#include "FpsCounter.h"
#include <GL/glx.h>

/// class
class Renderer
{
public:
    /// default constructor which creates its own window
    Renderer();
    /// Constructs this renderer with a given xorg display and drawable.
    /// \param dpy display
    /// \param drawable window
    Renderer(Display *dpy, GLXDrawable drawable);

    /// render a string to a certain position onto the screen
    /// \param text text to be rendered
    /// \param xCo x coordinate
    /// \param yCo y coordinate
    /// \param color color in which the text should be rendered
    void renderText(const std::string &text, int xCo, int yCo, const unsigned long color = defaultColor) const;
    /// renders a right and left feet rectangle constructed from the dimensions onto the screen
    /// \param dimensions dimensions of the rectangles to be rendered
    /// \param color color in which the dimensions should be rendered
    void renderCalibrationDimensions(DimensionsWrapper &dimensions, const unsigned long color = defaultColor) const;
    /// renders a single button and a string in the center of the button onto the screen
    /// \param button button to be rendered
    /// \param centerString string in the center of the button to be rendered
    /// \param color color of the button and string
    void renderButton(const Button &button, const std::string &centerString, const unsigned long color = defaultColor) const;
    /// renders a single foot and additional indicators onto the screen
    /// \param foot foot to be rendered
    /// \param color color of the foot
    void renderFoot(const Foot &foot, const unsigned long color = defaultColor) const;
    /// flushes the XOrg output buffer
    void flush() const;
    /// clears the entire window area
    void clear() const;
    /// checks if events are present in the event queue
    bool hasEvents() const;
    /// returns a copy of the next event in the event queue and removes it
    XEvent nextEvent() const;
    /// converts a XKeyEvent to a KeySym and readable string
    /// \param xKeyEvent input event to be converted
    /// \return pair of KeySym and string
    static std::pair<KeySym, std::string> xKeyEventToKeySymStringPair(XKeyEvent * xKeyEvent);

private:
    /// sets the foreground color
    /// \param color the new foreground color
    void setForeground(const unsigned long color) const;
    /// renders a rectangle onto the screen and
    /// projects rectangles which are not positioned on the cave floor
    /// \param rectangle the rectangle to be rendered
    /// \param color the color of the rectangle
    void renderRectangle(const Rectangle &rectangle, const unsigned long color = defaultColor) const;
    /// renders an 'R'(right) or 'L'(left) at the front of the specified foot to
    /// let the user see if they put on the trackers correctly
    /// \param foot foot of which the additional indicators should be rendered
    /// \param color color of the indicators
    void renderFootIndicator(const Foot &foot, const unsigned long color = defaultColor) const;

public:
    /// frames per second counter
    FpsCounter fpsCounter;
    /// timer to determine how often the debug window should be rendered
    Timer renderTimer;

private:
    /// display
    Display *dpy;
    /// window
    GLXDrawable drawable;
    /// graphics context necessary for xDraw calls
    GC gc;
    /// window attributes containing information such as window width and height
    XWindowAttributes attr;
    /// graphics context values containing information such as background and foreground color
    XGCValues gcValues;
    /// default color value to allow shorter rendering functions
    static constexpr unsigned long defaultColor = Color::black;
};
