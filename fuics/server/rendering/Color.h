#pragma once

#include "../logger/Logger.h"

/// namespace with color utility functions for XOrg which defines colors as unsigned long
/// as well as a few compile-time computed color values
namespace Color
{
	/// <a href="http://stackoverflow.com/questions/14375156/how-to-convert-a-rgb-color-value-to-an-hexadecimal-value-in-c/14375308#14375308">Stack Overflow</a>


	/// converts three color values between 0 and 255 to an unsigned long
	/// \param r red
	/// \param g green
	/// \param b blue
	/// \return resulting unsigned long color value
    inline
	constexpr unsigned long RGBToUnsignedLong(const int r, const int g, const int b)
	{
		return static_cast<unsigned long>(((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff));
	}
	/// converts three color values between 0 and 1 to an unsigned long
	/// \param r red
	/// \param g green
	/// \param b blue
	/// \return resulting unsigned long color value
    inline
	constexpr unsigned long RGBToUnsignedLong(const float r, const float g, const float b)
	{
		return RGBToUnsignedLong(static_cast<int>(r * 255.0f), static_cast<int>(g * 255.0f),
								 static_cast<int>(b * 255.0f));
	}

	// for testing of constexpr, requires a compile-time constant n as template parameter
	template<unsigned long n>
	inline
	void constN()
	{
		LOG4CXX_DEBUG(Logger::at("Color"), std::to_string(n))
	};


    constexpr unsigned long white = Color::RGBToUnsignedLong(1.0f, 1.0f, 1.0f);
	constexpr unsigned long black = Color::RGBToUnsignedLong(0.0f, 0.0f, 0.0f);
	constexpr unsigned long red = Color::RGBToUnsignedLong(1.0f, 0.0f, 0.0f);
	constexpr unsigned long green = Color::RGBToUnsignedLong(0.0f, 1.0f, 0.0f);
	constexpr unsigned long blue = Color::RGBToUnsignedLong(0.0f, 0.0f, 1.0f);
	constexpr unsigned long cyan = Color::RGBToUnsignedLong(0.0f, 1.0f, 1.0f);
	constexpr unsigned long magenta = Color::RGBToUnsignedLong(1.0f, 0.0f, 1.0f);
	constexpr unsigned long yellow = Color::RGBToUnsignedLong(1.0f, 1.0f, 0.0f);
	constexpr unsigned long darkerGreen = Color::RGBToUnsignedLong(0.2f, 0.8f, 0.1f);
}
