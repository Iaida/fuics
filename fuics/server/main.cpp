#include "CoreWindow.h"

/// Program entry point, starts logger which loads config file and Core with or without debug rendering,
/// no additional parameter -> no debug rendering,
/// additional parameter "w" -> debug rendering window
/// \param argc number of arguments passed to the program, at least 1 with program name
/// \param argv arguments as an array of c-strings
int main(int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    static Logger logger;

    // check for additional parameter "w"
    if (argc == 2 && std::string(argv[1]) == "w")
    {
        CoreWindow core;
        core.run();
    }
    else
    {
        Core core;
        core.run();
    }
}

