package client.exception;

import client.util.StringUtils;
import org.apache.log4j.Logger;

import javax.swing.*;


/**
 * @author Niklas
 */
public class ClientExceptionHandler implements Thread.UncaughtExceptionHandler{

    private static Logger LOG = Logger.getLogger(ClientExceptionHandler.class);

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        LOG.error("Uncaught exception in thread " + t, e);
        JOptionPane.showMessageDialog(
                null,
                StringUtils.formatStackTrace(e.getStackTrace()),
                e.toString(),
                JOptionPane.ERROR_MESSAGE);
    }
}
