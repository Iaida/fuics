package client.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author Niklas
 */
public class AbstractTCPConnection {
    /**
     * The socket on which this connection listens.
     */
    protected Socket socket;

    /**
     * Input stream attached to the socket
     */
    protected InputStream inStreamFromSocket;

    /**
     * Output stream attached to the socket.
     */
    protected OutputStream outStreamFromSocket;

    protected String hostname;
    protected Integer port;

    public void connect(String hostname, Integer port) throws IOException {
        this.hostname = hostname;
        this.port = port;
        socket = new Socket(hostname, port);
        inStreamFromSocket = socket.getInputStream();
        outStreamFromSocket = socket.getOutputStream();
    }

}
