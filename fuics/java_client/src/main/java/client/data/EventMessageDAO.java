package client.data;

import client.model.EventMessageOuterClass.EventMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nt
 */
public class EventMessageDAO extends AbstractTCPConnection {


    public void sendMessage(EventMessage msg) throws IOException {
        if(outStreamFromSocket == null ) {
            throw new RuntimeException("Outstream connect to socket is not connected or null.");
        }
        msg.writeTo(outStreamFromSocket);
        outStreamFromSocket.flush();
    }

    public List<EventMessage> bufferFromStream() throws IOException {
        ArrayList<EventMessage> messageList = new ArrayList<>();
        while(inStreamFromSocket.available() > 0) {
            EventMessage message = EventMessage.parseDelimitedFrom(inStreamFromSocket);
            messageList.add(message);
        }
        return messageList;
    }
}
