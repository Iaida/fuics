package client.data;

import client.model.ConfigurationMessageOuterClass.ConfigurationMessage;
import client.model.configuration.ConfigurationMessageAdapter;
import client.util.NumberUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static client.FUICSConstants.COFIGURATION_MSG_HEADER_SIZE;


public class ConfigurationMessageDAO extends AbstractTCPConnection{

    private static Logger LOG = Logger.getLogger(ConfigurationMessageDAO.class);
	public ConfigurationMessageDAO() {

	}



    public void sendMessage(ConfigurationMessageAdapter msg) throws IOException {
		if(outStreamFromSocket == null ) {
			throw new RuntimeException("Outstream connect to socket is not connected or null.");
		}
		ConfigurationMessage original = msg.toConfigurationMessage();
		byte[] headerBytes = NumberUtils.intToLittleEndianByteArray(original.getSerializedSize());
		byte[] msgBytes = original.toByteArray();

        byte[] combined = Arrays.copyOf(headerBytes, headerBytes.length + msgBytes.length);
        System.arraycopy(msgBytes, 0, combined, headerBytes.length, msgBytes.length);

		outStreamFromSocket.write(combined);
		outStreamFromSocket.flush();
	}




    private ConfigurationMessageAdapter readConfigurationMessageFromStream(InputStream inStream, int msgSize) throws IOException {
	    int bytesRead = 0;
	    byte messageBytes[] = new byte[msgSize];
	    while(inStream.available() > 0 && bytesRead < msgSize) {
	        messageBytes[bytesRead] = (byte)inStream.read();
            bytesRead++;
        }
        return new ConfigurationMessageAdapter(ConfigurationMessage.parseFrom(messageBytes));
    }

    private int readHeader(InputStream inStream) throws IOException {
        int bytesRead = 0;
        byte headerBytes[] = new byte[COFIGURATION_MSG_HEADER_SIZE];
	    while(inStream.available() > 0 && bytesRead < COFIGURATION_MSG_HEADER_SIZE ) {
            headerBytes[bytesRead] = (byte) inStream.read();
            bytesRead++;
        }
        return NumberUtils.byteArrayToLeInt(headerBytes);
    }

	public List<ConfigurationMessageAdapter> bufferFromStream() throws IOException {
	    ArrayList<ConfigurationMessageAdapter> messageList = new ArrayList<>();
	    LOG.debug("In stream available bytes: " + inStreamFromSocket.available());
	    while(inStreamFromSocket.available() > 0) {
	        int size = readHeader(inStreamFromSocket);
	        messageList.add(readConfigurationMessageFromStream(inStreamFromSocket, size));
        }
		return messageList;
	}
}
