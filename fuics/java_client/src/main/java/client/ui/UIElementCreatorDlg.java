package client.ui;
import client.model.configuration.DimensionsAdapter;
import client.model.configuration.ListenerAdapter;
import client.model.configuration.UIElementAdapter;
import client.model.configuration.enumadapters.ElementTypeAdapter;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.List;

public class UIElementCreatorDlg extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel panel1;
    private JComboBox<ElementTypeAdapter> cbxElementType;
    private JTextField txtID;
    private JTextField txtDimX;
    private JButton btnAddListener;
    private JButton btnRemoveListener;

    private JTextField txtDimY;
    private JTextField txtDimZ;
    private JTextField txtDimW;
    private JTextField txtDimH;

    /**
     * List of new Listeners created
     */
    private DefaultListModel<ListenerAdapter> listModelListeners;
    private JList<ListenerAdapter> lstListeners;

    /**
     * The current UIElementAdapter which is activly edited by this dialog.
     */
    private final UIElementAdapter uiElementAdapter;

    private final ActionListener lstnAddListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ListenerAdapter listenerAdapter = new ListenerAdapter();
            UIListenerCreationDialog dlg = new UIListenerCreationDialog(listenerAdapter);
            dlg.pack();
            dlg.setVisible(true);
            listModelListeners.addElement(listenerAdapter);
        }
    };

    private final ActionListener lstnRemoveListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            listModelListeners.remove(lstListeners.getSelectedIndex());
        }
    };

    private void initializeComponents() {
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        btnAddListener.addActionListener(lstnAddListener);
        btnRemoveListener.addActionListener(lstnRemoveListener);
        lstListeners.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        lstListeners.setLayoutOrientation(JList.VERTICAL);
        lstListeners.setVisibleRowCount(-1);
        listModelListeners = new DefaultListModel<>();
        lstListeners.setModel(listModelListeners);

        for (ElementTypeAdapter value : ElementTypeAdapter.values()) {
            cbxElementType.addItem(value);
        }

    }

    public UIElementCreatorDlg(final UIElementAdapter uiElementAdapter) {
        this.uiElementAdapter = uiElementAdapter;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        initializeComponents();
        // Show the dialog
        // Code added after these lines will not get executed before the window closes
        pack();
        setVisible(true);
    }

    private void composeUIElement() {
        Integer id = Integer.parseInt(txtID.getText());
        Double dimX = Double.parseDouble(txtDimX.getText());
        Double dimY = Double.parseDouble(txtDimY.getText());
        Double dimZ = Double.parseDouble(txtDimZ.getText());
        Double dimH = Double.parseDouble(txtDimH.getText());
        Double dimW = Double.parseDouble(txtDimW.getText());
        List<Object> listeners = Arrays.asList(listModelListeners.toArray());
        uiElementAdapter.setId(id);
        for (Object o : listModelListeners.toArray()) {
            ListenerAdapter listener = (ListenerAdapter) o;
            uiElementAdapter.addListeners(listener);
        }
        //Coordinates.Builder cBuilder = Coordinates.newBuilder();
        DimensionsAdapter coordinatesAdapter = new DimensionsAdapter();
        coordinatesAdapter.setHeight(dimH);
        coordinatesAdapter.setWidth(dimW);
        coordinatesAdapter.setDimX(dimX);
        coordinatesAdapter.setDimY(dimY);
        coordinatesAdapter.setDimZ(dimZ);
        uiElementAdapter.setCoordinates(coordinatesAdapter);
    }

    private void onOK() {
        composeUIElement();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel2.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        panel2.add(panel3, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        buttonOK = new JButton();
        buttonOK.setText("OK");
        panel3.add(buttonOK, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonCancel = new JButton();
        buttonCancel.setText("Cancel");
        panel3.add(buttonCancel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel1 = new JPanel();
        panel1.setLayout(new FormLayout("fill:20dlu:noGrow,left:4dlu:noGrow,fill:100dlu:noGrow,left:4dlu:noGrow,fill:100dlu:noGrow,left:4dlu:noGrow,fill:20dlu:noGrow", "center:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:100dlu:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        panel4.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        CellConstraints cc = new CellConstraints();
        panel1.add(spacer2, cc.xy(5, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
        final JLabel label1 = new JLabel();
        label1.setText("Create UI Element");
        panel1.add(label1, cc.xy(5, 3));
        final Spacer spacer3 = new Spacer();
        panel1.add(spacer3, cc.xy(3, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        final Spacer spacer4 = new Spacer();
        panel1.add(spacer4, cc.xy(7, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        cbxElementType = new JComboBox();
        panel1.add(cbxElementType, cc.xy(5, 5));
        final JLabel label2 = new JLabel();
        label2.setText("Type");
        panel1.add(label2, cc.xy(3, 5));
        final JLabel label3 = new JLabel();
        label3.setText("ID");
        panel1.add(label3, cc.xy(3, 7));
        final Spacer spacer5 = new Spacer();
        panel1.add(spacer5, cc.xy(1, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        txtID = new JTextField();
        panel1.add(txtID, cc.xy(5, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label4 = new JLabel();
        label4.setText("dim_x");
        panel1.add(label4, cc.xy(3, 9));
        txtDimX = new JTextField();
        panel1.add(txtDimX, cc.xy(5, 9, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label5 = new JLabel();
        label5.setText("dim_y");
        panel1.add(label5, cc.xy(3, 11));
        txtDimY = new JTextField();
        panel1.add(txtDimY, cc.xy(5, 11, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label6 = new JLabel();
        label6.setText("dim_z");
        panel1.add(label6, cc.xy(3, 13));
        txtDimZ = new JTextField();
        panel1.add(txtDimZ, cc.xy(5, 13, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label7 = new JLabel();
        label7.setText("dim_w");
        panel1.add(label7, cc.xy(3, 15));
        txtDimW = new JTextField();
        panel1.add(txtDimW, cc.xy(5, 15, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label8 = new JLabel();
        label8.setText("dim_h");
        panel1.add(label8, cc.xy(3, 17));
        txtDimH = new JTextField();
        panel1.add(txtDimH, cc.xy(5, 17, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label9 = new JLabel();
        label9.setText("Listeners");
        panel1.add(label9, cc.xy(3, 19));
        btnAddListener = new JButton();
        btnAddListener.setText("Add");
        panel1.add(btnAddListener, cc.xy(3, 23));
        btnRemoveListener = new JButton();
        btnRemoveListener.setText("Remove");
        panel1.add(btnRemoveListener, cc.xy(5, 23));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel1.add(scrollPane1, cc.xyw(3, 21, 3, CellConstraints.FILL, CellConstraints.FILL));
        lstListeners = new JList();
        final DefaultListModel defaultListModel1 = new DefaultListModel();
        lstListeners.setModel(defaultListModel1);
        scrollPane1.setViewportView(lstListeners);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
