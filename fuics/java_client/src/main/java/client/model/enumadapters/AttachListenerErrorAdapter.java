package client.model.enumadapters;

import client.model.EventMessageOuterClass.EventMessage.AttachListenerError;

/**
 * Created by Niklas on 18.02.2017.
 */
public enum AttachListenerErrorAdapter {
    /**
     * <code>NO_ERR = 0;</code>
     */
    NO_ERR(AttachListenerError.NO_ERR, "No Error"),
    /**
     * <code>ERR_ID_NOT_FOUND = 1;</code>
     */
    ERR_ID_NOT_FOUND(AttachListenerError.ERR_ID_NOT_FOUND, "ID not found"),
    /**
     * <code>ERR_INV_EVENT = 2;</code>
     */
    ERR_INV_EVENT(AttachListenerError.ERR_INV_EVENT, "Invalid Event"),
    /**
     * <code>ERR_UNKNOWN = 3;</code>
     */
    ERR_UNKNOWN(AttachListenerError.ERR_UNKNOWN, "Unknown Error");

    private AttachListenerError baseType;
    private String displayString;

    AttachListenerErrorAdapter(AttachListenerError baseType, String displayString) {
        this.displayString = displayString;
        this.baseType = baseType;
    }

    public String getDisplayString() {
        return displayString;
    }

    public AttachListenerError getBaseType() {
        return baseType;
    }

    public static AttachListenerErrorAdapter fromString(String value) {
        for(AttachListenerErrorAdapter type : AttachListenerErrorAdapter.values()) {
            if(type.getDisplayString().equals(value)) {
                return type;
            }
        }
        throw new RuntimeException("Could not extract ConfigurationErrorAdapter enum type from " + value);
    }

}
