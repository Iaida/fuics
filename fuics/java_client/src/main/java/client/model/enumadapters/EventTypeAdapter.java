package client.model.enumadapters;

import client.model.EventMessageOuterClass.EventMessage.EventType;

/**
 * Created by Niklas on 18.02.2017.
 */
public enum EventTypeAdapter {
    /**
     * <code>ON_ENTER = 0;</code>
     */
    ON_ENTER(EventType.ON_ENTER, "On Enter"),
    /**
     * <code>ON_EXIT = 1;</code>
     */
    ON_EXIT(EventType.ON_EXIT, "On Exit"),
    UNRECOGNIZED(EventType.UNRECOGNIZED, "Unrecognized");

    private EventType baseType;
    private String displayString;

    EventTypeAdapter(EventType baseType, String displayString) {
        this.baseType = baseType;
        this.displayString = displayString;
    }

    public static EventTypeAdapter fromString(String value) {
        for(EventTypeAdapter type : EventTypeAdapter.values()) {
            if(type.getDisplayString().equals(value)) {
                return type;
            }
        }
        throw new RuntimeException("Could not extract ConfigurationErrorAdapter enum type from " + value);
    }

    public String getDisplayString() {
        return displayString;
    }

    public EventType getBaseType() {
        return baseType;
    }
}
