package client.model.enumadapters;

import client.model.EventMessageOuterClass.EventMessage.EventMessageType;

/**
 * Created by Niklas on 18.02.2017.
 */
public enum EventMessageTypeAdapter {
    /**
     * <pre>
     * Request listener attach
     * </pre>
     *
     * <code>ATT_LSTN_RQST = 0;</code>
     */
    ATT_LSTN_RQST("Request Listener Attach", EventMessageType.ATT_LSTN_RQST),
    /**
     * <pre>
     * Listener attach response by server
     * </pre>
     *
     * <code>ATT_LSTN_RSPN = 1;</code>
     */
    ATT_LSTN_RSPN("Response Listener Attach", EventMessageType.ATT_LSTN_RSPN),
    /**
     * <pre>
     * Event. Duplex.
     * </pre>
     *
     * <code>EVENT = 2;</code>
     */
    EVENT("Event", EventMessageType.EVENT),
    UNRECOGNIZED("Unrecoginized", EventMessageType.UNRECOGNIZED);

    private String displayString;
    private EventMessageType baseType;



    EventMessageTypeAdapter(String displayString, EventMessageType baseType ) {
        this.displayString = displayString;
        this.baseType = baseType;
    }

    public String getDisplayString() {
        return displayString;
    }

    public EventMessageType getBaseType() {
        return baseType;
    }

    public static EventMessageTypeAdapter fromString(String value) {
        for(EventMessageTypeAdapter type : EventMessageTypeAdapter.values()) {
            if(type.getDisplayString().equals(value)) {
                return type;
            }
        }
        throw new RuntimeException("Could not extract EventMessageTypeAdapter enum value from " + value);
    }
}
