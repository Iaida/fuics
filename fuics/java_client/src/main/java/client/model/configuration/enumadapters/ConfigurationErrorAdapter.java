package client.model.configuration.enumadapters;

import client.model.ConfigurationMessageOuterClass.ConfigurationError;

/**
 * Created by Niklas on 13.02.2017.
 */
public enum ConfigurationErrorAdapter {
    /**
     * <pre>
     * No error
     * </pre>
     *
     * <code>ERR_NOERROR = 0;</code>
     */
    ERR_NOERROR("No Error", ConfigurationError.ERR_NOERROR),

    ERR_ASSIST_ACTIVE("Assist Active", ConfigurationError.ERR_ASSIST_ACTIVE),

    ERR_ASSIST_INACTIVE("Assist Inactive", ConfigurationError.ERR_ASSIST_INACTIVE),

    ERR_UNKNOWN_ID("Unknown ID", ConfigurationError.ERR_UNKNOWN_ID),

    ERR_UNKNOWN_ELEMENT("Unknown Element", ConfigurationError.ERR_UNKNOWN_ELEMENT),
    /**
     * <pre>
     * Unknown error
     * </pre>
     *
     * <code>ERR_UNKNOWN = 1;</code>
     */
    ERR_UNKNOWN("Unknown", ConfigurationError.ERR_UNKNOWN);

    private ConfigurationError originalError;
    private String displayString;

    ConfigurationErrorAdapter(String displayString, ConfigurationError originalError) {
        this.originalError = originalError;
        this.displayString = displayString;
    }

    public String getDisplayString() {
        return displayString;
    }

    public ConfigurationError getConfigurationError() {
        return originalError;
    }

    public static ConfigurationErrorAdapter fromOriginal(ConfigurationError originalError) {
        switch(originalError) {
            case ERR_NOERROR:
                return ERR_NOERROR;
            case ERR_UNKNOWN:
                return ERR_UNKNOWN;
            case ERR_ASSIST_ACTIVE:
                return ERR_ASSIST_ACTIVE;
            case ERR_ASSIST_INACTIVE:
                return ERR_ASSIST_INACTIVE;
            case ERR_UNKNOWN_ID:
                return ERR_UNKNOWN_ID;
            case ERR_UNKNOWN_ELEMENT:
                return ERR_UNKNOWN_ELEMENT;
            case UNRECOGNIZED:
            default:
                throw new RuntimeException(); //FIXME nt
        }
    }

    public static ConfigurationErrorAdapter fromString(String value) {
        for(ConfigurationErrorAdapter type : ConfigurationErrorAdapter.values()) {
            if(type.getDisplayString().equals(value)) {
                return type;
            }
        }
       throw new RuntimeException("Could not extract ConfigurationErrorAdapter enum type from " + value);
    }

}
