package client.model.configuration;

import client.model.ConfigurationMessageOuterClass.UIElement;
import client.model.ConfigurationMessageOuterClass.UIStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Niklas on 25.02.2017.
 */
public class UIStatusAdapter {
    @JsonIgnore
    private UIStatus originalStatus;
    @JsonProperty("canvasWidth")
    private Double canvasWidth;
    @JsonProperty("canvasHeight")
    private Double canvasHeight;
    @JsonProperty("uiElements")
    private List<UIElementAdapter> uiElements;

    public UIStatusAdapter() {
        uiElements = new ArrayList<>();
    }

    public UIStatusAdapter(UIStatus originalStatus) {
        canvasHeight = originalStatus.getCanvasHeight();
        canvasWidth = originalStatus.getCanvasWidth();
        uiElements = new ArrayList<>();
        for(UIElement element : originalStatus.getElementsList()) {
            uiElements.add(new UIElementAdapter(element));
        }
        this.originalStatus = originalStatus;
    }

    public UIStatus toStatus() {
        UIStatus.Builder builder = UIStatus.newBuilder();
        if(canvasHeight != null) {
            builder.setCanvasHeight(canvasHeight);
        }
        if(canvasWidth != null) {
            builder.setCanvasWidth(canvasWidth);
        }
        for(UIElementAdapter element : uiElements) {
            builder.addElements(element.toUIElement());
        }
        return builder.build();
    }

    public Double getCanvasWidth() {
        return canvasWidth;
    }

    public void setCanvasWidth(Double canvasWidth) {
        this.canvasWidth = canvasWidth;
    }

    public Double getCanvasHeight() {
        return canvasHeight;
    }

    public void setCanvasHeight(Double canvasHeight) {
        this.canvasHeight = canvasHeight;
    }
    public void setUiElements(List<UIElementAdapter> uiElements) {
        this.uiElements = uiElements;
    }

    public List<UIElementAdapter> getUiElements() {
        return uiElements;
    }

    public void addElement (UIElementAdapter element) {
        uiElements.add(element);
    }

    public String prettyString() {
        final StringBuilder resBuilder = new StringBuilder();
        String status = MessageFormat.format("UI Status ([{0}/{1}]\n", canvasWidth, canvasHeight);
        resBuilder.append(status);
        resBuilder.append("Elements\n");
        for (UIElementAdapter element : uiElements) {
            resBuilder.append(element.prettyString()).append("\n");
        }
        return resBuilder.toString();
    }
}
