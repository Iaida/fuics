package client.model.configuration;

import client.model.ConfigurationMessageOuterClass.Dimensions;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.MessageFormat;

/**
 * Created by Niklas on 25.02.2017.
 */
public class DimensionsAdapter {
    @JsonIgnore
    private Dimensions originalCoordinates;
    @JsonProperty("height")
    private Double height = 0.0;
    @JsonProperty("width")
    private Double width = 0.0;
    @JsonProperty("dimX")
    private Double dimX = 0.0;
    @JsonProperty("dimY")
    private Double dimY = 0.0;
    @JsonProperty("dimZ")
    private Double dimZ = 0.0;


    public DimensionsAdapter() {

    }

    public DimensionsAdapter(Double height, Double width, Double dimX, Double dimY, Double dimZ) {
        this.height = height;
        this.width = width;
        this.dimX = dimX;
        this.dimY = dimY;
        this.dimZ = dimZ;
    }

    public DimensionsAdapter(Dimensions originalCoordinates) {
        this.originalCoordinates = originalCoordinates;
        height = originalCoordinates.getHeight();
        width = originalCoordinates.getWidth();
        dimX = originalCoordinates.getX();
        dimZ = originalCoordinates.getZ();
        dimY = originalCoordinates.getY();
    }

    public Dimensions toCoordinates() {
        Dimensions.Builder builder = Dimensions.newBuilder();
        if(height != null) {
            builder.setHeight(height);
        }
        if(width != null) {
            builder.setWidth(width);
        }
        if(dimY != null) {
            builder.setY(dimY);
        }
        if(dimZ != null) {
            builder.setZ(dimZ);
        }
        if(dimX != null) {
            builder.setX(dimX);
        }
        return builder.build();
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getDimX() {
        return dimX;
    }

    public void setDimX(Double dimX) {
        this.dimX = dimX;
    }

    public Double getDimY() {
        return dimY;
    }

    public void setDimY(Double dimY) {
        this.dimY = dimY;
    }

    public Double getDimZ() {
        return dimZ;
    }

    public void setDimZ(Double dimZ) {
        this.dimZ = dimZ;
    }

    public String prettyString() {
        return MessageFormat.format("[{0}/{1}/{2}/{3}/{4}]", dimX, dimY, dimZ, height, width);
    }
}
