package client.model.configuration.enumadapters;

import client.model.ConfigurationMessageOuterClass;

/**
 * Created by Niklas on 28.02.2017.
 */
public enum EventTypeAdapter {

    /**
     * <code>NONE = 0;</code>
     */
    NONE(ConfigurationMessageOuterClass.EventType.NONE,"None"),
    /**
     * <code>PRESS = 1;</code>
     */
    PRESS(ConfigurationMessageOuterClass.EventType.PRESS,"Press"),
    /**
     * <code>RELEASE = 2;</code>
     */
    RELEASE(ConfigurationMessageOuterClass.EventType.RELEASE,"Release"),
    /**
     * <code>HOLD = 3;</code>
     */
    HOLD(ConfigurationMessageOuterClass.EventType.HOLD,"Hold");

    private final ConfigurationMessageOuterClass.EventType original;
    private final String displayString;

    EventTypeAdapter(ConfigurationMessageOuterClass.EventType original, String displayString) {
        this.original = original;
        this.displayString = displayString;
    }

    public ConfigurationMessageOuterClass.EventType getOriginal() {
        return original;
    }

    public String getDisplayString() {
        return displayString;
    }

    public static EventTypeAdapter fromOriginal(ConfigurationMessageOuterClass.EventType original) {
        switch(original) {

            case NONE:
                return NONE;
            case PRESS:
                return PRESS;
            case RELEASE:
                return RELEASE;
            case HOLD:
                return HOLD;
            case UNRECOGNIZED:
            default:
                throw new RuntimeException(); //FIXME
        }
    }
}
