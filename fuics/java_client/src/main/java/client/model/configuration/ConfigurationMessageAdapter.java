package client.model.configuration;

import client.model.ConfigurationMessageOuterClass.ConfigurationMessage;
import client.model.configuration.enumadapters.ConfigurationErrorAdapter;
import client.model.configuration.enumadapters.ConfigurationRequestTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Niklas on 25.02.2017.
 */

public class ConfigurationMessageAdapter {
    /**
     * The original Configuration Mesasge
     */
    @JsonIgnore
    private ConfigurationMessage original;

    @JsonProperty("requestType")
    private ConfigurationRequestTypeAdapter requestType = ConfigurationRequestTypeAdapter.REQUEST_ASSIST;

    @JsonProperty("dimensions")
    private DimensionsAdapter dimensions;

    @JsonProperty("configurationError")
    private ConfigurationErrorAdapter configurationError;

    @JsonProperty("uiStatus")
    private UIStatusAdapter uiStatus;

    @JsonProperty("listener")
    private ListenerAdapter listener;

    @JsonProperty("uiElement")
    private UIElementAdapter uiElement;

    public ConfigurationMessageAdapter() {

    }

    public ConfigurationMessageAdapter(ConfigurationMessage original) {
        requestType = ConfigurationRequestTypeAdapter.fromOriginal(original.getRequestType());
        dimensions = new DimensionsAdapter(original.getDimensions());
        configurationError = ConfigurationErrorAdapter.fromOriginal(original.getError());
        uiStatus = new UIStatusAdapter(original.getStatus());
        listener = new ListenerAdapter(original.getListenerInfo());
        uiElement = new UIElementAdapter(original.getElementInfo());
        this.original = original;
    }

    public ConfigurationMessage toConfigurationMessage() {
        ConfigurationMessage.Builder builder = ConfigurationMessage.newBuilder();
        if(requestType != null) {
            builder.setRequestType(requestType.getConfigurationRequestType());
        }
        if(dimensions != null) {
            builder.setDimensions(dimensions.toCoordinates());
        }
        if(configurationError != null) {
            builder.setError(configurationError.getConfigurationError());
        }
        if(uiStatus != null) {
            builder.setStatus(uiStatus.toStatus());
        }
        return builder.build();
    }

    public ConfigurationRequestTypeAdapter getRequestType() {
        return requestType;
    }

    public void setRequestType(ConfigurationRequestTypeAdapter requestType) {
        this.requestType = requestType;
    }

    public DimensionsAdapter getDimensions() {
        return dimensions;
    }

    public void setDimensions(DimensionsAdapter dimensions) {
        this.dimensions = dimensions;
    }

    public ConfigurationErrorAdapter getConfigurationError() {
        return configurationError;
    }

    public void setConfigurationError(ConfigurationErrorAdapter configurationError) {
        this.configurationError = configurationError;
    }

    public UIStatusAdapter getUiStatus() {
        return uiStatus;
    }

    public void setUiStatus(UIStatusAdapter uiStatus) {
        this.uiStatus = uiStatus;
    }

    public void writeTo(OutputStream outStreamFromSocket) throws IOException {
        ConfigurationMessage original = toConfigurationMessage();
        original.writeTo(outStreamFromSocket);

    }

    public String prettyString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Message\n");
        builder.append("Error: ").append(configurationError.getDisplayString()).append("\n");
        builder.append("Type: ").append(requestType.getDisplayString()).append("\n");
        builder.append("Coordinates: ").append(dimensions.prettyString()).append("\n");
        builder.append("Listener: ").append(listener.prettyString()).append("\n");
        builder.append("UIElement: ").append(uiElement.prettyString()).append("\n");
        builder.append(uiStatus.prettyString());
        return builder.toString();
    }
}
