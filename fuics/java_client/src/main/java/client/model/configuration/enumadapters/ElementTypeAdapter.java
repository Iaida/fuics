package client.model.configuration.enumadapters;

import client.model.ConfigurationMessageOuterClass.ElementType;

public enum ElementTypeAdapter {
	
	/**
	 * 
	 */
	 RECTANGLE(ElementType.RECTANGLE, "Rectangle");
	
	private final ElementType original;
	private final String displayString;
	
	ElementTypeAdapter(ElementType original, String displayString) {
		this.original = original;
		this.displayString = displayString;
	}
	
	public ElementType getOriginal() {
		return original;
	}

	public String getDisplayString() {
		return displayString;
	}

	public static ElementTypeAdapter fromOriginal(ElementType original) {
	    switch(original) {
            case RECTANGLE:
                return RECTANGLE;
            case UNRECOGNIZED:
            default:
                throw new RuntimeException();
        }
    }
}
