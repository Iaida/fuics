package client.model.configuration;


import client.model.ConfigurationMessageOuterClass.Listener;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import client.model.configuration.enumadapters.EventTypeAdapter;

import java.text.MessageFormat;


/**
 * Created by Niklas on 25.02.2017.
 */

public class ListenerAdapter {

    @JsonIgnore
    private Listener original;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("info")
    private String info;
    @JsonProperty
    private EventTypeAdapter listensTo;

    /**
     * Default constructor to follow EJB standards and allow (de)serialization
     */
    public ListenerAdapter() {

    }

    public ListenerAdapter(Listener original) {
        this.id = original.getId();
        this.info = original.getInfo();
        this.original = original;
        this.listensTo = EventTypeAdapter.fromOriginal(original.getListensTo());
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Listener toListener() {
        Listener.Builder builder = Listener.newBuilder();
        if(info != null) {
            builder.setInfo(info);
        }
        if(id != null) {
            builder.setId(id);
        }
        return builder.build();
    }

    public String prettyString() {
        return MessageFormat.format("Listener {0} (Info={1})", id, info);
    }
}
