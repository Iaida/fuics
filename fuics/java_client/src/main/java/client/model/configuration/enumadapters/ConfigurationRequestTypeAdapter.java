package client.model.configuration.enumadapters;


import client.model.ConfigurationMessageOuterClass.ConfigurationRequestType;

/**
 * Created by Niklas on 13.02.2017.
 */
public enum ConfigurationRequestTypeAdapter  {
    /**
     * <code>REQUEST_ASSIST = 0;</code>
     */
    REQUEST_ASSIST("Assist", ConfigurationRequestType.ASSIST),
    /**
     * <code>REQUEST_FINALIZE = 1;</code>
     */
    REQUEST_FINALIZE("Finalize", ConfigurationRequestType.FINALIZE),
    /**
     * <code>REQUEST_DEBUG = 2;</code>
     */
    REQUEST_DEBUG("Debug", ConfigurationRequestType.DEBUG),
    /**
     * <code>REQUEST_COORDINATES = 3;</code>
     */
    REQUEST_COORDINATES("Coordinates", ConfigurationRequestType.UPDATE_COORDINATES),
    /**
     * <code>REQUEST_ADD_ELEMENT = 5;</code>
     */
    REQUEST_ADD_ELEMENT("Add Element", ConfigurationRequestType.ADD_ELEMENT),
    /**
     * <code>REQUEST_REMOVE_ELEMENT = 6;</code>
     */
    REQUEST_REMOVE_ELEMENT("Remove Element", ConfigurationRequestType.REMOVE_ELEMENT),
    REQUEST_ADD_LISTENER("Add Listener", ConfigurationRequestType.ADD_LISTENER),
    REQUEST_REMOVE_LISTENER("Remove Listener", ConfigurationRequestType.REMOVE_LISTENER),
    /**
     * <pre>
     * Replaces the UIs current status with the given status.
     * This operation might take time for processing.
     * </pre>
     *
     * <code>REQUEST_REPLACE_STATUS = 7;</code>
     */
    REQUEST_REPLACE_STATUS("Replace Status", ConfigurationRequestType.REPLACE_STATUS);

    private String displayString;
    private ConfigurationRequestType baseType;

    ConfigurationRequestTypeAdapter(String displayString, ConfigurationRequestType baseType) {
        this.displayString = displayString;
        this.baseType = baseType;
    }

    public String getDisplayString() {
        return displayString;
    }

    public ConfigurationRequestType getConfigurationRequestType() {
        return baseType;
    }

    public static ConfigurationRequestTypeAdapter fromOriginal(ConfigurationRequestType original) {
        switch(original) {
            case ASSIST:
                return REQUEST_ASSIST;
            case FINALIZE:
                return REQUEST_FINALIZE;
            case DEBUG:
                return REQUEST_DEBUG;
            case UPDATE_COORDINATES:
                return REQUEST_COORDINATES;
            case ADD_ELEMENT:
                return REQUEST_ADD_ELEMENT;
            case REMOVE_ELEMENT:
                return REQUEST_REMOVE_ELEMENT;
            case REPLACE_STATUS:
                return REQUEST_REMOVE_ELEMENT;
            case ADD_LISTENER:
                return REQUEST_ADD_LISTENER;
            case REMOVE_LISTENER:
                return REQUEST_REMOVE_LISTENER;
            case UNRECOGNIZED:
            default:
                throw new RuntimeException(); //FIXME nt
        }
    }

    public static ConfigurationRequestTypeAdapter fromString(String value) {
        for(ConfigurationRequestTypeAdapter type : ConfigurationRequestTypeAdapter.values()) {
            if(type.getDisplayString().equals(value)) {
                return type;
            }
        }
        throw new RuntimeException("Could not extract ConfigurationRequestTypeAdapter enum value from " + value);
    }



}
