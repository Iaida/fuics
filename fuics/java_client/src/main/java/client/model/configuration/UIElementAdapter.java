package client.model.configuration;

import client.model.ConfigurationMessageOuterClass.Listener;
import client.model.ConfigurationMessageOuterClass.UIElement;
import client.model.configuration.enumadapters.ElementTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Niklas on 25.02.2017.
 */
public class UIElementAdapter {
    @JsonIgnore
    private UIElement originalElement;
    @JsonProperty("coordinates")
    private DimensionsAdapter coordinates;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("listeners")
    private List<ListenerAdapter> listeners;
    @JsonProperty("elementType")
    private ElementTypeAdapter elementType;


    public UIElementAdapter() {
        listeners = new ArrayList<>();
    }

    public UIElementAdapter(UIElement originalElement) {
        coordinates = new DimensionsAdapter(originalElement.getDimensions());
        id = originalElement.getId();
        listeners = new ArrayList<>();
        for(Listener listener : originalElement.getListenersList()) {
            listeners.add(new ListenerAdapter(listener));
        }
        elementType = ElementTypeAdapter.fromOriginal(originalElement.getType());
        this.originalElement = originalElement;
    }

    public UIElement toUIElement() {
        UIElement.Builder builder = UIElement.newBuilder();
        if(coordinates != null) {
            builder.setDimensions(coordinates.toCoordinates());
        }
        if(id != null) {
            builder.setId(id);
        }
        if(elementType != null) {
            builder.setType(elementType.getOriginal());
        }
        for(ListenerAdapter listener : listeners) {
            builder.addListeners(listener.toListener());
        }
        return builder.build();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ListenerAdapter> getListeners() {
        return listeners;
    }

    public void setListeners(List<ListenerAdapter> listeners) {
        this.listeners = listeners;
    }

    public ElementTypeAdapter getElementType() {
        return elementType;
    }

    public void setElementType(ElementTypeAdapter elementType) {
        this.elementType = elementType;
    }

    public DimensionsAdapter getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(DimensionsAdapter coordinates) {
        this.coordinates = coordinates;
    }

    public void addListeners(ListenerAdapter listener) {
        listeners.add(listener);
    }

    public String prettyString() {
        final StringBuilder resBuilder = new StringBuilder();
        resBuilder.append("└── ");
        resBuilder.append("Element ").append(id).append(" ").append(elementType.getDisplayString());
        resBuilder.append(" ").append(coordinates.prettyString()).append("\n");
        for(int i = 0; i < listeners.size(); i++) {
            if (i == listeners.size() - 1) {
                resBuilder.append("     └── ");
            } else {
                resBuilder.append("     ├── ");
            }
            resBuilder.append("Listener[ID/INFO]: ");
            resBuilder.append(listeners.get(i).prettyString());
            if (i != listeners.size() - 1) {
                resBuilder.append("\n");
            }
        }
        return resBuilder.toString();
    }
}
