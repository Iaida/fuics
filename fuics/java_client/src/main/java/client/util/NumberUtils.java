package client.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Niklas on 18.02.2017.
 */
public class NumberUtils {

    public static Double tryParseDouble(String toParse, Double defVal) {
        Double result = defVal;
        if(toParse != null) {
            try {
                result = Double.parseDouble(toParse);
            } catch(NumberFormatException e) {

            }
        }
        return result;
    }

    /**
     * Converts a byte array into a little endian representation of a 32 bit integer.
     * @param b
     * @return
     */
    public static int byteArrayToLeInt(byte[] b) {
        final ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        return bb.getInt();
    }

    public static byte[] intToLittleEndianByteArray(int num) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putInt(num);
        return byteBuffer.array();
    }

    public static Integer tryParseInteger(String toParse, Integer defVal) {
        Integer result = defVal;
        if(toParse != null) {
            try {
                result = Integer.parseInt(toParse);
            } catch(NumberFormatException e) {

            }
        }
        return result;
    }
}
