package client.util;

/**
 * @author Niklas
 */
public class StringUtils {

    public static String formatStackTrace(StackTraceElement[] stackTraceElements) {
        final StringBuilder result = new StringBuilder();
        for(StackTraceElement element : stackTraceElements) {
            result.append(element.toString());
            result.append("\n");
        }
        return result.toString();
    }

    public static String addLineBreaks(String value, int charPerLine) {
        StringBuilder builder = new StringBuilder(value.length());
        for(int i = 0; i < value.length(); i++) {
            builder.append(value.charAt(i));
            if(i % charPerLine == 0) {
                builder.append("\n");
            }
        }
        return builder.toString();
    }
}
