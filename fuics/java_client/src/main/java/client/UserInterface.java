package client;

import client.data.ConfigurationMessageDAO;
import client.data.EventMessageDAO;
import client.exception.ClientExceptionHandler;
import client.model.EventMessageOuterClass.EventMessage;
import client.model.configuration.ConfigurationMessageAdapter;
import client.model.configuration.DimensionsAdapter;
import client.model.configuration.UIElementAdapter;
import client.model.configuration.UIStatusAdapter;
import client.model.enumadapters.AttachListenerErrorAdapter;
import client.model.configuration.enumadapters.ConfigurationErrorAdapter;
import client.model.configuration.enumadapters.ConfigurationRequestTypeAdapter;
import client.model.enumadapters.*;
import client.ui.MessageQueue;
import client.ui.UIElementCreatorDlg;
import client.util.NumberUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.intellij.uiDesigner.core.Spacer;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class UserInterface {
    // Configuration Message components
    private JComboBox<String> cbxRequestType;
    private JComboBox<String> cbxConfigurationError;
    private JTextField txtFieldXPos;
    private JTextField txtFieldYPos;
    private JTextField txtFieldZPos;
    private JTextField txtFieldHeight;
    private JTextField txtFieldWidth;
    private JButton btnSendConfigurationRequest;

    // Event Message UI components
    private JComboBox<String> cbxEventMsgType;
    private JComboBox<String> cbxEventType;
    private JComboBox<String> cbxAttachListenerError;
    private JTextField txtElementID;
    private JTextField txtListenerID;
    private JTextField txtInfoString;


    // Shared Components
    private JTextField txtFieldHostname;
    private JTextField txtFieldPort;
    private JButton btnConnect;


    private JPanel mainPanel;
    private JTabbedPane tabbedPane1;
    private JButton btnSendEventMsg;
    private JLabel lblConfigConnectionState;

    private JButton btnAddUIElement;
    private JButton btnRemoveElement;


    private JTextArea txtAreaJSONOutput;
    private JButton btnToJSON;
    private JButton btnFromJSON;
    /**
     * List and model containing all current ui elements
     */
    private DefaultListModel<UIElementAdapter> uiElementListModel;
    private JList<UIElementAdapter> lstUIElements;

    private ConfigurationMessageDAO configurationMessageDAO = new ConfigurationMessageDAO();
    private EventMessageDAO eventMessageDAO = new EventMessageDAO();

    private ClientExceptionHandler exceptionHandler = new ClientExceptionHandler();

    private final static Logger LOG = Logger.getLogger(UserInterface.class);

    private List<String> commonlyUsedHosts = Arrays.asList("192.168.0.61", "localhost");


    /**
     * Attempts to read the current state of the user interface and create a {@link ConfigurationMessageAdapter} from it.
     * <p>
     * If any of the values read from the UI are unparseable or null, default values will be used.
     * Numbers will get 0 as default value, strings will get an empty string("") as default.
     * </p>
     *
     * @return the compiled ConfigurationMessage
     */
    private ConfigurationMessageAdapter createConfigurationMessage() {
        ConfigurationMessageAdapter msgAdapter = new ConfigurationMessageAdapter();
        DimensionsAdapter coordinatesAdapter = new DimensionsAdapter();
        Double posX = NumberUtils.tryParseDouble(txtFieldXPos.getText(), 0.0);
        Double posY = NumberUtils.tryParseDouble(txtFieldYPos.getText(), 0.0);
        Double width = NumberUtils.tryParseDouble(txtFieldWidth.getText(), 0.0);
        Double height = NumberUtils.tryParseDouble(txtFieldHeight.getText(), 0.0);
        coordinatesAdapter.setDimX(posX);
        coordinatesAdapter.setDimY(posY);
        coordinatesAdapter.setWidth(width);
        coordinatesAdapter.setHeight(height);


        msgAdapter.setDimensions(coordinatesAdapter);

        Object selectedRequestType = cbxRequestType.getSelectedItem();
        if (selectedRequestType instanceof String) {
            ConfigurationRequestTypeAdapter requestType = ConfigurationRequestTypeAdapter.fromString((String) selectedRequestType);
            msgAdapter.setRequestType(requestType);
        } else {
            throw new RuntimeException(); //FIXME nt good message
        }

        Object selectedErrorType = cbxConfigurationError.getSelectedItem();
        if (selectedErrorType instanceof String) {
            ConfigurationErrorAdapter errorType = ConfigurationErrorAdapter.fromString((String) selectedErrorType);
            msgAdapter.setConfigurationError(errorType);
        } else {
            throw new RuntimeException(); //FIXME nt good message
        }


        UIStatusAdapter statusAdapter = new UIStatusAdapter();
        statusAdapter.setCanvasHeight(1.0);
        statusAdapter.setCanvasWidth(1.0);
        for (Object o : uiElementListModel.toArray()) {
            UIElementAdapter element = (UIElementAdapter) o;
            statusAdapter.addElement(element);
        }

        msgAdapter.setUiStatus(statusAdapter);
        return msgAdapter;
    }

    private void trySetTextField(JTextField textField, Double value) {
        if (value == null) value = 0.0;
        textField.setText(value.toString());
    }

    /**
     * Reads a configuration message and writes it into the UI fields.
     *
     * @param msg
     */
    private void readConfigurationMessage(ConfigurationMessageAdapter msg) {
        cbxConfigurationError.setSelectedItem(msg.getConfigurationError());
        trySetTextField(txtFieldXPos, msg.getDimensions().getDimX());
        trySetTextField(txtFieldYPos, msg.getDimensions().getDimY());
        trySetTextField(txtFieldZPos, msg.getDimensions().getDimZ());

        trySetTextField(txtFieldHeight, msg.getDimensions().getHeight());
        trySetTextField(txtFieldWidth, msg.getDimensions().getWidth());

        for (UIElementAdapter uiElement : msg.getUiStatus().getUiElements()) {
            uiElementListModel.clear();
            uiElementListModel.addElement(uiElement);
        }
    }

    private EventMessage createEventMessage() {
        EventMessage.Builder msgBuilder = EventMessage.newBuilder();
        msgBuilder.setElementId(NumberUtils.tryParseInteger(txtElementID.getText(), 0));
        msgBuilder.setListenerId(NumberUtils.tryParseInteger(txtElementID.getText(), 0));
        String info = txtInfoString.getText();
        if (info == null) info = "";
        msgBuilder.setInfo(info);

        Object selectedEventType = cbxEventType.getSelectedItem();
        if (selectedEventType instanceof String) {
            EventTypeAdapter tp = EventTypeAdapter.fromString((String) selectedEventType);
            msgBuilder.setEventType(tp.getBaseType());
        }

        Object selectedEventMsgType = cbxEventMsgType.getSelectedItem();
        if (selectedEventMsgType instanceof String) {
            EventMessageTypeAdapter tp = EventMessageTypeAdapter.fromString((String) selectedEventMsgType);
            msgBuilder.setMessageType(tp.getBaseType());
        }

        Object selectedAttachListenerType = cbxAttachListenerError.getSelectedItem();
        if (selectedAttachListenerType instanceof String) {
            AttachListenerErrorAdapter tp = AttachListenerErrorAdapter.fromString((String) selectedAttachListenerType);
            msgBuilder.setError(tp.getBaseType());
        }
        return msgBuilder.build();
    }

    public UserInterface() {
        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
        btnConnect.addActionListener(lstnConnect);
        btnToJSON.addActionListener(lstnConvertToJSON);
        btnSendConfigurationRequest.addActionListener(lstnSendMessage);
        btnSendEventMsg.addActionListener(lstnSendEventMsg);
    }

    /**
     * Initializes the components that are representing choice options
     * for the Configuration Message protocol dialogue.
     */
    private void initializeConfigurationMessageComponents() {

        lstUIElements.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        lstUIElements.setLayoutOrientation(JList.VERTICAL);
        lstUIElements.setVisibleRowCount(-1);
        uiElementListModel = new DefaultListModel<>();
        lstUIElements.setModel(uiElementListModel);

        btnFromJSON.addActionListener(lstnReadFromJSON);
        btnAddUIElement.addActionListener(lstnAddUIElement);
        btnRemoveElement.addActionListener(lstnRemoveUIElement);

        lblConfigConnectionState.setText("-- not connected --");
        for (ConfigurationErrorAdapter error : ConfigurationErrorAdapter.values()) {
            cbxConfigurationError.addItem(error.getDisplayString());
        }
        for (ConfigurationRequestTypeAdapter type : ConfigurationRequestTypeAdapter.values()) {
            cbxRequestType.addItem(type.getDisplayString());
        }


    }

    private void intializeEventMesssageComponents() {
        for (EventMessageTypeAdapter type : EventMessageTypeAdapter.values()) {
            cbxEventMsgType.addItem(type.getDisplayString());
        }
        for (EventTypeAdapter type : EventTypeAdapter.values()) {
            cbxEventType.addItem(type.getDisplayString());
        }
        for (AttachListenerErrorAdapter type : AttachListenerErrorAdapter.values()) {
            cbxAttachListenerError.addItem(type.getDisplayString());
        }
    }

    public void show() {
        initializeConfigurationMessageComponents();
        intializeEventMesssageComponents();


        JFrame frame = new JFrame("UserInterface");
        frame.setContentPane(this.mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    private ActionListener lstnSendMessage = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            ConfigurationMessageAdapter msg = createConfigurationMessage();
            try {
                configurationMessageDAO.sendMessage(msg);
            } catch (IOException e1) {
                LOG.error("Could not send message " + msg + " to connected remote.");
                JOptionPane.showMessageDialog(null, "Could not send message.");
            }
        }
    };

    private ActionListener lstnConnect = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String hostname = txtFieldHostname.getText();
            Integer port = Integer.parseInt(txtFieldPort.getText());
            try {
                LOG.info("Attempting to connect to " + hostname + ":" + port + "[ConfigurationMessage]");
                configurationMessageDAO.connect(hostname, port);
                LOG.info("Connection successful to " + hostname + ":" + port + "[ConfigurationMessage]");
                lblConfigConnectionState.setText("Connected to " + hostname + ":" + port);

                /*LOG.info("Attempting to connect to " + hostname + ":" + port + "[EventMessage]");
                eventMessageDAO.connect(hostname, port);
                LOG.info("Connection successful to " + hostname + ":" + port + "[EventMessage]");*/
                new MessageQueue(configurationMessageDAO);

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Could not connect to " + hostname + ":" + port);
                LOG.error("Could not connect to " + hostname + ":" + port, ex);
            }
        }
    };

    private ActionListener lstnSendEventMsg = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            EventMessage msg = createEventMessage();
            try {
                LOG.info("Sending event message.");
                eventMessageDAO.sendMessage(msg);
                LOG.info("Event message sent.");
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(null, "Could not send event message to connected host.");
                LOG.error("Could not send event message to connected host.", e1);
            }
        }
    };

    private final ActionListener lstnAddUIElement = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            UIElementAdapter uiElementAdapter = new UIElementAdapter();
            UIElementCreatorDlg dlg = new UIElementCreatorDlg(uiElementAdapter);
            uiElementListModel.addElement(uiElementAdapter);
        }
    };

    private final ActionListener lstnRemoveUIElement = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            uiElementListModel.remove(lstUIElements.getSelectedIndex());
        }
    };

    private final ActionListener lstnConvertToJSON = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ObjectMapper mapper = new ObjectMapper();
            final ObjectWriter w = mapper.writer().with(SerializationFeature.INDENT_OUTPUT);
            String json;
            try {
                json = w.writeValueAsString(createConfigurationMessage());
            } catch (JsonProcessingException e1) {
                throw new RuntimeException(e1);
            }
            txtAreaJSONOutput.setText(json);
        }
    };

    private final ActionListener lstnReadFromJSON = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ObjectMapper mapper = new ObjectMapper();
            final ObjectReader r = mapper.readerFor(ConfigurationMessageAdapter.class);
            Object json;
            try {
                json = r.readValue(txtAreaJSONOutput.getText());
            } catch (IOException e1) {
                throw new RuntimeException(e1);
            }
            ConfigurationMessageAdapter msg = (ConfigurationMessageAdapter) json;
            readConfigurationMessage(msg);
        }
    };



    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new FormLayout("fill:20dlu:noGrow,left:4dlu:noGrow,fill:80dlu:grow,left:4dlu:noGrow,fill:80dlu:grow,left:4dlu:noGrow,fill:80dlu:grow,left:4dlu:noGrow,fill:80dlu:grow,left:4dlu:noGrow,fill:20dlu:noGrow", "center:d:grow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:20dlu:grow"));
        tabbedPane1 = new JTabbedPane();
        CellConstraints cc = new CellConstraints();
        mainPanel.add(tabbedPane1, cc.xyw(3, 5, 7));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new FormLayout("center:120dlu:noGrow,left:4dlu:noGrow,fill:200dlu:noGrow,left:4dlu:noGrow,fill:5dlu:noGrow,left:4dlu:noGrow,fill:100dlu:grow,left:4dlu:noGrow,fill:100dlu:grow,left:4dlu:noGrow,fill:d:grow", "center:d:grow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:20dlu:grow,top:4dlu:noGrow,center:20dlu:noGrow,top:4dlu:noGrow,center:20dlu:grow,top:4dlu:noGrow,center:d:grow,top:4dlu:noGrow,center:d:grow"));
        tabbedPane1.addTab("SendConfigMessage", panel1);
        final JLabel label1 = new JLabel();
        label1.setText("height");
        panel1.add(label1, cc.xy(1, 3));
        txtFieldHeight = new JTextField();
        txtFieldHeight.setText("1");
        panel1.add(txtFieldHeight, cc.xy(3, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label2 = new JLabel();
        label2.setText("width");
        panel1.add(label2, cc.xy(1, 5));
        txtFieldWidth = new JTextField();
        txtFieldWidth.setText("1");
        panel1.add(txtFieldWidth, cc.xy(3, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label3 = new JLabel();
        label3.setText("xPos");
        panel1.add(label3, cc.xy(1, 7));
        final JLabel label4 = new JLabel();
        label4.setText("Configuration Error");
        panel1.add(label4, cc.xy(1, 13));
        cbxConfigurationError = new JComboBox();
        panel1.add(cbxConfigurationError, cc.xy(3, 13));
        final JLabel label5 = new JLabel();
        label5.setText("Reqeust Type");
        panel1.add(label5, cc.xy(1, 15));
        cbxRequestType = new JComboBox();
        cbxRequestType.setBackground(new Color(-1645592));
        panel1.add(cbxRequestType, cc.xy(3, 15));
        final JLabel label6 = new JLabel();
        label6.setText("yPos");
        panel1.add(label6, cc.xy(1, 9));
        final JLabel label7 = new JLabel();
        label7.setText("zPos");
        panel1.add(label7, cc.xy(1, 11));
        txtFieldXPos = new JTextField();
        txtFieldXPos.setText("1");
        panel1.add(txtFieldXPos, cc.xy(3, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        txtFieldYPos = new JTextField();
        txtFieldYPos.setText("1");
        panel1.add(txtFieldYPos, cc.xy(3, 9, CellConstraints.FILL, CellConstraints.DEFAULT));
        txtFieldZPos = new JTextField();
        txtFieldZPos.setText("1");
        panel1.add(txtFieldZPos, cc.xy(3, 11, CellConstraints.FILL, CellConstraints.DEFAULT));
        lblConfigConnectionState = new JLabel();
        lblConfigConnectionState.setText("Label");
        panel1.add(lblConfigConnectionState, cc.xy(3, 17));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, cc.xy(1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
        final Spacer spacer2 = new Spacer();
        panel1.add(spacer2, cc.xy(5, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label8 = new JLabel();
        label8.setText("UI Elements");
        panel1.add(label8, cc.xy(3, 19));
        btnRemoveElement = new JButton();
        btnRemoveElement.setBackground(new Color(-3112323));
        btnRemoveElement.setForeground(new Color(-107687));
        btnRemoveElement.setText("-");
        panel1.add(btnRemoveElement, cc.xy(1, 23));
        final Spacer spacer3 = new Spacer();
        panel1.add(spacer3, cc.xy(1, 25, CellConstraints.DEFAULT, CellConstraints.FILL));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel1.add(scrollPane1, cc.xywh(3, 21, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        lstUIElements = new JList();
        scrollPane1.setViewportView(lstUIElements);
        final JLabel label9 = new JLabel();
        label9.setText("JSON output");
        panel1.add(label9, cc.xy(7, 3));
        btnSendConfigurationRequest = new JButton();
        btnSendConfigurationRequest.setText("Send Message");
        panel1.add(btnSendConfigurationRequest, cc.xy(1, 27));
        final JLabel label10 = new JLabel();
        label10.setText("Status");
        panel1.add(label10, cc.xy(1, 17));
        btnAddUIElement = new JButton();
        btnAddUIElement.setBackground(new Color(-14292992));
        btnAddUIElement.setText("+");
        panel1.add(btnAddUIElement, cc.xy(1, 21));
        btnToJSON = new JButton();
        btnToJSON.setText("To JSON");
        panel1.add(btnToJSON, cc.xy(7, 27));
        btnFromJSON = new JButton();
        btnFromJSON.setText("From JSON");
        panel1.add(btnFromJSON, cc.xy(9, 27));
        final JScrollPane scrollPane2 = new JScrollPane();
        panel1.add(scrollPane2, cc.xywh(7, 5, 3, 21, CellConstraints.FILL, CellConstraints.FILL));
        txtAreaJSONOutput = new JTextArea();
        txtAreaJSONOutput.setText("A");
        scrollPane2.setViewportView(txtAreaJSONOutput);
        final Spacer spacer4 = new Spacer();
        panel1.add(spacer4, cc.xy(3, 29, CellConstraints.DEFAULT, CellConstraints.FILL));
        final Spacer spacer5 = new Spacer();
        panel1.add(spacer5, cc.xy(11, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new FormLayout("fill:120dlu:noGrow,left:4dlu:noGrow,fill:200dlu:noGrow", "center:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        tabbedPane1.addTab("SendEventMessage", panel2);
        final JLabel label11 = new JLabel();
        label11.setText("Type");
        panel2.add(label11, cc.xy(1, 1));
        cbxEventMsgType = new JComboBox();
        panel2.add(cbxEventMsgType, cc.xy(3, 1));
        final JLabel label12 = new JLabel();
        label12.setText("EventType");
        panel2.add(label12, cc.xy(1, 3));
        cbxEventType = new JComboBox();
        panel2.add(cbxEventType, cc.xy(3, 3));
        cbxAttachListenerError = new JComboBox();
        panel2.add(cbxAttachListenerError, cc.xy(3, 5));
        final JLabel label13 = new JLabel();
        label13.setText("Error");
        panel2.add(label13, cc.xy(1, 5));
        final JLabel label14 = new JLabel();
        label14.setText("Element ID");
        panel2.add(label14, cc.xy(1, 7));
        txtElementID = new JTextField();
        panel2.add(txtElementID, cc.xy(3, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label15 = new JLabel();
        label15.setText("Listener ID");
        panel2.add(label15, cc.xy(1, 9));
        txtListenerID = new JTextField();
        panel2.add(txtListenerID, cc.xy(3, 9, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label16 = new JLabel();
        label16.setText("Info String");
        panel2.add(label16, cc.xy(1, 11));
        txtInfoString = new JTextField();
        panel2.add(txtInfoString, cc.xy(3, 11, CellConstraints.FILL, CellConstraints.DEFAULT));
        btnSendEventMsg = new JButton();
        btnSendEventMsg.setText("Send");
        panel2.add(btnSendEventMsg, cc.xy(1, 13));
        final Spacer spacer6 = new Spacer();
        mainPanel.add(spacer6, cc.xy(1, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        txtFieldHostname = new JTextField();
        txtFieldHostname.setText("192.168.0.61");
        mainPanel.add(txtFieldHostname, cc.xy(5, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label17 = new JLabel();
        label17.setText("Hostname");
        mainPanel.add(label17, cc.xy(3, 7));
        final JLabel label18 = new JLabel();
        label18.setText("Port");
        mainPanel.add(label18, cc.xy(7, 7));
        final Spacer spacer7 = new Spacer();
        mainPanel.add(spacer7, cc.xy(1, 9, CellConstraints.DEFAULT, CellConstraints.FILL));
        btnConnect = new JButton();
        btnConnect.setText("Connect");
        mainPanel.add(btnConnect, cc.xy(9, 9));
        txtFieldPort = new JTextField();
        txtFieldPort.setText("5000");
        mainPanel.add(txtFieldPort, cc.xy(9, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        final Spacer spacer8 = new Spacer();
        mainPanel.add(spacer8, cc.xy(11, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        label5.setLabelFor(cbxRequestType);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}
